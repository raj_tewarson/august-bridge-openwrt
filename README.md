August Bridge for openwrt
=============

to build the bridge application, until there's a OpenWRT_SDK you will need to likely have a full openwrt build

build the application by running 'make package/august_bridge/compile' at the top level of openwrt

Alternatively from the feeds: 

$ echo src-git git@bitbucket.org/august-bridge-openwrt.git august_bridge > feeds.conf    
$ ./scripts/feeds update   
$ ./scripts/feeds install -a     

in the openwrt/package directory
git clone git@bitbucket.org/august-bridge-openwrt.git august_bridge

TODO: while there should be a preconfigured config file for the particular project eg linkit_7688_dot_config

$ make menuconfig   

or copy linkit_7688_dot_config to openwrt/.config (if you're using a mediatek 7688)

select august_bridge Under utils->august_bridge   
IMPORTANT NOTE: make sure libcurl is configured to use "openssl" not mbedtls...

$ make V=99   

NOTES: A complete connect solution would likely require the following packages:
august-bridge-openwrt (this repo)    
august-wac-openwrt    (to support apple wireless accessory configuration/device setup)    
august-update-openwrt (to support OTA updates)    

Notes to setup wifi:

$ uci set wireless.default_radio0.mode='sta'    
$ uci set wireless.default_radio0.network='wwan'    
$ uci set wireless.default_radio0.ssid='ssid'    
$ uci set wireless.default_radio0.key='password'    
$ uci commit    
$ wifi    

for some reason dbus bluetooth doesn't start correctly    
$ killall dbus-daemon    
$ dbus-daemon --system    
$ hciconfig hci0 up   
$ /usr/bin/august_bridge -v 6 -s D1GPR0021J -b "https://rbs.august.com" -l "FDCF01C8A52646BD86ABD4CA31B28A82"  -m "78:9C:85:01:24:28"   
 


