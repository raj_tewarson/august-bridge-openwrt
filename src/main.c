/**
 * \brief Entry point for bridge application.
 * 
 * The remote bridge for the august doorbell interfaces with Bluez and accepts
 * commands from the remote bridge server. The basic flow is as follows:
 *
 * [X] 1. The bridge gets an auth token from the RBS sever.
 * [X] 2. The bridge does a GET and waits for the server to issue a command.
 * [X] 3. The server issues a command.
 * [X] 4. The bridge enqueues the command to be run and runs another GET.
 * [X] 5. The bridge proxies the command to the lock.
 * [X] 6. The bridge POSTs the return value from the lock to the server.
 *
 */

// System includes
#include <glib.h>
#include <json.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>


// Application includes
#include "threads/ble.h"
#include "threads/common.h"
#include "threads/lockop.h"
#include "threads/messages.h"

#include "utils/curl.h"
#include "utils/dbus.h"
#include "utils/environ.h"
#include "utils/file.h"
#include "utils/json.h"
#include "utils/logging.h"
#include "utils/string.h"
#include "blescand/blescan.h"

// Constants
#define AUTH_TOKEN_KEY  "token"
#define BRIDGE_ID_KEY   "_id"
#define RETRY_COUNT     30
#define MAX_SLEEP       15
#define LOCK_ID_LENGTH  32
#define LOCK_MAC_LENGTH 17
#define LOCK_GEN_LENGTH 1

static thread_config_t config = {
  .running = true,
};

// Private functions
static void usage(void);
static void term_handler(int signum);

/* setup */
static void setup_bridge(
  const char *server,
  const char *serial,
  const char **auth_token,
  const char **bridge_id);
static const char *get_auth_token(json_object *data);
static const char *get_bridge_id(json_object *data);
static const char *get_lock_id(void);
static const char *get_lock_mac(void);

void usage(void) {
  printf("Usage: bridge [-h] -s <serial> -b <server>"
    " [-l <lock id>] [-m <lock mac>] [-n]\n"
    "  -h\t\t\tPrints this message\n"
    "  -s <serial number>\tThe serial number of the device.\n"
    "  -b <bridge server>\tThe bridge server to connect to.\n"
    "  -d <hci device>\t(Optional) The bluetooth adapter to use.\n"
    "  -l <lock id>\t\t(Optional) The august provided lock id.\n"
    "  -m <lock mac>\t\t(Optional) The mac address of the lock.\n"
    "  -n \t\t\t(Optional) Disable blueZ cache.\n"
    "  -v \t\t\t(Optional) Specify the verbosity of the bridge.\n"
    "  -r \t\t\t(Optional) Enabled video recording on dirty bit trigger.\n"
    "\nEnviron Overrides:\n"
    "  " ENV_MARS_CA "\t\tSpecifies the location of the ca certificate.\n"
    "\t\t\tDefaults to \"" MARS_CA "\"\n\n"
    "  " ENV_MARS_CERT "\t\tSpecifies the location of the SSL cert file.\n"
    "\t\t\tDefaults to \"" MARS_CERT "\"\n\n"
#if KEISTER_ENABLED == 0
    "  " ENV_MARS_KEY "\t\tSpecifies the location of the SSL key file.\n"
    "\t\t\tDefaults to \"" MARS_KEY "\"\n\n"
#endif
    "  " ENV_MARS_LOCK_MAC "\t\tSpecifies the location of the mac file.\n"
    "\t\t\tDefaults to \"" MARS_LOCK_MAC "\"\n\n"
    "  " ENV_MARS_LOCK_ID "\t\tSpecifies the location of the lock id file.\n"
    "\t\t\tDefaults to \"" MARS_LOCK_ID "\"\n\n");
}

static void term_handler(int signum) {
  g_message("terminating logger");
  config.running = false;
}

const char *get_auth_token(json_object *data) {
  return json_clone_string_value(data, AUTH_TOKEN_KEY);
}

const char *get_bridge_id(json_object *data) {
  return json_clone_string_value(data, BRIDGE_ID_KEY);
}

const char *get_lock_id(void) {
  const char * contents = NULL;
  const char *lock_id_path = get_lock_id_path();
  if (file_exists(lock_id_path)) {
    contents = file_read(lock_id_path);
    if (contents && LOCK_ID_LENGTH == strlen(contents)) {
      return contents;
    } else {
      return NULL;
    }
  } else {
    return NULL;
  }
}

const char *get_lock_mac(void) {
  const char * contents = NULL;
  const char *lock_mac_path = get_lock_mac_path();
  if (file_exists(lock_mac_path)) {
    contents = file_read(lock_mac_path);
    if (NULL == contents) {
      return NULL;
    } else if (LOCK_MAC_LENGTH == strlen(contents)) {
      return contents;
    } else {
      return NULL;
    }
  } else {
    return NULL;
  }
}

uint8_t get_lock_gen(void) {
  char* contents = NULL;
  const char *lock_gen_path = get_lock_gen_path();
  uint8_t lock_gen = 0;

  if (file_exists(lock_gen_path)) {
    contents = file_read(lock_gen_path);
    if (NULL != contents) {
      if (LOCK_GEN_LENGTH == strlen(contents))
        lock_gen = atoi(contents);
      free(contents);
    } /* if */
  } /* if */

  return lock_gen;
} /* get_lock_gen */

void setup_bridge(
    const char *server,
    const char *serial,
    const char **auth_token,
    const char **bridge_id) {

  bool success = false;
  int http_status = 0;
  unsigned int retry_attempts = 0;
  const char *url = NULL;
  const char *response = NULL;
  json_object *j_response = NULL;

  g_debug("%s", __FUNCTION__);

  url = get_bridge_setup_url(server, serial);

  for (retry_attempts = 0; retry_attempts < RETRY_COUNT; retry_attempts++) {
    if (200 == (http_status = get(url, &response, NULL, false, &config.running))) {
      success = true;
      break;
    }

    if (404 == http_status) {
      g_warning("Bridge isn't registered");
      success = false;
      exit(ENODEV);
      break;
    }

    g_debug(
      "Couldn't get auth info: %d, (attempt %d of %d)",
      http_status,
      (retry_attempts + 1),
      RETRY_COUNT);
    sleep(MIN(MAX_SLEEP, 1 + retry_attempts));
  }
  
  if (!success) {
    g_warning("Could not get auth information from bridge server: %d",
      http_status);

    if (http_status <= 0)
      exit(EHOSTUNREACH);
    else
      exit(ENOKEY);
  }

  j_response = json_tokener_parse(response);

  *auth_token = get_auth_token(j_response);
  *bridge_id = get_bridge_id(j_response);

  if (url != NULL) free((void *)url);
  if (response != NULL) free((void *)response);
  if (j_response != NULL) json_object_put(j_response);
}


static void signal_seg_fault(int sig, siginfo_t *si, void *ctx)
{
    void *buffer[16];
    char **str;
    int i, n;

    //n = backtrace(buffer, 16);

    fprintf(stderr, "SEG_FAULT @%X -- %d\nStack:\n", si->si_addr, n);
    //str = backtrace_symbols(buffer, n);

    //for(i=0; i<n; i++) {
    //    fprintf(stderr, "%s\n", str[i]);
    // }

    _exit(-1);
}


int main(int argc, char *argv[]) {
  bool help = false;
  GAsyncQueue *ble_messages_queue = NULL;
  bool blescand = true;

  g_log_set_default_handler(custom_log, NULL);

  struct sigaction action;

  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = term_handler;
  sigaction(SIGTERM, &action, NULL);
  //sigaction(SIGINT, &action, NULL);

  // TODO: segfault handler
  //memset(&action, 0, sizeof(struct sigaction));
  //action.sa_handler = signal_seg_fault;
  //action.sa_flags   = SA_SIGINFO;
  //sigaction(SIGSEGV, &action, NULL);

  config.use_bluez_cache = true;
  config.dirty_video = false;

  int opt;
  while ((opt = getopt(argc, argv, "hs:b:d:l:m:nv:g:rx")) != -1) {
    //printf("options = %c\n", opt);
    switch (opt) {
      case 'h':
        help = true;
        break;
      case 's':
        config.serial_number = optarg;
        break;
      case 'b':
        config.bridge_server = optarg;
        break;
      case 'd':
        config.hci_device = optarg;
        break;
      case 'l':
        config.lock_id = optarg;
        break;
      case 'm':
        config.lock_mac = optarg;
        break;
      case 'g':
        config.lock_gen = atoi(optarg);
        break;
      case 'n':
        config.use_bluez_cache = false;
        break;
      case 'v':
        verbosity = 1 << (1 + atoi(optarg));
        break;
      case 'r':
        printf(" r command called\n");
        config.dirty_video = true;
        break;
      case 'x':
        blescand = false;
        break;
      default:
        printf("Invalid argument\n");
        usage();
        exit(-1);
    }
  }

  if (help) {
    usage();
    exit(0);
  }

  g_debug("Starting bridge");

  init_curl();

  if (NULL == config.serial_number)
    die(EAGAIN, "need serial number to continue");

  // TODO: iconfig_fetch...
 
  if (NULL == config.bridge_server)
    die(EAGAIN, "need bridge server to continue");
  if (NULL == config.lock_id)
    config.lock_id = get_lock_id();
  if (NULL == config.lock_mac)
    config.lock_mac = get_lock_mac();
  if (0 == config.lock_gen)
    config.lock_gen = get_lock_gen();
  if (NULL == config.hci_device)
    config.hci_device = "hci0";
  
  setup_bridge(config.bridge_server, config.serial_number,
    &(config.auth_token), &(config.bridge_id));

  g_message("serial_number: %s", config.serial_number);
  g_message("bridge_server: %s", config.bridge_server);
  g_message("lock_id: %s", config.lock_id);
  g_message("lock_mac: %s", config.lock_mac);
  g_message("lock_gen: %d", config.lock_gen);
  g_message("bridge_id: %s", config.bridge_id);

  ble_messages_queue = g_async_queue_new();
  
  if(blescand) {
    blescan_init();
  }

  create_lockop_thread(&config);
  create_ble_thread(&config, ble_messages_queue);
  create_messages_thread(&config, ble_messages_queue);

  g_debug("startup complete");

  pthread_join(lockop_thread, NULL);
  g_debug("lockop_thread joined");
  pthread_join(ble_thread, NULL);
  pthread_join(messages_thread, NULL);

  g_debug("threads joined");

  g_async_queue_unref(ble_messages_queue);

  if (config.auth_token != NULL) free((void *)config.auth_token);
  if (config.bridge_id != NULL) free((void *)config.bridge_id);

  dbus_cleanup();
  cleanup_curl();

  g_debug("exit complete");

  return 0;
}
