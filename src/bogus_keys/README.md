KEISTER WTH
===========

The Mars project needs a few secret values in order to work and those values
are stored within an encrypted structure (called a 'Keister') in flash memory
on the Mars device.

At the moment, the secret values needed by Mars are:

a symmetric encryption key for Mars update packages
a public/private key pair for signing Mars update packages
a private key for SSL client certificates
a symmetric encryption key for passing WiFi setup parameters from Android
a symmetric encryption key for accessing the Mars Keister

those keys are available in ascii-hex and C header file forms in this
directory so that intermediate build files need not be stored on machines that
use this data.

the Mars source code repository contains a directory called bogus_keys that
has place holders for all of the keys needed by Mars for use in software
development.

production keys are available on secure external storage media that will mount
on OS X machines as /Volumes/Keys

the top-level Mars Makefile will use the environment variable
SECURE_KEYS_PATH to search for secure keys.
if SECURE_KEYS_PATH is not set, the Mars Makefile will use bogus_keys to build.

