/*
 * Copyright 2015 August Home
 *
 * THIS FILE WAS AUTOMATICALLY GENERATED FROM /home/vagrant/august-mars-firmware/keister/../bogus_keys/mars_keister_master_key.hex
 */

#ifndef MARS_KEISTER_MASTER_KEY_H
#define MARS_KEISTER_MASTER_KEY_H 1 

static const unsigned char mars_keister_master_key[] = {
0x4c, 0xb7, 0xe4, 0xf8, 0x01, 0x07, 0xaa, 0xd0, 0xe4, 0x40, 0x52, 0x1d, 0x1c, 0x08, 0x23, 0xf8, };
#endif