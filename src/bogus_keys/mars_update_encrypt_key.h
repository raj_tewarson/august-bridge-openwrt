/*
 * Copyright 2015 August Home
 *
 * THIS FILE WAS AUTOMATICALLY GENERATED FROM mars_update_encrypt_key.hex
 */

#ifndef MARS_UPDATE_ENCRYPT_KEY_H
#define MARS_UPDATE_ENCRYPT_KEY_H 1 

static const unsigned char mars_update_encrypt_key[] = {
  0x0f, 0x98, 0x37, 0xba, 0x4c, 0x66, 0xf8, 0xd0, 
  0x7f, 0x76, 0xdd, 0x9d, 0x64, 0x3b, 0x31, 0x70, 
  0x3b, 0xd9, 0xcd, 0x45, 0x5d, 0x32, 0xa5, 0x9b, 
  0xef, 0x11, 0x43, 0x3c, 0x24, 0xea, 0x78, 0xf4, 
  };
#endif
