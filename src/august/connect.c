#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "august/connect.h"
#include "august/aug_lock_ble_properties.h"

#include "utils/dbus.h"
#include "utils/environ.h"
#include "utils/logging.h"
#include "utils/string.h"
#include "utils/timeout.h"

static gboolean disconnect_timeout(gpointer user_data);

static void device_added_handler(
    GDBusObjectManager *manager,
    GDBusObject *object,
    gpointer user_data) {

  connect_data_t *connect_data = (connect_data_t *)user_data;
  const char *other_path = g_dbus_object_get_object_path(object);
  const char *my_path = 
    g_dbus_proxy_get_object_path(connect_data->device_proxy);

  if ((0 == strcmp(my_path, other_path)) &&
     (strlen(my_path) == strlen(other_path))) {
    // Clear the device discovery timeout
    connect_data->discover_timeout_id =
      clear_timeout(connect_data->discover_timeout_id);

    // Call discovery callback with success
    connect_data->discovered_cb(connect_data->user_data, (void *)true);
  }
}

static void service_discovery_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  connect_data_t *connect_data = (connect_data_t *)user_data;
  GVariantDict *dict = g_variant_dict_new(changed_properties);
  GVariant *value = g_variant_dict_lookup_value(dict, "Characteristics", NULL);

  if (value && g_variant_n_children(value) > 0) {

   g_debug("Found a service");

   connect_data->service_discovered = 
     dbus_service_find_august_characteristics(
       connect_data->service_proxy);

    if (connect_data->service_discovered) {
      g_debug("It's our service");

      if (connect_data->connect_guard_time_passed) {
        dbus_adapter_stop_discovery(connect_data->adapter_proxy);
          // Clear the connect timeouts
          connect_data->connect_timeout_id =
            clear_timeout(connect_data->connect_timeout_id);
          connect_data->disconnect_timeout_id =
            clear_timeout(connect_data->disconnect_timeout_id);

          // Call connect callback with success
          connect_data->connected_cb(connect_data->user_data, (void *)true);
      } /* if */
    } /* if */

    if (value != NULL) g_variant_unref(value);
  }
}

/*
 * find_august_service
 *
 * iterate through the given services to find the August lock service
 */
static void find_august_service(
    GVariant *services,
    connect_data_t *connect_data) {

  if (services && g_variant_is_container(services)) {
    GVariantIter iter;
    GVariant *child;

    g_variant_iter_init(&iter, services);

    while ((!dbus_object_exists(connect_data->service_proxy)) &&
          ((child = g_variant_iter_next_value(&iter)))) {
      GError *error = NULL;
      GVariant *tuple = NULL;
      GDBusProxy *new_proxy = NULL;

      g_debug("service %s", g_variant_get_string(child, NULL));

      new_proxy = dbus_generic_proxy(
        g_variant_get_string(child, NULL),
        "org.bluez.GattService1");

      tuple = g_dbus_proxy_call_sync(
        new_proxy,
        "org.freedesktop.DBus.Properties.Get",
        g_variant_new("(ss)", "org.bluez.GattService1", "UUID"),
        G_DBUS_CALL_FLAGS_NONE,
        500, // If this isn't set to some small number of ms, we miss events
        NULL,
        &error);

      if (tuple && g_variant_is_container(tuple)) {
        GVariant *svc_uuid = NULL;

        g_variant_get(tuple, "(v)", &svc_uuid);

        g_debug("service UUID %s", g_variant_get_string(svc_uuid, NULL));

        if ((0 == strcasecmp(
          kAugLockServiceUUID,
          g_variant_get_string(svc_uuid, NULL))) ||
           (0 == strcasecmp(
          kAugLockServiceUUID16_PLUS_128_BASE,
          g_variant_get_string(svc_uuid, NULL)))) {

          if (connect_data->service_proxy)
            g_object_unref(connect_data->service_proxy);

          connect_data->service_proxy = new_proxy;
          g_object_ref(new_proxy);

          connect_data->service_discovery_handler_id = g_signal_connect(
              connect_data->service_proxy,
              "g-properties-changed",
              (GCallback)service_discovery_handler,
              (gpointer)connect_data);

          connect_data->service_discovered = 
            dbus_service_find_august_characteristics(
              connect_data->service_proxy);

          if (connect_data->connect_guard_time_passed) {
            dbus_adapter_stop_discovery(connect_data->adapter_proxy);
              // Clear the connect timeouts
              connect_data->connect_timeout_id =
                clear_timeout(connect_data->connect_timeout_id);
              connect_data->disconnect_timeout_id =
                clear_timeout(connect_data->disconnect_timeout_id);

            // Call connect callback with success
            connect_data->connected_cb(connect_data->user_data, (void *)true);
          } /* if */
        } /* if */

        g_variant_unref(svc_uuid);
      } /* if */

      g_variant_unref(tuple);
      g_variant_unref(child);
      g_object_unref(new_proxy);
    } /* while */
  } /* if */
} /* find_august_service */

static void connect_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  connect_data_t *connect_data = (connect_data_t *)user_data;
  GVariantDict dict;
  g_variant_dict_init(&dict, changed_properties);
  GVariant *value = g_variant_dict_lookup_value(&dict, "Connected", NULL);

  if (value && g_variant_get_boolean(value)) {
    // Set the disconnect timer. If we don't get a disconnect before the timer
    // expires then we are actually connected.
    if (get_timeout_value(ENV_WAIT_FOR_CONNECTION)) {
      connect_data->disconnect_timeout_id = create_timeout(
        get_timeout_value(ENV_WAIT_FOR_CONNECTION),
        disconnect_timeout,
        connect_data);
    } else {
      if (connect_data->service_discovered)
        disconnect_timeout(connect_data);
    }
  }
  if (value != NULL) g_variant_unref(value);

  if (!connect_data->service_discovered) {
    value = g_variant_dict_lookup_value(&dict, "GattServices", NULL);

    if (0 == get_timeout_value(ENV_WAIT_FOR_CONNECTION))
      connect_data->connect_guard_time_passed = true;

    find_august_service(value, connect_data);

    if (value != NULL) g_variant_unref(value);
  } /* if */

  g_variant_dict_clear(&dict);
}

static void disconnect_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  connect_data_t *connect_data = (connect_data_t *)user_data;
  GVariantDict dict;
  g_variant_dict_init(&dict, changed_properties);
  GVariant *value = g_variant_dict_lookup_value(&dict, "Connected", NULL);

  if (value && !g_variant_get_boolean(value)) {

    dbus_adapter_stop_discovery(connect_data->adapter_proxy);

    // call disconnect callback with ?success?
    connect_data->disconnected_cb(connect_data->user_data, (void *)true);

    // Clear any pending disconnect timeouts
    connect_data->disconnect_timeout_id =
      clear_timeout(connect_data->disconnect_timeout_id);
    if (value != NULL) g_variant_unref(value);
  } /* if */

  g_variant_dict_clear(&dict);
} /* disconnect_handler */

static void device_removed_handler(
    GDBusObjectManager *manager,
    GDBusObject *object,
    gpointer user_data) {

  connect_data_t *connect_data = (connect_data_t *)user_data;
  const char *other_path = g_dbus_object_get_object_path(object);
  const char *my_path = g_dbus_proxy_get_object_path(connect_data->device_proxy);
  
  fprintf(stderr, "%s()\n", __FUNCTION__);

  if(!other_path || !my_path) {
    fprintf(stderr, "%s()  path empty\n", __FUNCTION__);
    return;
  }

  if ((0 == strcmp(my_path, other_path)) &&
     (strlen(my_path) == strlen(other_path))) {
    // Clear the disconnect timeout
    connect_data->remove_timeout_id =
      clear_timeout(connect_data->remove_timeout_id);

    // Call removed callback with success
    connect_data->removed_cb(connect_data->user_data, (void *)true);
  }
}

static gboolean discover_timeout(gpointer user_data) {
  connect_data_t *connect_data = (connect_data_t *)user_data;
  connect_data->discovered_cb(connect_data->user_data, NULL);
  return G_SOURCE_REMOVE;
}

static gboolean connect_timeout(gpointer user_data) {
  connect_data_t *connect_data = (connect_data_t *)user_data;
  connect_data->connected_cb(connect_data->user_data, NULL);
  return G_SOURCE_REMOVE;
}

// If this poorly named timer fires before we get a disconnect, we should
// be really actually connected.
static gboolean disconnect_timeout(gpointer user_data) {
  connect_data_t *connect_data = (connect_data_t *)user_data;

  connect_data->connect_guard_time_passed = true;

  if (connect_data->service_discovered) {
    dbus_adapter_stop_discovery(connect_data->adapter_proxy);
    // Clear the connect timeouts
    connect_data->connect_timeout_id =
      clear_timeout(connect_data->connect_timeout_id);

    // Call connect callback with success
    connect_data->connected_cb(connect_data->user_data, (void *)true);
  } /* if */

  return G_SOURCE_REMOVE;
}

static gboolean remove_timeout(gpointer user_data) {
  connect_data_t *connect_data = (connect_data_t *)user_data;
  connect_data->removed_cb(connect_data->user_data, NULL);
  return G_SOURCE_REMOVE;
}

connect_data_t *connect_data_new(
    uint8_t lock_gen,
    const char *bt_address,
    const char *hci_device,
    const char *lock_id,
    connect_callback_t discovered_cb,
    connect_callback_t connected_cb,
    connect_callback_t disconnected_cb,
    connect_callback_t removed_cb,
    void *user_data) {

  g_message("%s()", __FUNCTION__);

  connect_data_t *connect_data = malloc(sizeof(connect_data_t));
  if (NULL == connect_data) {
    die(EAGAIN, "couldn't initialize connect data");
  }
  memset(connect_data, 0, sizeof(connect_data_t)); 

  connect_data->bt_address = bt_address;
  connect_data->hci_device = hci_device;
  connect_data->lock_id = lock_id;

  connect_data->discovered_cb = discovered_cb;
  connect_data->connected_cb = connected_cb;
  connect_data->disconnected_cb = disconnected_cb;
  connect_data->removed_cb = removed_cb;
  connect_data->user_data = user_data;

  connect_data->object_manager = dbus_object_manager();
  connect_data->adapter_proxy = dbus_adapter_proxy(hci_device);
  connect_data->device_proxy = dbus_device_proxy(bt_address, hci_device);
  connect_data->service_proxy = dbus_service_proxy(lock_gen, bt_address, hci_device);

  connect_data->device_added_handler_id = g_signal_connect(
    connect_data->object_manager,
    "object-added",
    (GCallback)device_added_handler,
    (gpointer)connect_data);
  if (connect_data->device_added_handler_id <= 0)
    die(EAGAIN, "could not connect device added handler");

  connect_data->service_discovery_handler_id = g_signal_connect(
      connect_data->service_proxy,
      "g-properties-changed",
      (GCallback)service_discovery_handler,
      (gpointer)connect_data);
  if (connect_data->service_discovery_handler_id <= 0)
    die(EAGAIN, "could not hook up service discovery handler");

  connect_data->connect_handler_id = g_signal_connect(
      connect_data->device_proxy,
      "g-properties-changed",
      (GCallback)connect_handler,
      (gpointer)connect_data);
  if (connect_data->connect_handler_id <= 0)
    die(EAGAIN, "could not hook up connect handler");

  connect_data->disconnect_handler_id = g_signal_connect(
      connect_data->device_proxy,
      "g-properties-changed",
      (GCallback)disconnect_handler,
      (gpointer)connect_data);
  if (connect_data->disconnect_handler_id <= 0)
    die(EAGAIN, "could not hook up disconnect signal handler");

  connect_data->device_removed_handler_id = g_signal_connect(
    connect_data->object_manager,
    "object-removed",
    (GCallback)device_removed_handler,
    (gpointer)connect_data);
  if (connect_data->device_removed_handler_id <= 0)
    die(EAGAIN, "could not connect device removed handler");

  return connect_data;
}

void connect_data_free(connect_data_t *connect_data) {
  g_signal_handler_disconnect(
      connect_data->object_manager,
      connect_data->device_added_handler_id);
  g_signal_handler_disconnect(
      connect_data->service_proxy,
      connect_data->service_discovery_handler_id);
  g_signal_handler_disconnect(
      connect_data->device_proxy,
      connect_data->connect_handler_id);
  g_signal_handler_disconnect(
      connect_data->device_proxy,
      connect_data->disconnect_handler_id);
  g_signal_handler_disconnect(
      connect_data->object_manager,
      connect_data->device_removed_handler_id);

  g_object_unref(connect_data->object_manager);
  g_object_unref(connect_data->adapter_proxy);
  g_object_unref(connect_data->device_proxy);
  g_object_unref(connect_data->service_proxy);

  clear_timeout(connect_data->connect_timeout_id);
  clear_timeout(connect_data->discover_timeout_id);
  clear_timeout(connect_data->remove_timeout_id);

  free((void *)connect_data);
}

void discover_device(connect_data_t *connect_data) {

  if (dbus_object_exists(connect_data->device_proxy)) {
    connect_data->discovered_cb(connect_data->user_data, (void *)true);
  } else {
    GError *err = dbus_adapter_start_discovery(connect_data->adapter_proxy);
    if (err) {
      g_warning("start discovery says %s", err->message);
      g_error_free(err);
    }

    // Clear old timeout
    connect_data->discover_timeout_id = 
      clear_timeout(connect_data->discover_timeout_id);

    // Build new timeout
    connect_data->discover_timeout_id = create_timeout(
      get_timeout_value(ENV_DISCOVER_TIMEOUT),
      discover_timeout,
      connect_data);
  } /* else */
}

void discover_stop(connect_data_t *connect_data) {
  GError *err = dbus_adapter_stop_discovery(connect_data->adapter_proxy);
  if (err) {
    g_warning("stop discovery says: %s", err->message);
    g_error_free(err);
  }
}

void connect_to_device(connect_data_t *connect_data) {
  GError *err = dbus_adapter_start_service_discovery(
    connect_data->adapter_proxy);
  if (err) {
    g_warning("start discovery says %s", err->message);
    g_error_free(err);
  }

  err = dbus_device_connect(connect_data->device_proxy);
  if (err) {
    g_warning("connect says %s", err->message);
    g_error_free(err);
  }

  if (0 == get_timeout_value(ENV_WAIT_FOR_CONNECTION))
    connect_data->connect_guard_time_passed = true;
  else
    connect_data->connect_guard_time_passed = false;

  if (!dbus_object_exists(connect_data->service_proxy)) {
    g_warning("LOOKS LIKE SERVICES ARE MISSING!");

    if (dbus_device_is_connected(connect_data->device_proxy)) {
      GError *error = NULL;
      GVariant *tuple = NULL;

      tuple = g_dbus_proxy_call_sync(
        connect_data->device_proxy,
        "org.freedesktop.DBus.Properties.Get",
        g_variant_new("(ss)", "org.bluez.Device1", "GattServices"),
        G_DBUS_CALL_FLAGS_NONE,
        500, // If this isn't set to some small number of ms, we miss events
        NULL,
        &error);

      if (tuple) {
        GVariant *gv_services = NULL;
        g_variant_get(tuple, "(v)", &gv_services);

        find_august_service(gv_services, connect_data);

        g_variant_unref(gv_services);
        g_variant_unref(tuple);
      }

      if (error) {
        g_warning("%s %s", __FUNCTION__, error->message);
        g_error_free(error);

        dbus_adapter_start_service_discovery(connect_data->adapter_proxy);
        connect_data->service_discovered = false;
      } else {
        // Set the disconnect timer. If we don't get a disconnect before
        // the timer expires then we are actually connected.
        if (get_timeout_value(ENV_WAIT_FOR_CONNECTION)) {
          connect_data->disconnect_timeout_id = create_timeout(
            get_timeout_value(ENV_WAIT_FOR_CONNECTION),
            disconnect_timeout,
            connect_data);
        } else {
          disconnect_timeout(connect_data);
        }
      }
    } else {
      dbus_adapter_start_service_discovery(connect_data->adapter_proxy);
      connect_data->service_discovered = false;
    }
  } else {
    //connect_data->service_discovered = true;
    if (!connect_data->service_discovered) {
      g_warning("looking for august chars from connect handler\n");
      connect_data->service_discovered = 
        dbus_service_find_august_characteristics(
          connect_data->service_proxy);
    } /* if */
  } /* if */

  // Clear old timeout
  connect_data->connect_timeout_id =
    clear_timeout(connect_data->connect_timeout_id);

  // Build new timeout
  connect_data->connect_timeout_id = create_timeout(
    (connect_data->service_discovered ? get_timeout_value(ENV_CONNECT_TIMEOUT):
      get_timeout_value(ENV_DISCOVER_TIMEOUT)),
    connect_timeout,
    connect_data);
}

void disconnect_from_device(connect_data_t *connect_data) {
  GError *err = dbus_device_disconnect(connect_data->device_proxy);
  if (err) {
    g_warning("disconnect says %s", err->message);
    g_error_free(err);
  }
}


void remove_device(connect_data_t *connect_data) {
  const char *path = g_dbus_proxy_get_object_path(connect_data->device_proxy);
  GError *err = dbus_adapter_remove_device(connect_data->adapter_proxy, path);
  if (err) {
    g_warning("remove device says: %s", err->message);
    g_error_free(err);
  }

  // Clear old timeout
  connect_data->remove_timeout_id =
    clear_timeout(connect_data->remove_timeout_id);

  // Build new timeout
  connect_data->remove_timeout_id = create_timeout(
    get_timeout_value(ENV_REMOVE_TIMEOUT),
    remove_timeout,
    connect_data);
}

int get_rssi(connect_data_t *connect_data) {
  int rssi = 0;
  GVariant *val = NULL;

  if (connect_data) {
    val = g_dbus_proxy_get_cached_property(connect_data->device_proxy, "RSSI");
  }

  if (val) {
    rssi = g_variant_get_int16(val);
    g_variant_unref(val);
  }

  return rssi;
}
