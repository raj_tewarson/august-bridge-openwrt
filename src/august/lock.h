#ifndef __LOCK__
#define __LOCK__

#include <json.h>
#include <gio/gio.h>
#include <stdbool.h>

#include "august/comm.h"
#include "august/connect.h"
#include "august/scan.h"

typedef enum {
  LOCK_STATE_IDLE,                 // Waiting for commands
  LOCK_STATE_SCANNING,             // Get blescand to help us find the lock id
  LOCK_STATE_DISCOVERING,          // Getting bluez to find the lock
  LOCK_STATE_CONNECTING,           // Ready to connect
  LOCK_STATE_REGISTERING_SECURE,   // Registered for secure notifications
  LOCK_STATE_REGISTERING_DATA,     // Registered for data notifications
  LOCK_STATE_READY,                // Ready to issue write commands
  LOCK_STATE_REMOVING,             // Removing device from cache
  LOCK_STATE_CACHING,              // Discover prior to any connect attempts
  LOCK_STATE_ERROR,
} lock_state_t;

typedef enum {
  LOCK_EVENT_REGISTER,               // Register new lock
  LOCK_EVENT_INITIATE_SESSION,       // Start lock connection
  LOCK_EVENT_REBOOT,                 // Trigger bridge reboot

  LOCK_EVENT_SCAN,                   // Scan for lock id
  LOCK_EVENT_SCAN_HIT,               // Scan succeeded
  LOCK_EVENT_SCAN_TIMEOUT,           // Scan failed to complete
  LOCK_EVENT_DISCOVER,               // Discover command
  LOCK_EVENT_DISCOVER_HIT,           // Discovery succeeded
  LOCK_EVENT_DISCOVER_TIMEOUT,       // Discovery failed to complete
  LOCK_EVENT_CONNECT,                // Connect to device
  LOCK_EVENT_CONNECT_HIT,            // Connected to device
  LOCK_EVENT_CONNECT_TIMEOUT,        // Connect failed to complete
  LOCK_EVENT_REGISTER_SECURE,        // Register for notifications on secure
  LOCK_EVENT_REGISTER_SECURE_HIT,    // Registration complete for secure
  LOCK_EVENT_REGISTER_SECURE_TIMEOUT,// Registration timeout for secure
  LOCK_EVENT_REGISTER_DATA,          // Register for notifications on data
  LOCK_EVENT_REGISTER_DATA_HIT,      // Registration complete for data
  LOCK_EVENT_REGISTER_DATA_TIMEOUT,  // Registration timeout for data

  LOCK_EVENT_READY,                  // Ready to accept commands
  LOCK_EVENT_WRITE_SECURE,           // Write to secure characteristic
  LOCK_EVENT_WRITE_SECURE_ERROR,
  LOCK_EVENT_WRITE_DATA,             // Write to data characteristic
  LOCK_EVENT_WRITE_DATA_ERROR,

  LOCK_EVENT_DISCONNECT,             // Disconnected from device
  LOCK_EVENT_REMOVE,                 // Remove from cache
  LOCK_EVENT_REMOVE_HIT,             // Remove successful
  LOCK_EVENT_REMOVE_TIMEOUT,         // Remove timed out
  LOCK_EVENT_CONNECT_WRITE,

  LOCK_EVENT_ERROR,
} lock_event_t;

typedef void (*lock_callback_t)(json_object *response);

typedef struct {
  lock_callback_t register_cb;
  lock_callback_t connect_cb;
  lock_callback_t disconnect_cb;
  lock_callback_t read_secure_cb;
  lock_callback_t read_data_cb;
  lock_callback_t dirty_bit_cb;
  lock_callback_t reboot_cb;
  lock_callback_t error_cb;
} lock_funcs_t;

typedef struct {
  lock_state_t state;
  lock_funcs_t callbacks;
  int connect_tries;
  int rssi;
  bool security_state_clean;

  bool use_bluez_cache;

  uint8_t lock_gen;
  char *bt_address;
  const char *hci_device;
  const char *lock_id;

  comm_data_t *comm_data;
  connect_data_t *connect_data;
  scan_data_t *scan_data;

  time_t last_disconnect_time;
  
  bool dirty_video;
} lock_t;

// Init and free
void lock_init(
    lock_funcs_t *callbacks,
    uint8_t lock_gen,
    const char *bt_address,
    const char *hci_device,
    const char *lock_id,
    bool use_bluez_cache,
    bool dirty_video);
void lock_cleanup(void);
void lock_free(void);

// Lock commands
void lock_next(json_object *command);

#endif /* LOCK */
