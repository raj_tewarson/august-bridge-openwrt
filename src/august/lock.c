#include <errno.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>

#include "august/lock.h"

#include "utils/environ.h"
#include "utils/json.h"
#include "utils/string.h"
#include "utils/dbus.h"

#define TRANSITION(X)   (lock.state = (X))

#ifndef ADVERTISING_TIMEOUT_COMMAND
#define ADVERTISING_TIMEOUT_COMMAND "/usr/bin/systemctl restart bluetooth"
#endif

static lock_t lock;

static int list_sock_fd = -1;

static void lock_step(lock_event_t event, json_object *command);

static char *strlockstate(lock_state_t lock_state) {
  switch (lock_state) {
    case LOCK_STATE_IDLE:
      return "LOCK_STATE_IDLE";
    case LOCK_STATE_SCANNING:
      return "LOCK_STATE_SCANNING";
    case LOCK_STATE_DISCOVERING:
      return "LOCK_STATE_DISCOVERING";
    case LOCK_STATE_CONNECTING:
      return "LOCK_STATE_CONNECTING";
    case LOCK_STATE_REGISTERING_SECURE:
      return "LOCK_STATE_REGISTERING_SECURE";
    case LOCK_STATE_REGISTERING_DATA:
      return "LOCK_STATE_REGISTERING_DATA";
    case LOCK_STATE_READY:
      return "LOCK_STATE_READY";
    case LOCK_STATE_REMOVING:
      return "LOCK_STATE_REMOVING";
    case LOCK_STATE_CACHING:
      return "LOCK_STATE_CACHING";
    case LOCK_STATE_ERROR:
      return "LOCK_STATE_ERROR";
    default:
      return "UNKNOWN";
  }
}

static char *strlockevent(lock_event_t lock_event) {
  switch (lock_event) {
    case LOCK_EVENT_REGISTER:
      return "LOCK_EVENT_REGISTER";
    case LOCK_EVENT_INITIATE_SESSION:
      return "LOCK_EVENT_INITIATE_SESSION";
    case LOCK_EVENT_REBOOT:
      return "LOCK_EVENT_REBOOT";
    case LOCK_EVENT_SCAN:
      return "LOCK_EVENT_SCAN";
    case LOCK_EVENT_SCAN_HIT:
      return "LOCK_EVENT_SCAN_HIT";
    case LOCK_EVENT_SCAN_TIMEOUT:
      return "LOCK_EVENT_SCAN_TIMEOUT";
    case LOCK_EVENT_DISCOVER:
      return "LOCK_EVENT_DISCOVER";
    case LOCK_EVENT_DISCOVER_HIT:
      return "LOCK_EVENT_DISCOVER_HIT";
    case LOCK_EVENT_DISCOVER_TIMEOUT:
      return "LOCK_EVENT_DISCOVER_TIMEOUT";
    case LOCK_EVENT_CONNECT:
      return "LOCK_EVENT_CONNECT";
    case LOCK_EVENT_CONNECT_HIT:
      return "LOCK_EVENT_CONNECT_HIT";
    case LOCK_EVENT_CONNECT_TIMEOUT:
      return "LOCK_EVENT_CONNECT_TIMEOUT";
    case LOCK_EVENT_REGISTER_SECURE:
      return "LOCK_EVENT_REGISTER_SECURE";
    case LOCK_EVENT_REGISTER_SECURE_HIT:
      return "LOCK_EVENT_REGISTER_SECURE_HIT";
    case LOCK_EVENT_REGISTER_SECURE_TIMEOUT:
      return "LOCK_EVENT_REGISTER_SECURE_TIMEOUT";
    case LOCK_EVENT_REGISTER_DATA:
      return "LOCK_EVENT_REGISTER_DATA";
    case LOCK_EVENT_REGISTER_DATA_HIT:
      return "LOCK_EVENT_REGISTER_DATA_HIT";
    case LOCK_EVENT_REGISTER_DATA_TIMEOUT:
      return "LOCK_EVENT_REGISTER_DATA_TIMEOUT";
    case LOCK_EVENT_READY:
      return "LOCK_EVENT_READY";
    case LOCK_EVENT_WRITE_SECURE:
      return "LOCK_EVENT_WRITE_SECURE";
    case LOCK_EVENT_WRITE_SECURE_ERROR:
      return "LOCK_EVENT_WRITE_SECURE_ERROR";
    case LOCK_EVENT_WRITE_DATA:
      return "LOCK_EVENT_WRITE_DATA";
    case LOCK_EVENT_WRITE_DATA_ERROR:
      return "LOCK_EVENT_WRITE_DATA_ERROR";
    case LOCK_EVENT_DISCONNECT:
      return "LOCK_EVENT_DISCONNECT";
    case LOCK_EVENT_REMOVE:
      return "LOCK_EVENT_REMOVE";
    case LOCK_EVENT_REMOVE_HIT:
      return "LOCK_EVENT_REMOVE_HIT";
    case LOCK_EVENT_REMOVE_TIMEOUT:
      return "LOCK_EVENT_REMOVE_TIMEOUT";
    case LOCK_EVENT_ERROR:
      return "LOCK_EVENT_ERROR";
    case LOCK_EVENT_CONNECT_WRITE:
      return "LOCK_EVENT_CONNECT_WRITE";
    default:
      return "UNKNOWN_EVENT";
  }
}


// Called on Connect to verify we're looking for the right lock
static void update_lock_id(json_object *command)
{
  const char *lock_id = json_peek_string(command, "lockID");

  // we have a lock id but rbs is telling us a different one
  if(lock.lock_id && lock_id && (strcmp(lock.lock_id, lock_id) != 0)) {
    unlink(get_lock_gen_path());
    lock.lock_gen = 0;

    unlink(get_lock_mac_path());
    if(lock.bt_address) {
      free(lock.bt_address);
      lock.bt_address = NULL;
    }

    unlink(get_lock_id_path());
    if(lock.lock_id) {
      free(lock.lock_id);
      lock.lock_id = string_clone(lock_id);
    }

    if(lock.scan_data) {
      scan_data_free(lock.scan_data);
      lock.scan_data = NULL;
    }

    if(lock.connect_data) {
      connect_data_free(lock.connect_data);
      lock.connect_data = NULL;
    }
  }
}


static void scan_callback(void *user_data, void *param, uint8_t lock_gen) {
  if (param) {
    lock.lock_gen = lock_gen;
    lock.bt_address = (char *)param;
    lock_step(LOCK_EVENT_SCAN_HIT, NULL);
  } else {
    lock_step(LOCK_EVENT_SCAN_TIMEOUT, NULL);
  }
}

static void discovered_callback(void *user_data, void *param) {
  if (param) {
    lock_step(LOCK_EVENT_DISCOVER_HIT, NULL);
  } else {
    lock_step(LOCK_EVENT_DISCOVER_TIMEOUT, NULL);
  }
}

static void connected_callback(void *user_data, void *param) {
  if (param) {
    lock_step(LOCK_EVENT_CONNECT_HIT, NULL);
  } else {
    lock_step(LOCK_EVENT_CONNECT_TIMEOUT, NULL);
  }
}

static void registered_secure_callback(void *user_data, void *param) {
  if (param) {
    lock_step(LOCK_EVENT_REGISTER_SECURE_HIT, NULL);
  } else {
    lock_step(LOCK_EVENT_REGISTER_SECURE_TIMEOUT, NULL);
  }
}

static void registered_data_callback(void *user_data, void *param) {
  if (param) {
    lock_step(LOCK_EVENT_REGISTER_DATA_HIT, NULL);
  } else {
    lock_step(LOCK_EVENT_REGISTER_DATA_TIMEOUT, NULL);
  }
}

static void read_secure_callback(void *user_data, void *param) {
  const char *data = (const char *)param;

  if (data) {
    lock.callbacks.read_secure_cb(json_data_secure(lock.lock_id, data));
  } else {
    lock_step(LOCK_EVENT_WRITE_SECURE_ERROR, NULL);
  }
}

static void read_data_callback(void *user_data, void *param) {
  const char *data = (const char *)param;

  if (data) {
    lock.callbacks.read_data_cb(json_data(lock.lock_id, data));
  } else {
    lock_step(LOCK_EVENT_WRITE_DATA_ERROR, NULL);
  }
}

static void disconnected_callback(void *user_data, void *param) {
  if ((LOCK_STATE_IDLE < lock.state) &&
      (LOCK_STATE_READY > lock.state) &&
     (++lock.connect_tries < get_connect_retries())) {
    TRANSITION(LOCK_STATE_IDLE);
    lock_step(LOCK_EVENT_INITIATE_SESSION, NULL);
   } else {
    lock.connect_tries = 0;
    lock_step(LOCK_EVENT_REMOVE, NULL);
  }
}

static void removed_callback(void *user_data, void *param) {
  if (param) {
    lock_step(LOCK_EVENT_REMOVE_HIT, NULL);
  } else {
    lock_step(LOCK_EVENT_REMOVE_TIMEOUT, NULL);
  }
}
/*
 * open_socket_to_listener
 *
 * opens a socket to the listener
 * returns nonzero if anything goes wrong
 */
static int open_listener_socket() {
  int res = 0;
  list_sock_fd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if (list_sock_fd < 0) {
    g_debug("Unable to open a socket to the listener\n");
    res = -1;
  } else {
    struct sockaddr_un address = {0};

    address.sun_family = AF_UNIX;
    snprintf(address.sun_path, 100, "/tmp/pubnub.socket");

    if (connect(list_sock_fd, (struct sockaddr *)&address,
          sizeof(struct sockaddr_un)) != 0) {
      close(list_sock_fd);
      g_debug("Unable to connect to listener socket\n");
      res = -1;
    }
  }
  return res;
} /* open_socket_to_listener */

/*
 * send_pubnub_message
 */
int send_pubnub_message(const char *msg) {
    int retries = 3;
    int failed = 0;

    if(!msg)
       return 0;

    int str_len = strlen(msg);

    do {
      failed = write(list_sock_fd, msg, str_len) < str_len;
      if (failed) {
        g_debug("Unable to talk to listener, cannot publish\n");
        close(list_sock_fd);
        open_listener_socket();
      }
      retries--;
    } while (failed && retries);

    /* after 3 retries, give up for good */
    if (failed && !retries){
      g_debug("send_pubnub_message FAILED!!!\n");
      return 1;
    }else{
      g_debug("send_pubnub_message complete!\n");
      return 0; 
    }
} /* send_pubnub_message */

static bool dirty_bit_callback(void *user_data, uint32_t param)
{
  g_debug("%s(%d) %d", __FUNCTION__, param, lock.state);

  if (LOCK_STATE_IDLE != lock.state)
    return false; 

  switch((uint32_t)param) {
    case SCAN_ADV_TYPE_IDLE:
      lock.callbacks.dirty_bit_cb(json_idle(lock.lock_id));
      break;
    case SCAN_ADV_TYPE_DIRTY:
      if (lock.dirty_video)
        send_pubnub_message("{\"source\":\"bridge\","
          "\"status\":\"dirty\"}");
      lock.callbacks.dirty_bit_cb(json_dirty(lock.lock_id));
      break;
    case SCAN_ADV_TYPE_BUSY:
      lock.callbacks.dirty_bit_cb(json_busy(lock.lock_id));
      break;
    case SCAN_ADV_TYPE_TIMEOUT:
      lock.callbacks.dirty_bit_cb(json_timeout(lock.lock_id));
      /*
       * HACK:
       * If the advertisement timeout happens it may be because we can't
       * discover the device we're looking for. In that case we're going
       * to proactively restart the bluetooth daemon on the device.
       *
       */
      system(ADVERTISING_TIMEOUT_COMMAND);
      break;
    default:
      break;
  }
  return true;
}

static inline void check_lock_id(json_object *command) {
  json_object_get(command);
  if (!lock.lock_id) {
    lock.lock_id = json_clone_string_value(command, "lockID");
  }
  json_object_put(command);
}

static void
we_use_colonic_mac_addresses(
  char *bt_address) {

  /* Convert MAC address to uppercase for string comparison */
  for (char *c = bt_address; ; c++) {
    if (c == NULL || *c == '\0')
      break;
    else if (*c == '.')
      *c = ':';
    else if (islower(*c))
      *c = toupper(*c);
  } /* for */
} /* we_use_colonic_mac_addresses */

static inline void check_bt_address(json_object *command) {
  json_object_get(command);
  if (!lock.bt_address) {

    lock.bt_address = (char*)json_clone_string_value(command, "macAddr");

    we_use_colonic_mac_addresses(lock.bt_address);
  }
  json_object_put(command);
}

static inline void check_scan_data() {
  g_debug("%s", __FUNCTION__);
  // bt_address is optional here
  if (!lock.scan_data && lock.lock_id) {
    lock.scan_data = scan_data_new(
      lock.bt_address,
      lock.lock_id,
      scan_callback,
      dirty_bit_callback,
      (void *)&lock);
  }
}

static inline void check_connect_data(void) {
  g_debug("%s", __FUNCTION__);

  if (!lock.connect_data && lock.bt_address && lock.lock_id) {
    lock.connect_data = connect_data_new(
      lock.lock_gen,
      lock.bt_address,
      lock.hci_device,
      lock.lock_id,
      discovered_callback,
      connected_callback,
      disconnected_callback,
      removed_callback,
      (void *)&lock);
  }
}

static inline void check_comm_data(void) {
  g_debug("%s", __FUNCTION__);

  if (!lock.comm_data && lock.bt_address && lock.lock_id) {
    lock.comm_data = comm_data_new(
      lock.lock_gen,
      lock.bt_address,
      lock.hci_device,
      registered_secure_callback,
      read_secure_callback,
      registered_data_callback,
      read_data_callback,
      (void *)&lock);
  }
}


static bool do_connect_write = false;
static json_object *connect_write_cmd = NULL;


static void lock_step(lock_event_t event, json_object *command) {
  g_debug("state: %s, event: %s", strlockstate(lock.state), strlockevent(event));

  if (lock.connect_data) {
    int new_rssi = get_rssi(lock.connect_data);
    if (new_rssi && (lock.rssi != new_rssi)) {
      g_debug("RSSI: %d", new_rssi);
      lock.rssi = new_rssi;
    }
  }

  switch (event) {

/*
 * META
 */
    case LOCK_EVENT_REGISTER:
      {
        lock_cleanup();
        check_lock_id(command);
        check_bt_address(command);
        check_scan_data();
        check_connect_data();

        /* XXX do not call check_comm_data() here!
         * bad stuff happens if characteristics haven't been discovered yet.
         */

        if (lock.use_bluez_cache) {
          lock.state = LOCK_STATE_CACHING;
          lock_step(LOCK_EVENT_DISCOVER, NULL);
        } else {
          scan_start_dirty_bit(lock.scan_data);
        }

        if (strcmp(json_peek_string(command, "lockID"), lock.lock_id) != 0) {
          lock.callbacks.register_cb(command);
        }
        json_object_put(command);
      }
      break;
    case LOCK_EVENT_CONNECT_WRITE:
      do_connect_write  = true;
      connect_write_cmd = command;
      int32_t seq_num = json_peek_int(command, "sequenceNumber");
      g_message("seq_num = %d\n", seq_num);
      json_set_sequence_num(seq_num);
    case LOCK_EVENT_INITIATE_SESSION:
      {
        if (LOCK_STATE_READY == lock.state) {
          g_debug("already connected");
          
          if(command) {

            if(do_connect_write) {
              lock_step(LOCK_EVENT_WRITE_SECURE, connect_write_cmd);
              break;
            }

            lock.callbacks.connect_cb(json_connection_state(lock.lock_id,
              JSON_CONN_STATE_ALREADY_CONNECTED, NULL));

            if (lock.security_state_clean) {
              lock.callbacks.connect_cb(json_connection_state(lock.lock_id,
                JSON_CONN_STATE_CONNECTED, &lock.rssi));
              json_object_put(command);
            } else {
              TRANSITION(LOCK_STATE_ERROR);
              lock_step(LOCK_EVENT_DISCONNECT, command);
            } /* if */
          } /* if */
          break;
        } else
          /**
           * MARS-1740, MARS-1760
           *
           * I think there are situations that cause connections to fail more
           * often with the idle callback. The queue latency used to make us
           * more robust against service discovery not being completed in a
           * timely fashion, now we rely more heavily on this retry mechanism.
           *
           * We don't want to reset the state machine when we've already
           * discovered. We just want to retry our connection.
           */
          if ((LOCK_STATE_CONNECTING == lock.state) &&
              (lock.connect_tries < get_connect_retries())) {
          g_debug("retrying connect");
          scan_stop(lock.scan_data);
          connect_to_device(lock.connect_data);
          break;
        }

        if (!lock.bt_address) {
          lock_step(LOCK_EVENT_SCAN, command);
        } else {
          if (LOCK_STATE_CACHING == lock.state)
            /*
             * Special Case: 
             *
             * If the doorbell is currently in the middle of caching and it
             * gets a connect command we need to make sure the doorbell goes
             * through the full connection dance, not just the caching dance.
             *
             * This case will generally come after a LOCK_EVENT_DISCOVER, but
             * before LOCK_EVENT_DISCOVER_HIT or LOCK_EVENT_DISCOVER_TIMEOUT.
             * That's why we don't step the state machine here, our command
             * has already dispatched.
             *
             */
            lock.state = LOCK_STATE_DISCOVERING;
          else
            lock_step(LOCK_EVENT_DISCOVER, command);
        }
         
        if(command)
          lock.callbacks.connect_cb(json_connection_state(lock.lock_id,
            JSON_CONN_STATE_CONNECTING, NULL));

        if(!do_connect_write)
          json_object_put(command);
      }
      break;
    case LOCK_EVENT_REBOOT:
      {
        lock_step(LOCK_EVENT_DISCONNECT, command);
        lock.callbacks.reboot_cb(command);
        json_object_put(command);
      }
      break;

/*
 * CONNECTION
 */
    case LOCK_EVENT_SCAN:
      if (LOCK_STATE_IDLE == lock.state) {
        check_lock_id(command);
        check_scan_data();
        TRANSITION(LOCK_STATE_SCANNING);
        scan_start_lock_id(lock.scan_data);
        json_object_put(command);
      }
      break;
    case LOCK_EVENT_SCAN_HIT:
      if (LOCK_STATE_SCANNING == lock.state) {
        {
          /* We can register here */
          json_object *command = json_object_new_object();
          json_object_object_add(command, "lockID",
              json_object_new_string(lock.lock_id));

          if (lock.lock_gen) {
            char mac_plus_gen[20];

            strcpy(mac_plus_gen, lock.scan_data->bt_address);
            mac_plus_gen[17] = ',';
            mac_plus_gen[18] = lock.lock_gen + 0x30;
            mac_plus_gen[19] = '\0';  

            json_object_object_add(command, "macAddr",
                json_object_new_string(mac_plus_gen));
          } else
            json_object_object_add(command, "macAddr",
                json_object_new_string(lock.scan_data->bt_address));

          lock.callbacks.register_cb(command);
          json_object_put(command);
        }
        scan_start_dirty_bit(lock.scan_data);
        lock_step(LOCK_EVENT_DISCOVER, NULL);
      }
      break;
    case LOCK_EVENT_SCAN_TIMEOUT:
      if (LOCK_STATE_SCANNING == lock.state) {
        scan_start_dirty_bit(lock.scan_data);
        TRANSITION(LOCK_STATE_IDLE);
      }
      break;
    case LOCK_EVENT_DISCOVER:
      if ((LOCK_STATE_IDLE == lock.state) ||
          (LOCK_STATE_SCANNING == lock.state) ||
          (LOCK_STATE_CACHING == lock.state)) {
        check_scan_data();
        check_connect_data();
        if (LOCK_STATE_CACHING != lock.state)
          TRANSITION(LOCK_STATE_DISCOVERING);
        discover_device(lock.connect_data);
      }
      break;
    case LOCK_EVENT_DISCOVER_HIT:
      if (LOCK_STATE_DISCOVERING == lock.state) {
        discover_stop(lock.connect_data);
        lock_step(LOCK_EVENT_CONNECT, NULL);
      } else 
      if (LOCK_STATE_CACHING == lock.state) {
        discover_stop(lock.connect_data);
        TRANSITION(LOCK_STATE_IDLE);
        check_scan_data();
        scan_start_dirty_bit(lock.scan_data);
      }
      break;
    case LOCK_EVENT_DISCOVER_TIMEOUT:
      if ((LOCK_STATE_DISCOVERING == lock.state) ||
          (LOCK_STATE_CACHING == lock.state)) {
        discover_stop(lock.connect_data);
        remove_device(lock.connect_data);

        check_scan_data();
        scan_start_dirty_bit(lock.scan_data);

        // TODO: Decide if BlueZ needs to be reinitialized if we can't find the
        //       device. Maybe just an `hciconfig hci reset` will do, maybe
        //       changing the scan parameters, maybe a full restart of the app
        //       and stack. Don't know yet.
      }
      break;
    case LOCK_EVENT_CONNECT:
      if (LOCK_STATE_DISCOVERING == lock.state) {
        TRANSITION(LOCK_STATE_CONNECTING);
        scan_stop(lock.scan_data);
        connect_to_device(lock.connect_data);
      }
      break;
    case LOCK_EVENT_CONNECT_HIT:
      if (LOCK_STATE_CONNECTING == lock.state) {
        lock.connect_tries = 0;
        lock_step(LOCK_EVENT_REGISTER_SECURE, NULL);
      }
      break;
    case LOCK_EVENT_CONNECT_TIMEOUT:
      if (LOCK_STATE_CONNECTING == lock.state) {
        if (++lock.connect_tries < get_connect_retries()) {
          lock_step(LOCK_EVENT_INITIATE_SESSION, NULL);
        } else {
          lock.connect_tries = 0;
          disconnect_from_device(lock.connect_data);
          lock_step(LOCK_EVENT_REMOVE, NULL);
        }
      }
      break;
    case LOCK_EVENT_REGISTER_SECURE:
      if (LOCK_STATE_CONNECTING == lock.state) {
        check_comm_data();
        TRANSITION(LOCK_STATE_REGISTERING_SECURE);
        comm_register_secure(lock.comm_data, lock.use_bluez_cache);
      }

      break;
    case LOCK_EVENT_REGISTER_SECURE_HIT:
      if (LOCK_STATE_REGISTERING_SECURE == lock.state) {
        lock_step(LOCK_EVENT_REGISTER_DATA, NULL);
      }


      break;
    case LOCK_EVENT_REGISTER_SECURE_TIMEOUT:
      if (LOCK_STATE_REGISTERING_SECURE == lock.state) {
        disconnect_from_device(lock.connect_data);
      }
      break;
    case LOCK_EVENT_REGISTER_DATA:
      if (LOCK_STATE_REGISTERING_SECURE == lock.state) {
        check_comm_data();
        TRANSITION(LOCK_STATE_REGISTERING_DATA);
        comm_register_data(lock.comm_data, lock.use_bluez_cache);
      }
      break;
    case LOCK_EVENT_REGISTER_DATA_HIT:
      if (LOCK_STATE_REGISTERING_DATA == lock.state) {
        lock_step(LOCK_EVENT_READY, NULL);
      }
      break;
    case LOCK_EVENT_REGISTER_DATA_TIMEOUT:
      if (LOCK_STATE_REGISTERING_DATA == lock.state) {
        disconnect_from_device(lock.connect_data);
      }
      break;
    case LOCK_EVENT_READY:
      if (LOCK_STATE_REGISTERING_DATA == lock.state) {
        TRANSITION(LOCK_STATE_READY);
        lock.callbacks.connect_cb(json_connection_state(lock.lock_id,
              JSON_CONN_STATE_CONNECTED, &lock.rssi));
      }

      if(do_connect_write) {
        lock_step(LOCK_EVENT_WRITE_SECURE, connect_write_cmd);
      }

      break;

/*
 * COMMUNICATION
 */
    case LOCK_EVENT_WRITE_SECURE:
      if (LOCK_STATE_READY == lock.state) {
        const char *data = json_peek_string(command, "data");
        lock.security_state_clean = false;
        comm_write_secure(lock.comm_data, data);
        json_object_put(command);
      } else {
        lock.callbacks.disconnect_cb(json_connection_state(lock.lock_id,
            JSON_CONN_STATE_DISCONNECTED, NULL));
      }
      do_connect_write = false;
      break;
    case LOCK_EVENT_WRITE_DATA:
      if (LOCK_STATE_READY == lock.state) {
        const char *data = json_peek_string(command, "data");
        comm_write_data(lock.comm_data, data);
        json_object_put(command);
      } else {
        lock.callbacks.disconnect_cb(json_connection_state(lock.lock_id,
            JSON_CONN_STATE_DISCONNECTED, NULL));
      }
      break;
    case LOCK_EVENT_WRITE_DATA_ERROR:
    case LOCK_EVENT_WRITE_SECURE_ERROR:
      disconnect_from_device(lock.connect_data);
      comm_data_reset(lock.comm_data);
      lock.security_state_clean = true;
      json_object_put(command);
      lock_step(LOCK_EVENT_REMOVE, NULL);
      break;

/*
 * DISCONNECT
 */
    case LOCK_EVENT_DISCONNECT:
      if ((LOCK_STATE_CONNECTING == lock.state) ||
          (LOCK_STATE_REGISTERING_SECURE == lock.state) ||
          (LOCK_STATE_REGISTERING_DATA == lock.state) ||
          (LOCK_STATE_READY == lock.state)) {
        disconnect_from_device(lock.connect_data);
        comm_data_reset(lock.comm_data);
      } else {
        g_debug("already disconnected");
        if (difftime(time(NULL), lock.last_disconnect_time) >
            get_timeout_value(ENV_GRATUITOUS_DISCONNECT_GUARD_TIME)) {
          lock.callbacks.disconnect_cb(json_connection_state(lock.lock_id,
            JSON_CONN_STATE_DISCONNECTED, NULL));
          lock.last_disconnect_time = time(NULL);
        } /* if */
      }

      lock.security_state_clean = true;

      // NB: This may result in a double lock remove, but that's not a big deal.
      //     BlueZ will warn us and we'll ignore it.
      json_object_put(command);
      lock_step(LOCK_EVENT_REMOVE, NULL);
      break;
    case LOCK_EVENT_REMOVE:
      if (LOCK_STATE_IDLE != lock.state) {

        if (lock.use_bluez_cache) {
          lock_step(LOCK_EVENT_REMOVE_HIT, NULL);
        } else {
          TRANSITION(LOCK_STATE_REMOVING);
          if (lock.connect_data)
            remove_device(lock.connect_data);
        } /* else */
      }
      break;
    case LOCK_EVENT_REMOVE_TIMEOUT:
      // Intentional fall through
    case LOCK_EVENT_REMOVE_HIT:
      TRANSITION(LOCK_STATE_IDLE);
      lock.callbacks.disconnect_cb(json_connection_state(lock.lock_id,
            JSON_CONN_STATE_DISCONNECTED, NULL));
      lock.last_disconnect_time = time(NULL);
      lock.security_state_clean = true;
      scan_start_dirty_bit(lock.scan_data);
      break;

/*
 * ERROR
 */
    default:
      break;
  }
}

void lock_init(
    lock_funcs_t *callbacks,
    uint8_t lock_gen,
    const char *bt_address,
    const char *hci_device,
    const char *lock_id,
    bool use_bluez_cache, 
    bool dirty_video) {

  memcpy(&(lock.callbacks), callbacks, sizeof(lock_funcs_t));

  lock.state = LOCK_STATE_IDLE;
  lock.use_bluez_cache = use_bluez_cache;
  lock.lock_gen = lock_gen;
  lock.bt_address = (char*)string_clone(bt_address);
  lock.dirty_video = dirty_video;
  we_use_colonic_mac_addresses(lock.bt_address);

  lock.hci_device = string_clone(hci_device);
  lock.lock_id = string_clone(lock_id);

  if (lock.bt_address) {
    if (lock.use_bluez_cache) {
      lock.state = LOCK_STATE_CACHING;
      lock_step(LOCK_EVENT_DISCOVER, NULL);
    } else {
      check_scan_data();
      scan_start_dirty_bit(lock.scan_data);
    }
  }

  lock.last_disconnect_time = 0;
  lock.rssi = 0;
  lock.security_state_clean = true;
}

void lock_cleanup(void) {
  if (lock.comm_data) {
    comm_data_free(lock.comm_data);
    lock.comm_data = NULL;
  }

  if (lock.connect_data) {
    connect_data_free(lock.connect_data);
    lock.connect_data = NULL;
  }

  if (lock.scan_data) {
    scan_data_free(lock.scan_data);
    lock.scan_data = NULL;
  }

  if (lock.bt_address) {
    free((void *)lock.bt_address);
    lock.bt_address = NULL;
  }

  if (lock.lock_id) {
    free((void *)lock.lock_id);
    lock.lock_id = NULL;
  }

  // Don't clear the hci device, or the callbacks because we'll need them again.
}

void lock_free(void) {
  lock_cleanup();
  if (lock.hci_device) {
    free((void *)lock.hci_device);
    lock.hci_device = NULL;
  }
}

void lock_next(json_object *command) {
  

  switch (json_command_type(command)) {
    case JSON_REGISTER_COMMAND:
      lock_step(LOCK_EVENT_REGISTER, command);
      break;
    case JSON_CONNECT_COMMAND:
      update_lock_id(command);
      lock_step(LOCK_EVENT_INITIATE_SESSION, command);
      break;
    case JSON_WRITE_SECURE_COMMAND:
      lock_step(LOCK_EVENT_WRITE_SECURE, command);
      break;
    case JSON_WRITE_DATA_COMMAND:
      lock_step(LOCK_EVENT_WRITE_DATA, command);
      break;
    case JSON_DISCONNECT_COMMAND:
      lock_step(LOCK_EVENT_DISCONNECT, command);
      break;
    case JSON_REBOOT_COMMAND:
      lock_step(LOCK_EVENT_REBOOT, command);
      break;
    case JSON_CONNECT_WRITE_COMMAND:
      lock_step(LOCK_EVENT_CONNECT_WRITE, command);
      break;
    default:
      break;
  }
}
