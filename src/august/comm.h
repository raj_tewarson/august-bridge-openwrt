#ifndef __LOCK_COMM_H__
#define __LOCK_COMM_H__

#include <gio/gio.h>
#include <stdbool.h>

typedef void (*comm_callback_t)(void *user_data, void *param);

typedef struct {
  // Glib callback handlers
  unsigned long register_secure_handler_id;
  unsigned long register_secure_timeout_id;

  unsigned long register_data_handler_id;
  unsigned long register_data_timeout_id;

  unsigned long read_secure_handler_id;
  unsigned long read_data_handler_id;

  // Lock callback functions
  comm_callback_t register_secure_cb;
  comm_callback_t read_secure_cb;
  comm_callback_t write_secure_error_cb;

  comm_callback_t register_data_cb;
  comm_callback_t read_data_cb;
  comm_callback_t write_data_error_cb;

  // DBus proxies
  GDBusProxy *read_secure_proxy;
  GDBusProxy *write_secure_proxy;

  GDBusProxy *read_data_proxy;
  GDBusProxy *write_data_proxy;

  // Misc
  void *user_data;        // Passed to callback functions
  const char *bt_address; // NOTE: Not freed by connect_data_free.
  const char *hci_device; // NOTE: Not freed by connect_data_free.
} comm_data_t;

// Init and free
comm_data_t *comm_data_new(
  uint8_t lock_gen,
  const char *bt_address,
  const char *hci_device,
  comm_callback_t register_secure_cb,
  comm_callback_t write_secure_cb,
  comm_callback_t register_data_cb,
  comm_callback_t write_data_cb,
  void *user_data);
void comm_data_free(comm_data_t *comm_data);
void comm_data_reset(comm_data_t *comm_data);

// Comm functions
void comm_register_secure(comm_data_t *comm_data, bool fire_and_forget);
void comm_write_secure(comm_data_t *comm_data, const char *data);

void comm_register_data(comm_data_t *comm_data, bool fire_and_forget);
void comm_write_data(comm_data_t *comm_data, const char *data);

#endif /* LOCK_COMM_H */
