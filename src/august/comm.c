#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "august/comm.h"

#include "utils/dbus.h"
#include "utils/environ.h"
#include "utils/logging.h"
#include "utils/string.h"
#include "utils/timeout.h"

static void register_secure_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  comm_data_t *comm_data = (comm_data_t *)user_data;
  GVariantDict *dict = g_variant_dict_new(changed_properties);
  GVariant *value = g_variant_dict_lookup_value(dict, "Notifying", NULL);

  if (value && g_variant_get_boolean(value)) {
    comm_data->register_secure_timeout_id =
      clear_timeout(comm_data->register_secure_timeout_id);
    comm_data->register_secure_cb(comm_data->user_data, (void *)true);
    g_variant_unref(value);
  }

  g_variant_dict_unref(dict);
}

static void read_secure_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  comm_data_t *comm_data = (comm_data_t *)user_data;
  GVariantDict *dict = g_variant_dict_new(changed_properties);
  GVariant *value = g_variant_dict_lookup_value(dict, "Value", NULL);

  if (value) {
    const char *data = string_bytes_to_hex(value, 18);
    comm_data->read_secure_cb(
      comm_data->user_data, (void *)data);
    if (value != NULL) g_variant_unref(value);
    free((void *)data);
  }

  g_variant_dict_unref(dict);
}

static void register_data_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  comm_data_t *comm_data = (comm_data_t *)user_data;
  GVariantDict *dict = g_variant_dict_new(changed_properties);
  GVariant *value = g_variant_dict_lookup_value(dict, "Notifying", NULL);

  if (value && g_variant_get_boolean(value)) {
    comm_data->register_data_timeout_id = 
      clear_timeout(comm_data->register_data_timeout_id);
    comm_data->register_data_cb(comm_data->user_data, (void *)true);
    g_variant_unref(value);
  }

  g_variant_dict_unref(dict);
}

static void read_data_handler(
    GDBusProxy *proxy,
    GVariant *changed_properties,
    GStrv invalidated_properties,
    gpointer user_data) {

  comm_data_t *comm_data = (comm_data_t *)user_data;
  GVariantDict *dict = g_variant_dict_new(changed_properties);
  GVariant *value = g_variant_dict_lookup_value(dict, "Value", NULL);

  if (value) {
    const char *data = string_bytes_to_hex(value, 18);
    comm_data->read_data_cb(comm_data->user_data,(void *)data);
    if (value != NULL) g_variant_unref(value);
    free((void *)data);
  }

  g_variant_dict_unref(dict);
}

static gboolean register_secure_timeout(gpointer user_data) {
  comm_data_t *comm_data = (comm_data_t *)user_data;
  comm_data->register_secure_cb(comm_data->user_data, NULL);

  return G_SOURCE_REMOVE;
}

static gboolean register_data_timeout(gpointer user_data) {
  comm_data_t *comm_data = (comm_data_t *)user_data;
  comm_data->register_data_cb(comm_data->user_data, NULL);

  return G_SOURCE_REMOVE;
}

comm_data_t *comm_data_new(
  uint8_t lock_gen,
  const char *bt_address,
  const char *hci_device,
  comm_callback_t register_secure_cb,
  comm_callback_t read_secure_cb,
  comm_callback_t register_data_cb,
  comm_callback_t read_data_cb,
  void *user_data) {

  comm_data_t *comm_data = malloc(sizeof(comm_data_t));
  if (!comm_data) {
    die(EAGAIN, "couldn't initialize comm data");
  }
  memset(comm_data, 0, sizeof(comm_data_t)); 

  comm_data->bt_address = bt_address;
  comm_data->hci_device = hci_device;

  comm_data->read_secure_proxy = dbus_read_secure_proxy(lock_gen, bt_address, hci_device);
  comm_data->write_secure_proxy = dbus_write_secure_proxy(lock_gen, bt_address, hci_device);

  comm_data->read_data_proxy = dbus_read_data_proxy(lock_gen, bt_address, hci_device);
  comm_data->write_data_proxy = dbus_write_data_proxy(lock_gen, bt_address, hci_device);

  comm_data->register_secure_cb = register_secure_cb;
  comm_data->read_secure_cb = read_secure_cb;

  comm_data->register_data_cb = register_data_cb;
  comm_data->read_data_cb = read_data_cb;

  comm_data->user_data = user_data;

  comm_data->register_secure_handler_id = g_signal_connect(
      comm_data->read_secure_proxy,
      "g-properties-changed",
      (GCallback)register_secure_handler,
      (gpointer)comm_data);
  if (comm_data->register_secure_handler_id <= 0)
    die(EAGAIN, "could not connect register secure signal handler");

  comm_data->read_secure_handler_id = g_signal_connect(
      comm_data->read_secure_proxy,
      "g-properties-changed",
      (GCallback)read_secure_handler,
      (gpointer)comm_data);
  if (comm_data->read_secure_handler_id <= 0)
    die(EAGAIN, "could not connect read secure signal handler");

  comm_data->register_data_handler_id = g_signal_connect(
      comm_data->read_data_proxy,
      "g-properties-changed",
      (GCallback)register_data_handler,
      (gpointer)comm_data);
  if (comm_data->register_data_handler_id <= 0)
    die(EAGAIN, "could not connect register data signal handler");

  comm_data->read_data_handler_id = g_signal_connect(
      comm_data->read_data_proxy,
      "g-properties-changed",
      (GCallback)read_data_handler,
      (gpointer)comm_data);
  if (comm_data->read_data_handler_id <= 0)
    die(EAGAIN, "could not connect read data signal handler");

  return comm_data;
}

void comm_data_free(comm_data_t *comm_data) {
  g_signal_handler_disconnect(
      comm_data->read_secure_proxy,
      comm_data->register_secure_handler_id);
  g_signal_handler_disconnect(
      comm_data->read_secure_proxy,
      comm_data->read_secure_handler_id);

  g_signal_handler_disconnect(
      comm_data->read_data_proxy,
      comm_data->register_data_handler_id);
  g_signal_handler_disconnect(
      comm_data->read_data_proxy,
      comm_data->read_data_handler_id);

  g_object_unref(comm_data->read_secure_proxy);
  g_object_unref(comm_data->write_secure_proxy);

  g_object_unref(comm_data->read_data_proxy);
  g_object_unref(comm_data->write_data_proxy);

  clear_timeout(comm_data->register_secure_timeout_id);
  clear_timeout(comm_data->register_data_timeout_id);

  free((void *)comm_data);
}

void comm_data_reset(comm_data_t *comm_data) {
  if (comm_data) {
    comm_data->register_secure_timeout_id = 
      clear_timeout(comm_data->register_secure_timeout_id);
    comm_data->register_data_timeout_id =
      clear_timeout(comm_data->register_data_timeout_id);
  } /* if */
}

void comm_register_secure(
  comm_data_t *comm_data,
  bool fire_and_forget) {
  bool seems_registered = false;

  GError *err = dbus_char_start_notify(comm_data->read_secure_proxy);
  if (err) {
    g_warning("register secure says: %s", err->message);
    g_error_free(err);
    seems_registered = true;
  }

  // Clear old timeout
  comm_data->register_secure_timeout_id =
    clear_timeout(comm_data->register_secure_timeout_id);

  if (fire_and_forget && seems_registered) {
    comm_data->register_secure_cb(comm_data->user_data, (void *)true);
  } else {
    // Build new timeout
    comm_data->register_secure_timeout_id = create_timeout(
        get_timeout_value(ENV_REGISTER_TIMEOUT),
        register_secure_timeout,
        comm_data);
  } /* else */
}

void comm_write_secure(comm_data_t *comm_data, const char *data) {
  GError *err = dbus_char_write_value(comm_data->write_secure_proxy, data);
  if (err) {
    g_warning("write secure says (%d): %s", err->code, err->message);
    if (41 == err->code)
      comm_data->read_secure_cb(comm_data->user_data, NULL);
    g_error_free(err);
  }
}

void comm_register_data(
  comm_data_t *comm_data,
  bool fire_and_forget) {
  bool seems_registered = false;

  GError *err = dbus_char_start_notify(comm_data->read_data_proxy);
  if (err) {
    g_warning("register data says (%d): %s", err->code, err->message);
    g_error_free(err);
    seems_registered = true;
  }

  // Clear old timeout
  comm_data->register_data_timeout_id =
    clear_timeout(comm_data->register_data_timeout_id);

  if (fire_and_forget && seems_registered) {
    comm_data->register_data_cb(comm_data->user_data, (void *)true);
  } else {
    // Build new timeout
    comm_data->register_data_timeout_id = create_timeout(
        get_timeout_value(ENV_REGISTER_TIMEOUT),
        register_data_timeout,
        comm_data);
  }
}

void comm_write_data(comm_data_t *comm_data, const char *data) {
  GError *err = dbus_char_write_value(comm_data->write_data_proxy, data);
  if (err) {
    g_warning("write data says: %s", err->message);
    if (41 == err->code)
      comm_data->read_data_cb(comm_data->user_data, NULL);
    g_error_free(err);
  }
}
