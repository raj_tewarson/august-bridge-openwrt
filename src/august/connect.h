#ifndef __LOCK_CONNECT_H__
#define __LOCK_CONNECT_H__

#include <glib.h>
#include <gio/gio.h>
#include <stdbool.h>

typedef void (*connect_callback_t)(void *user_data, void *param);

typedef struct {
  // Glib callback handlers
  unsigned long device_added_handler_id;      // Used for discovery
  unsigned long service_discovery_handler_id; // Used for connect
  unsigned long connect_handler_id;           // Used for connect
  unsigned long disconnect_handler_id;        // Used for disconnect
  unsigned long device_removed_handler_id;    // Used for disconnect

  unsigned long discover_timeout_id;
  unsigned long connect_timeout_id;
  unsigned long remove_timeout_id;
  unsigned long disconnect_timeout_id;

  // Lock callback functions
  connect_callback_t discovered_cb;   // Param NULL if timeout
  connect_callback_t connected_cb;    // Param NULL if timeout
  connect_callback_t disconnected_cb; // Param NULL if timeout
  connect_callback_t removed_cb;      // Param NULL if timeout

  // DBus proxies
  GDBusObjectManager *object_manager;
  GDBusProxy *adapter_proxy;
  GDBusProxy *device_proxy;
  GDBusProxy *service_proxy;

  // Misc
  void *user_data;        // Passed to callback functions
  const char *bt_address; // NOTE: Not freed by connect_data_free.
  const char *hci_device; // NOTE: Not freed by connect_data_free.
  const char *lock_id;    // NOTE: Not freed by connect_data_free.

  bool service_discovered; // both of these must be true to be connected
  bool connect_guard_time_passed;
} connect_data_t;

// Init and free
connect_data_t *connect_data_new(
    uint8_t     lock_gen,
    const char *bt_address,
    const char *hci_device,
    const char *lock_id,
    connect_callback_t discovered_cb,
    connect_callback_t connected_cb,
    connect_callback_t disconnected_cb,
    connect_callback_t removed_cb,
    void *user_data);
void connect_data_free(connect_data_t *connect_data);

// Connect functions
void discover_device(connect_data_t *connect_data);
void discover_stop(connect_data_t *connect_data);

void connect_to_device(connect_data_t *connect_data);
void disconnect_from_device(connect_data_t *connect_data);
void remove_device(connect_data_t *connect_data);

int get_rssi(connect_data_t *connect_data);

#endif /* LOCK_CONNECT_H */
