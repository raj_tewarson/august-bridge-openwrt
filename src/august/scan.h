#ifndef __LOCK_SCAN_H__
#define __LOCK_SCAN_H__

#include <glib.h>
#include <gio/gio.h>
#include <stdbool.h>

typedef void (*scan_callback_t)(void *user_data, void *param, uint8_t lock_gen);

/* scan_dirty_callback_t returns false when scan response can't be sent */
typedef bool (*scan_dirty_callback_t)(void *user_data, uint32_t param);

typedef enum {
  SCAN_ADV_TYPE_IDLE,
  SCAN_ADV_TYPE_DIRTY,
  SCAN_ADV_TYPE_BUSY,
  SCAN_ADV_TYPE_TIMEOUT,
} scan_adv_type_t;

typedef struct {
  // Glib callback handlers
  unsigned long scan_handler_id;
  unsigned long scan_timeout_id;
  unsigned long resend_dirty_timeout_id;
  unsigned long advertising_timeout_id;

  // Lock callback functions
  scan_callback_t scan_cb;      // Param NULL if timeout, mac string otherwise
  scan_dirty_callback_t dirty_bit_cb; // Param value of new dirty bit

  // DBus proxy
  GDBusProxy *scan_proxy;

  // Misc
  void *user_data;        // Passed to the callback functions
  const char *lock_id;    // NOTE: Not freed by scan_data_free.
  const char *bt_address; // NOTE: Not freed by scan_data_free.
  uint8_t target_lock_id[16];
  uint8_t dirty_bit;
  uint8_t busy;
  uint8_t timed_out;
} scan_data_t;

// Init and free
scan_data_t *scan_data_new(
    const char *bt_address,
    const char *lock_id,
    scan_callback_t scan_cb,
    scan_dirty_callback_t dirty_bit_cb,
    void *user_data);
void scan_data_free(scan_data_t *scan_data);

// Scan functions
void scan_start_lock_id(scan_data_t *scan_data);
void scan_start_dirty_bit(scan_data_t *scan_data);
void scan_stop(scan_data_t *scan_data);

#endif /* LOCK_SCAN_H */
