#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "august/scan.h"

#include "utils/dbus.h"
#include "utils/environ.h"
#include "utils/logging.h"
#include "utils/string.h"
#include "utils/timeout.h"

#define MAX_ADV_DATA_LENGTH 31

#define EV_TYPE_ADV_IND         0
#define EV_TYPE_ADV_DIRECT_IND  1
#define EV_TYPE_ADV_SCAN_IND    2
#define EV_TYPE_ADV_NONCONN_IND 3
#define EV_TYPE_SCAN_RSP        4

#define AD_TYPE_FLAGS                 0x01
#define AD_TYPE_PARTIAL_16BIT_UUIDS   0x02
#define AD_TYPE_COMPLETE_16BIT_UUIDS  0x03
#define AD_TYPE_PARTIAL_32BIT_UUIDS   0x04
#define AD_TYPE_COMPLETE_32BIT_UUIDS  0x05
#define AD_TYPE_PARTIAL_128BIT_UUIDS  0x06
#define AD_TYPE_COMPLETE_128BIT_UUIDS 0x07
#define AD_TYPE_SHORTENED_LOCAL_NAME  0x08
#define AD_TYPE_COMPLETE_LOCAL_NAME   0x09
#define AD_TYPE_TRANSMIT_POWER        0x0A
#define AD_TYPE_MFG_SPECIFIC          0xFF

#define CIC_AUGUST  0x01D1
#define CIC_KC_TECH 0x0016

static bool find_lock_id(
    scan_data_t *scan_data,
    char *mac,
    uint8_t *packet,
    uint8_t length) {

  if (length < 20) {
    return false;
  }

  if (*packet != 0xff) {
    return false;
  }

  if (!((*(packet + 1) == 0x16) && (*(packet + 2) == 0x00)) &&
     !((*(packet + 1) == 0xD1) && (*(packet + 2) == 0x01))) {
    return false;
  }

#if 0
  // MARS-1827: Jupiter is messing with the CIC so it's possible to miss
  // the correct lock here

  if (*(packet + 3) != 0x02) {
    return;
  }

  if (*(packet + 4) != 0x15) {
    return;
  }
#endif

  if (0 != memcmp(
      (packet + 5),
      scan_data->target_lock_id,
      sizeof(scan_data->target_lock_id))) {
    return false;
  }

  scan_data->bt_address = string_clone(mac);

  return true;
}

static bool find_lock_gen(
    scan_data_t *scan_data,
    char *mac,
    uint8_t *packet,
    uint8_t length,
    uint8_t *lock_gen) {

  fprintf(stderr, "%s()\n", __FUNCTION__);

  if (!is_jupiter(mac)) {
    *lock_gen = 1;
    return true;
  } /* if */

  if (*packet != AD_TYPE_SHORTENED_LOCAL_NAME &&
      *packet != AD_TYPE_COMPLETE_LOCAL_NAME) {
    return false;
  }

  char *local_name = (char *)(packet + 1);
  char *local_name_prefixes[] = {"Aug", "L3", "L4", "M1", NULL};
  int index;
  for (index = 0; local_name_prefixes[index]; index++) {
    char *prefix = local_name_prefixes[index];
    int prefix_len = strlen(prefix);
    int cmp_len = (prefix_len < (length - 1)) ? prefix_len : (length - 1);
    if (0 == memcmp(local_name, prefix, cmp_len)) {
      *lock_gen = index + 2;
      return true;
    } /* if */
  } /* for */

  return false;
} /* find_lock_gen */

static gboolean resend_dirty_timeout(gpointer user_data) {
  scan_data_t *scan_data = (scan_data_t *)user_data;

  if ((scan_data->dirty_bit) && (!scan_data->timed_out)) {
    g_warning("re-send lock dirty 0x%x", scan_data->dirty_bit);
    scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_DIRTY);

    scan_data->resend_dirty_timeout_id = create_timeout(
      get_timeout_value(ENV_RESEND_DIRTY_BIT_TIMEOUT),
      resend_dirty_timeout,
      scan_data);
  } /* if */

  return G_SOURCE_REMOVE;
} /* resend_dirty_timeout */

static gboolean advertising_timeout(gpointer user_data) {
  scan_data_t *scan_data = (scan_data_t *)user_data;

  g_warning("advertising timeout");

  scan_data->advertising_timeout_id =
    clear_timeout(scan_data->advertising_timeout_id);

  if (scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_TIMEOUT)) {
    scan_data->timed_out = 1;
  } else {
    /* couldn't send status--reset the advertising timeout timer */
    scan_data->advertising_timeout_id = create_timeout(
      get_timeout_value(ENV_ADVERTISING_TIMEOUT),
      advertising_timeout,
      scan_data);
  } /* if */

  return G_SOURCE_REMOVE;
} /* advertising_timeout */

static void find_dirty_bit(
    scan_data_t *scan_data,
    char *mac,
    uint8_t *packet,
    uint8_t length,
    uint32_t response_type) {

  uint8_t dirty_bit = 0;

  if (NULL == scan_data->bt_address) {
    return;
  }

  if (*(packet + 0) != 0xff) {
    return;
  }

  if (length != 4) {
    return;
  }

  if ((*(packet + 1) != 0xD1) || (*(packet + 2) != 0x01)) {
    return;
  }

  if (!string_compare_mac(scan_data->bt_address, mac)) {
    return;
  }

  dirty_bit = (*(packet + 3) & 0x01);

  // If we were not busy and now we are busy
  if (EV_TYPE_ADV_NONCONN_IND == response_type &&
      (!(scan_data->busy) || (scan_data->timed_out))) {
    scan_data->busy = true;
    scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_BUSY);
    goto done;
  }

  // If our dirty bit has changed and we're not busy
  if (scan_data->dirty_bit != dirty_bit &&
      (!(scan_data->busy) || (scan_data->timed_out))) {
    scan_data->resend_dirty_timeout_id = 
      clear_timeout(scan_data->resend_dirty_timeout_id);

    if (dirty_bit) {
      if (scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_DIRTY))
        scan_data->dirty_bit = dirty_bit;

      scan_data->resend_dirty_timeout_id = create_timeout(
        get_timeout_value(ENV_RESEND_DIRTY_BIT_TIMEOUT),
        resend_dirty_timeout,
        scan_data);
    } else {
      if (scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_IDLE))
        scan_data->dirty_bit = dirty_bit;
    }
    goto done;
  }

  // If we were busy and now we're not busy
  if (EV_TYPE_ADV_NONCONN_IND != response_type &&
      ((scan_data->busy) || (scan_data->timed_out))) {
    scan_data->busy = false;
    scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_IDLE);
    goto done;
  }

done:
  scan_data->timed_out = 0;
}

static void scan_callback(
    GDBusProxy *proxy,
    gchar *sender,
    gchar *signal,
    GVariant *params,
    gpointer user_data) {

  scan_data_t *scan_data = (scan_data_t *)user_data;
  uint32_t response_type;
  uint32_t address_type;
  char *mac = NULL;
  GVariantIter *data_iter = NULL;

  uint8_t adv_data[MAX_ADV_DATA_LENGTH];
  size_t adv_data_len;
  guint32 adv_data_index = 0;
  
  //fprintf(stderr, "%s()\n", __FUNCTION__);

  g_variant_get(params, "(iis(ay))",
    &response_type,
    &address_type,
    &mac,
    &data_iter);

  fprintf(stderr, "%s(%s)\n", __FUNCTION__, mac);

  adv_data_len = g_variant_iter_n_children(data_iter);
  if (adv_data_len > MAX_ADV_DATA_LENGTH) {
    g_warning("adv data too long");
    adv_data_len = MAX_ADV_DATA_LENGTH;
  } /* if */

  /* copy variant stuff out into an old fashioned array */
  while (g_variant_iter_loop(data_iter, "y", &adv_data[adv_data_index]) &&
        (++adv_data_index < adv_data_len)) {}

  g_variant_iter_free(data_iter);

  /* if the lock MAC address is still unknown, look for the lock ID in
   * each advertising packet
   */

  // TODO: wtf?
  if (NULL == scan_data->bt_address) {
    uint8_t *data_ptr = adv_data;
    uint8_t ad_length;
    uint8_t lock_gen = 0;
    bool found_lock_id = false;
    bool found_lock_gen = false;
    
    fprintf(stderr, "%s:%d\n", __FUNCTION__, __LINE__);

    while (data_ptr < adv_data + adv_data_len) {
      ad_length = *data_ptr++;
      if (found_lock_id)
        found_lock_gen = 
          find_lock_gen(scan_data, mac, data_ptr, ad_length, &lock_gen);
      else
        found_lock_id = find_lock_id(scan_data, mac, data_ptr, ad_length);
      data_ptr += ad_length;
    } /* while */

    if (found_lock_id) {
       fprintf(stderr, "%s:%d\n", __FUNCTION__, __LINE__);

      /* L1 and L2 locks defy the current implementation of find_lock_gen
       * by not having a lock name in the scan response packet.
       * Handle that case here by checking the lock MAC address
       */
      if (!found_lock_gen) {
        if (is_jupiter(scan_data->bt_address))
          lock_gen = 2;
        else
          lock_gen = 1;
      } /* if */

      scan_data->scan_timeout_id = clear_timeout(scan_data->scan_timeout_id);
      scan_data->scan_cb(scan_data->user_data, (void *)scan_data->bt_address, lock_gen);
    } /* if */
  } /* if */

  if ((NULL != scan_data->bt_address) &&
     (string_compare_mac(scan_data->bt_address, mac))) {

    uint8_t *data_ptr = adv_data;
    uint8_t ad_length;
       
    fprintf(stderr, "%s:%d\n", __FUNCTION__, __LINE__);

    while (data_ptr < adv_data + adv_data_len) {
      ad_length = *data_ptr++;
      find_dirty_bit(scan_data, mac, data_ptr, ad_length, response_type);
      data_ptr += ad_length;
    }

    /* reset the advertising timeout timer */
    scan_data->advertising_timeout_id = 
      clear_timeout(scan_data->advertising_timeout_id);

    scan_data->advertising_timeout_id = create_timeout(
      get_timeout_value(ENV_ADVERTISING_TIMEOUT),
      advertising_timeout,
      scan_data);

    /* HK advertisements won't have an August CIC or dirty bit..
     * reset timed_out status anyway in case we happen to just be catching
     * the HK adverts
     */
    if (scan_data->timed_out) {
      scan_data->dirty_bit_cb(scan_data->user_data, SCAN_ADV_TYPE_IDLE);
      scan_data->timed_out = 0;
    } /* if */
  } /* if */

  free((void *)mac);
}

static gboolean scan_timeout(gpointer user_data) {
  scan_data_t *scan_data = (scan_data_t *)user_data;
  g_warning("failed to find lock id: %s", scan_data->lock_id);
  scan_data->scan_cb(scan_data->user_data, NULL, 0);

  return G_SOURCE_REMOVE;
}

scan_data_t *scan_data_new(
  const char *bt_address,
  const char *lock_id,
  scan_callback_t scan_cb,
  scan_dirty_callback_t dirty_bit_cb,
  void *user_data) {

  scan_data_t *scan_data = malloc(sizeof(scan_data_t));
  if (NULL == scan_data) {
    die(EAGAIN, "couldn't initialize scan data");
  }
  memset(scan_data, 0, sizeof(scan_data_t));

  fprintf(stderr, "%s()\n", __FUNCTION__);

  scan_data->dirty_bit = 0;
  scan_data->busy = 0;
  scan_data->scan_proxy = dbus_ble_scan_proxy();
  scan_data->lock_id = lock_id;
  scan_data->bt_address = bt_address;
  scan_data->scan_cb = scan_cb;
  scan_data->dirty_bit_cb = dirty_bit_cb;
  scan_data->user_data = user_data;

  string_hex_to_byte_array(lock_id, scan_data->target_lock_id);

  scan_data->scan_handler_id = g_signal_connect(
      scan_data->scan_proxy,
      "g-signal",
      (GCallback)scan_callback,
      (gpointer)scan_data);

  if (scan_data->scan_handler_id <= 0)
    die(EAGAIN, "could not connect signal handler for scan callback");

  return scan_data;
}

void scan_data_free(scan_data_t *scan_data) {
  g_signal_handler_disconnect(
    scan_data->scan_proxy,
    scan_data->scan_handler_id);
  clear_timeout(scan_data->scan_timeout_id);
  clear_timeout(scan_data->advertising_timeout_id);
  g_object_unref(scan_data->scan_proxy);
  free((void *)scan_data);
}

void scan_start_lock_id(scan_data_t *scan_data) {
  GError *err = NULL;

  fprintf(stderr, "%s()\n", __FUNCTION__);

  err = dbus_ble_scan_stop(scan_data->scan_proxy);
  if (err) {
    g_warning("%s says: %s", __FUNCTION__, err->message);
    g_error_free(err);
    err = NULL;
  }

  dbus_ble_scan_set_properties(scan_data->scan_proxy, 0, 1, 48, 96, NULL);

  err = dbus_ble_scan_start(scan_data->scan_proxy);
  if (err) {
    g_warning("%s says: %s", __FUNCTION__, err->message);
    g_error_free(err);
  }

  scan_data->scan_timeout_id = clear_timeout(scan_data->scan_timeout_id);

  scan_data->scan_timeout_id = create_timeout(
    get_timeout_value(ENV_SCAN_TIMEOUT),
    scan_timeout,
    scan_data);
}

void scan_start_dirty_bit(scan_data_t *scan_data) {
  GError *err = NULL;
  
  fprintf(stderr, "%s()\n", __FUNCTION__);

  err = dbus_ble_scan_stop(scan_data->scan_proxy);
  if (err) {
    g_warning("%s says: %s", __FUNCTION__, err->message);
    g_error_free(err);
    err = NULL;
  }

  dbus_ble_scan_set_properties(scan_data->scan_proxy, 0, 0, 48, 384,
    (gchar*)scan_data->bt_address);

  err = dbus_ble_scan_start(scan_data->scan_proxy);
  if (err) {
    g_warning("%s says: %s", __FUNCTION__, err->message);
    g_error_free(err);
  }

  scan_data->advertising_timeout_id =
    clear_timeout(scan_data->advertising_timeout_id);

  scan_data->advertising_timeout_id = create_timeout(
    get_timeout_value(ENV_ADVERTISING_TIMEOUT),
    advertising_timeout,
    scan_data);
}

void scan_stop(scan_data_t *scan_data) 
{
  fprintf(stderr, "%s()\n", __FUNCTION__);

  GError *err = dbus_ble_scan_stop(scan_data->scan_proxy);
  if (err) {
    g_warning("scan stop says: %s", err->message);
    g_error_free(err);
  }
}
