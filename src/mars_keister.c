/*
 * Copyright 2015, August Home Inc
 *
 * Mars immutable key store implementation
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/aes.h>

#include <mars_keister_master_key.h>
#include "mars_keister.h"

#define KEISTER_DEFAULT_BLOB_PATH "/usr/share/august_bridge/ks"

#define KEISTER_MAGIC_NUMBER 0x734b

static struct MARS_KEISTER_S {
  uint16_t magic;
  uint16_t blob_size;
  uint8_t iv[16];
  uint8_t *encrypted_keys;
} mars_keister = { 0 };

/*
 * mars_keister_get_key
 *
 * retrieve the given keyNum from the keister.
 * the maximum size key to return is passed in *keySize
 * the size of the key is returned in *keySize
 * if 'key' is non-NULL, key data is copied to the address pointed to
 * by 'key'.
 *
 * returns <0 on error
 */
int 
mars_keister_get_key(
  mars_key_index_t keyNum,
  uint16_t *keySize,
  uint8_t *key) {

  int ret = -1;
  uint8_t *clear_keys;
  uint16_t thisKeyNum = 0;
  uint8_t *thisKey;
  uint16_t maxCopyBytes;
  AES_KEY aesKey;
  uint8_t iv[16];

  if ((mars_keister.magic != KEISTER_MAGIC_NUMBER) ||
      (mars_keister.blob_size == 0) ||
      (mars_keister.encrypted_keys == NULL)) {
    fprintf(stderr, "%s something's missing\n", __FUNCTION__);
    goto bail;
  } /* if */

  memcpy(iv, mars_keister.iv, sizeof(iv));

  maxCopyBytes = *keySize;

  clear_keys = malloc(mars_keister.blob_size + AES_BLOCK_SIZE); 

  if (!clear_keys) {
    goto bail;
  } /* if */

  //fprintf(stderr, "%s(%d)\n", __FUNCTION__, mars_keister.blob_size);

  AES_set_decrypt_key(mars_keister_master_key, AES_BLOCK_SIZE * 8, &aesKey);
  
  AES_cbc_encrypt(mars_keister.encrypted_keys, clear_keys,
    mars_keister.blob_size, &aesKey, iv, AES_DECRYPT);
  
  thisKey = clear_keys;
  *keySize = *((uint16_t*)thisKey);

  while ((thisKeyNum < keyNum) &&
        (thisKey < clear_keys + mars_keister.blob_size)) {
    thisKey += *keySize + sizeof(uint16_t);
    thisKeyNum++;
    *keySize = *((uint16_t*)thisKey);
  } /* while */

  if ((thisKeyNum == keyNum) &&
     (thisKey + *keySize < clear_keys + mars_keister.blob_size)) {
    if (key)
      memcpy(key, thisKey + sizeof(uint16_t),
        (maxCopyBytes < *keySize ? maxCopyBytes : *keySize));
    ret = 0;
  } 

  memset(clear_keys, 0, mars_keister.blob_size);
  free(clear_keys);

bail:
  return ret;
} /* mars_keister_get_key */

/*
 * mars_keister_init
 *
 * initialize the keystore interface.
 *
 * returns <0 on error
 */
int
mars_keister_init(void) {

  int ret = -1;
  int n   = 0;
  char *keister_blob_path = NULL;
  FILE *pBlobFile = NULL;

  if (NULL == (keister_blob_path = getenv("KEISTER_BLOB_PATH"))) {
    keister_blob_path = KEISTER_DEFAULT_BLOB_PATH;
  }

  /* read flash data size & IV */
  pBlobFile = fopen(keister_blob_path, "r");

  if (!pBlobFile) {
    fprintf(stderr, "%s fopen(%s)\n", __FUNCTION__, keister_blob_path);
    goto bail;
  } /* if */

  n = sizeof(mars_keister.magic) + sizeof(mars_keister.blob_size) + sizeof(mars_keister.iv);
  ret = fread((void*)&mars_keister, 1, n, pBlobFile);

  if (mars_keister.magic != KEISTER_MAGIC_NUMBER) {
    fprintf(stderr, "%s bad magic %X\n", __FUNCTION__, mars_keister.magic);
    goto bail;
  } /* if */

  /* allocate encrypted_keys buffer */
  mars_keister.encrypted_keys = malloc(mars_keister.blob_size);
  fprintf(stderr, "%s(%d) -> %X\n", __FUNCTION__, mars_keister.blob_size, mars_keister.encrypted_keys);

  if (!mars_keister.encrypted_keys) {
    mars_keister.blob_size = 0;
    goto bail;
  } /* if */

  /* read encrypted keys */
  ret = fread((void*)mars_keister.encrypted_keys, 1, mars_keister.blob_size,  pBlobFile);

  if (ret < 0) {
    fprintf(stderr, "%s fread %d\n", __FUNCTION__, ret);

    free(mars_keister.encrypted_keys);
    mars_keister.encrypted_keys = NULL;
    mars_keister.blob_size = 0;
    goto bail;
  } /* if */

bail:
  if (pBlobFile)
    fclose(pBlobFile);

  return ret;
} /* mars_keister_init */

/*
 * mars_keister_done
 *
 * free the keystore interface.
 *
 * returns <0 on error
 */
int
mars_keister_done(void) {
  int ret = -1;

  if (mars_keister.encrypted_keys) {
    free(mars_keister.encrypted_keys);
  } /* if */

  return ret;
} /* mars_keister_done */
