#ifndef __THREADS_LOCKOP__
#define __THREADS_LOCKOP__

#include <pthread.h>

/**
 * \brief The thread struct for the lockop thread.
 */
extern pthread_t lockop_thread;

/**
 * \brief Creates the lockop thread.
 *
 * \param[in] ptr     Pointer to thread config data.
 * \param[in] queue   Outgoing queue.
 *
 * \returns Whatever `pthread_create` returns.
 */
int create_lockop_thread(thread_config_t *config);


#endif /* THREADS_LOCKOP */
