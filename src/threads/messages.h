#ifndef __THREADS_MESSAGES__
#define __THREADS_MESSAGES__

#include <pthread.h>

/**
 * \brief The thread struct for the messages thread.
 */
extern pthread_t messages_thread;

/**
 * \brief Creates the messages thread.
 *
 * \param[in] ptr     Pointer to thread config data.
 * \param[in] queue   Outgoing queue.
 *
 * \returns Whatever `pthread_create` returns.
 */
int create_messages_thread(thread_config_t *config, GAsyncQueue *queue);

#endif /* THREADS_MESSAGES */
