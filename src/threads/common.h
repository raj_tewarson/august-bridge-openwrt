#ifndef __THREADS_COMMON__
#define __THREADS_COMMON__

#include <glib.h>
#include <stdbool.h>

/**
 * \brief Configuration data common to all threads.
 */
typedef struct {
  const char *serial_number;
  const char *bridge_server;
  const char *auth_token;
  const char *bridge_id;
  const char *hci_device;
  uint8_t     lock_gen;
  const char *lock_mac;
  const char *lock_id;
  bool use_bluez_cache;
  volatile bool running;
  bool dirty_video;
} thread_config_t;

#endif /* THREADS_COMMON */
