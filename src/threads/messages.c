#include <json.h>
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include "threads/common.h"
#include "threads/messages.h"

#include "utils/curl.h"
#include "utils/json.h"

// Private data structures
thread_config_t *thread_config;
GAsyncQueue *in_queue;

// Private variables
static pthread_attr_t messages_thread_attr;

// Private functions
static void *messages_main(void *ptr);

// Public variables
pthread_t messages_thread;

int create_messages_thread(thread_config_t *config, GAsyncQueue *queue) {
  thread_config = config;
  in_queue = queue;

  pthread_attr_init(&messages_thread_attr);
  return pthread_create(
      &messages_thread,
      &messages_thread_attr,
      messages_main,
      NULL);
}

void *messages_main(void *ptr) {
  long http_response_code = 0;
  json_object *response = NULL;
  const char *messages_url = NULL;

  do {
    response = (json_object *)g_async_queue_timeout_pop(in_queue, 1000000);

    if (NULL == response) {
      continue;
    }

    messages_url = get_messages_url(
        thread_config->bridge_server,
        json_peek_string(response, "lockID"));

    g_info("POST %s", json_object_get_string(response));
    http_response_code = post(
        messages_url,
        json_object_get_string(response),
        thread_config->auth_token);
    if (http_response_code != 200)
      g_critical("error when posting messages: %ld", http_response_code);

    if (response != NULL) {
      json_object_put(response);
      response = NULL;
    }

    if (messages_url != NULL) {
      free((void *)messages_url);
      messages_url = NULL;
    }
  } while (thread_config->running);

  g_debug("messages thread exiting");

  return NULL;
}
