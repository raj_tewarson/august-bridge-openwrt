#ifndef __THREADS_BLE__
#define __THREADS_BLE__

#include <pthread.h>

#include "threads/common.h"

/**
 * \brief The thread struct for the ble thread.
 */
extern pthread_t ble_thread;

/**
 * \brief Creates the ble thread.
 *
 * \param[in] config    Pointer to thread config data.
 * \param[in] config    Input queue.
 * \param[in] config    Output queue.
 *
 * \returns Whatever `pthread_create` returns.
 */
int create_ble_thread(
    thread_config_t *config,
    GAsyncQueue *outqueue);

unsigned int ble_thread_oneshot(void *data);

#endif /* THREADS_BLE */

