#include <gio/gio.h>
#include <glib.h>
#include <json.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "august/lock.h"

#include "threads/ble.h"
#include "threads/common.h"

#include "utils/environ.h"
#include "utils/file.h"
#include "utils/json.h"
#include "utils/logging.h"
#include "utils/string.h"
#include "utils/dbus.h"

// Private variables
static pthread_attr_t ble_thread_attr;
static thread_config_t *thread_config;
static GAsyncQueue *out_queue;
static GMainContext *ble_context;
static GMainLoop *ble_loop;

// Private functions

static bool oneshot_callback(void *data) {
  json_object *command = (json_object *)data;
  g_debug("oneshot");
  lock_next(command);
  return G_SOURCE_REMOVE;
}

static bool running_callback(void *data) {
  if (false == thread_config->running) {
    g_debug("quitting ble_loop");
    g_main_loop_quit(ble_loop);
  }
  return G_SOURCE_CONTINUE;
}

static void register_callback(json_object *response) {
  char* mac_string = NULL;
  char* gen_string = NULL;

  g_debug("register callback");
  file_write(json_peek_string(response, "lockID"), get_lock_id_path());
 
  mac_string = strdup(json_peek_string(response, "macAddr")); 

  if (mac_string) {
    gen_string = strchr(mac_string, ',');

    if (gen_string) {
      *gen_string = '\0';
      gen_string++;
      file_write(gen_string, get_lock_gen_path());
    } /* if */
    file_write(mac_string, get_lock_mac_path());
    free(mac_string);
  } /* if */
}

static void connect_callback(json_object *response) {
  g_debug("connect callback");
  g_async_queue_push(out_queue, response);
}

static void disconnect_callback(json_object *response) {
  g_debug("disconnect callback");
  g_async_queue_push(out_queue, response);
}

static void read_secure_callback(json_object *response) {
  g_debug("write secure callback");
  g_async_queue_push(out_queue, response);
}

static void read_data_callback(json_object *response) {
  g_debug("write data callback");
  g_async_queue_push(out_queue, response);
}

static void dirty_bit_callback(json_object *response) {
  g_debug("dirty callback");
  g_async_queue_push(out_queue, response);
}

static void reboot_callback(json_object *response) {
  g_debug("reboot callback");
  die(EAGAIN, "got reboot command");
}

static void error_callback(json_object *response) {
  g_debug("error callback");
  /* Nothing to do here */
  g_async_queue_push(out_queue, response);
}

static void *ble_main(void *ptr) {
  GSource *running = NULL;
  lock_funcs_t callbacks = {0};

  // Must set default thread context prior to building the lock object.
  ble_context = g_main_context_new();
  g_main_context_push_thread_default(ble_context);

  callbacks.register_cb = register_callback;
  callbacks.connect_cb = connect_callback;
  callbacks.disconnect_cb = disconnect_callback;
  callbacks.read_secure_cb = read_secure_callback;
  callbacks.read_data_cb = read_data_callback;
  callbacks.dirty_bit_cb = dirty_bit_callback;
  callbacks.reboot_cb = reboot_callback;
  callbacks.error_cb = error_callback;

  lock_init(
    &callbacks,
    thread_config->lock_gen,
    thread_config->lock_mac,
    thread_config->hci_device,
    thread_config->lock_id,
    thread_config->use_bluez_cache,
    thread_config->dirty_video);

  ble_loop = g_main_loop_new(ble_context, false);

  running = g_timeout_source_new(500);
  g_source_set_callback(running, (GSourceFunc)running_callback, NULL, NULL);
  g_source_set_priority (running, G_PRIORITY_DEFAULT_IDLE);
  g_source_attach(running, ble_context);
  g_source_unref(running);

  g_main_loop_run(ble_loop);

  g_debug("ble_loop exited");

  if (ble_context != NULL) g_main_context_unref(ble_context);
  if (ble_loop != NULL) g_main_loop_unref(ble_loop);

  lock_free();

  return NULL;
}

// Public variables
pthread_t ble_thread;

int create_ble_thread(
    thread_config_t *config,
    GAsyncQueue *outqueue) {

  thread_config = config;
  out_queue = outqueue;

  pthread_attr_init(&ble_thread_attr);
  return pthread_create(
    &ble_thread,
    &ble_thread_attr,
    ble_main,
    NULL);
}

unsigned int ble_thread_oneshot(void *data) {
  unsigned int id = 0;
  GSource *idle = NULL;

  g_debug("%s", __FUNCTION__);

  idle = g_idle_source_new();
  g_source_set_callback(idle, (GSourceFunc)oneshot_callback, data, NULL);
  g_source_set_priority (idle, G_PRIORITY_HIGH_IDLE);
  id = g_source_attach(idle, ble_context);
  g_source_unref(idle);

  return id;
}
