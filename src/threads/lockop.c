#include <errno.h>
#include <json.h>
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include "threads/common.h"
#include "threads/lockop.h"
#include "threads/ble.h"

#include "utils/curl.h"
#include "utils/logging.h"

// Private variables
static pthread_attr_t lockop_thread_attr;
static thread_config_t *thread_config;
static const char *lockop_url;

// Private functions
static void *lockop_main(void *ptr);

// Public variables
pthread_t lockop_thread;

int create_lockop_thread(thread_config_t *config) {
  thread_config = config;
  lockop_url = get_lockop_url(config->bridge_server, config->bridge_id);

  pthread_attr_init(&lockop_thread_attr);
  return pthread_create(
      &lockop_thread,
      &lockop_thread_attr,
      lockop_main,
      NULL);
}

void *lockop_main(void *ptr) {
  long http_response_code = 0;
  const char *response = NULL;
  const char *headers = NULL;

  do {
    http_response_code = get(lockop_url, &response, &headers,
        thread_config->auth_token, &thread_config->running);

    /*
     * MARS-1484 - Categorize BLE session speed
     *
     * In an effort to better categorize the speed of RBS I've asked the
     * server team to add their transaction id to the headers of GET lockop.
     * This transaction ID is valid until the next GET lockop occurs.
     *
     * I can do this because RBS is stateful and synchronous. RBS will not
     * tick its state machine until it recieves _all_ responses from the
     * bridge application. There may be situations in which the transaction id
     * is incorrect, but this should cover 90% of cases without having to
     * rewrite the dbus interface to better support tracing.
     *
     * If the server does not respond with a transaction id, we generate one
     * by incrementing our transaction id counter.
     *
     */
    {
      if (transaction_id != NULL) {
        free((void *)transaction_id);
        transaction_id = NULL;
      }
      if (headers != NULL) {
        transaction_id = get_transaction_id(headers);
      }
    }

    switch (http_response_code) {
      case 200:
        if (response) {
          g_info("GET %s", response);
          ble_thread_oneshot((void *)json_tokener_parse(response));
        } else {
          g_warning("Empty response from RBS!");
        }
        break;
      case 401:
        die(EAGAIN, "Failed server auth");
        break;
      case 404:
      case 409:
        die(EAGAIN, "Bad bridge id");
        break;
      case 502:
        g_info("GET timeout");
        break;
      default:
        sleep(1);
        /* This is necessary because if RBS goes down we don't want to go
           into a crazy spin loop */
        break;
    }

    if (response != NULL) {
      free((void *)response);
      response = NULL;
    }
    if (headers != NULL) {
      free((void *)headers);
      headers = NULL;
    }
  } while(thread_config->running);

  if (lockop_url != NULL) free((void *)lockop_url);
  return NULL;
}
