/*
 * Copyright 2019, August Home Inc
 *
 */
#ifndef BLESCAN_H
#define BLESCAN_H

#ifdef __cplusplus
extern "C" {
#endif

int blescan_init();
void blescan_exit();

#ifdef __cplusplus
}
#endif


#endif 
