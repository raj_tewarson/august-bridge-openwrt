/*
 *
 *  blescan - dbus interface to ble scanning with bluez 
 *
 *  Copyright (C) 2015 August Home, Inc
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <pthread.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include "blescan-generated.h"

#define BLESCAN_BUS_NAME_DOTTED "com.august.BLEScan"
#define BLESCAN_BUS_PATH "/com/august/BLEScan"

#define BLESCAN_FILTER_DUP 0
#define BLESCAN_HCI_TIMEOUT 1000

#define BLESCAN_DEFAULT_SCAN_TYPE 0
#define BLESCAN_DEFAULT_SCAN_INTERVAL 250
#define BLESCAN_DEFAULT_SCAN_WINDOW 16

/*
 * blescan privates
 */
static struct blescan_s {
  BLEScan *obj;
  GIOChannel *gio_hci;

  int hci_device_handle;
  int hci_device_number;

  bool filter_scan_results;
  bdaddr_t filter_mac;

  bool scanning;

  pthread_t thread;
  GMainLoop *loop;
} blescan;

/*
 * blescan_set_defaults
 *
 * set default properties on the skeleton object
 */
static void
blescan_set_defaults(
  BLEScan *object) {

  blescan_set_device(object, hci_get_route(NULL));
  blescan_set_type_(object, BLESCAN_DEFAULT_SCAN_TYPE);
  blescan_set_interval(object, BLESCAN_DEFAULT_SCAN_INTERVAL);
  blescan_set_window(object, BLESCAN_DEFAULT_SCAN_WINDOW);
  blescan_set_addr_filter(object, "");
} /* blescan_set_defaults */

/*
 * blescan_hci_in
 *
 * called by glib when data is available on the HCI file descriptor
 */
static gboolean
blescan_hci_in(
  GIOChannel *gio_channel,
  GIOCondition gio_condition,
  gpointer data) {

  GError *gio_error = NULL;
  GIOStatus gio_stat;

  if (gio_condition & G_IO_HUP) {
    g_debug("HCI HUP");
    return FALSE;
  } /* if */

  if (gio_condition & G_IO_IN) {
    gchar read_buf[HCI_MAX_EVENT_SIZE];
    gsize bytes_read;

    gio_stat = g_io_channel_read_chars(gio_channel, read_buf,
      sizeof(read_buf), &bytes_read, &gio_error);

    if (G_IO_STATUS_ERROR == gio_stat) {
      g_error("GIO Error on HCI device %s\n", gio_error->message);
    } else {
      le_advertising_info *info;
      evt_le_meta_event *meta = (void *)(read_buf + (1 + HCI_EVENT_HDR_SIZE));
      char bd_addr_string[18];

      if (meta->subevent != EVT_LE_ADVERTISING_REPORT) {
        goto done;
      } /* if */

      info = (le_advertising_info *) (meta->data + 1);

      if ((blescan.filter_scan_results) &&
         (0 != bacmp(&blescan.filter_mac, &info->bdaddr)))
        goto done;

      ba2str(&info->bdaddr, bd_addr_string);

      blescan_emit_scan_response(blescan.obj,
        info->evt_type,
        info->bdaddr_type,
        bd_addr_string,
        g_variant_new_from_data(G_VARIANT_TYPE("(ay)"),
         info->data, info->length, TRUE, NULL, NULL));
    } /* if */
  } /* if */

done:
  return TRUE;
} /* blescan_hci_in */

/*
 * blescan_close_hci_device
 *
 * close the HCI file descriptor and shut down the associated IO channel
 */
static bool
blescan_close_hci_device(void) {

  if (blescan.gio_hci) {
    g_io_channel_shutdown(blescan.gio_hci, false, NULL);
    g_io_channel_unref(blescan.gio_hci);
  } /* if */

  if (blescan.hci_device_handle >= 0) {
    hci_close_dev(blescan.hci_device_handle);
  } /* if */

  blescan.hci_device_handle = -1;
  blescan.gio_hci = NULL;

  return true;
} /* blescan_close_hci_device */

/*
 * blescan_open_hci_device
 *
 * open the HCI device specified by the object properties
 * and set up an IO channel to read scan data
 */
static bool
blescan_open_hci_device(void) {

  /* don't re-open an HCI device, but do close and open a new device
   * if the target HCI device number has changed
   */
  if (blescan.hci_device_handle >= 0) {
    if (blescan.hci_device_number == blescan_get_device(blescan.obj))
      return true;
    else
      blescan_close_hci_device();
  } /* if */

  blescan.hci_device_number = blescan_get_device(blescan.obj);

  if ((blescan.hci_device_handle = 
      hci_open_dev(blescan.hci_device_number)) < 0) {
    return false;
  } /* if */

  /* set a new HCI filter */
  struct hci_filter new_filter;

  hci_filter_clear(&new_filter);
  hci_filter_set_ptype(HCI_EVENT_PKT, &new_filter);
  hci_filter_set_event(EVT_LE_META_EVENT, &new_filter);

  if (setsockopt(blescan.hci_device_handle, SOL_HCI, HCI_FILTER,
      &new_filter, sizeof(new_filter)) < 0) {

    return false;
  } /* if */

  blescan.gio_hci = g_io_channel_unix_new(blescan.hci_device_handle);
  g_io_channel_set_encoding(blescan.gio_hci, NULL, NULL);
  g_io_channel_set_buffered(blescan.gio_hci, FALSE);
  g_io_add_watch(blescan.gio_hci, G_IO_IN | G_IO_HUP, blescan_hci_in, NULL);

  return true;
} /* blescan_open_hci_device */

/*
 * on_blescan_start
 *
 * called by the dbus skeleton bits to start scanning
 */
static gboolean
on_blescan_start(
  BLEScan *BLEScanObj,
  GDBusMethodInvocation  *invocation,
  gpointer                user_data) {

  if (true == blescan.scanning) {
    g_dbus_method_invocation_return_dbus_error(
      invocation,
      BLESCAN_BUS_NAME_DOTTED ".Error.Failed",
      "Already scanning");

    goto done;
  } /* if */

  if (FALSE == blescan_open_hci_device()) {
    g_dbus_method_invocation_return_dbus_error(
      invocation,
      BLESCAN_BUS_NAME_DOTTED ".Error.Failed",
      "Unable to open HCI device");

    goto done;
  } /* if */

  if (hci_le_set_scan_parameters(blescan.hci_device_handle,
      blescan_get_type_(BLEScanObj),
      htobs(blescan_get_interval(BLEScanObj)),
      htobs(blescan_get_window(BLEScanObj)),
      0x00, 0x00, BLESCAN_HCI_TIMEOUT) < 0) {

    g_dbus_method_invocation_return_dbus_error(
      invocation,
      BLESCAN_BUS_NAME_DOTTED ".Error.Failed",
      "Scan parameters failed");

    goto done;
  } /* if */

  /* filter on a MAC address */
  const gchar* addr_filter = blescan_get_addr_filter(BLEScanObj);

  if (0 == str2ba(addr_filter, &blescan.filter_mac)) {
    blescan.filter_scan_results = true;
  } else {
    blescan.filter_scan_results = false;
  }

  if (hci_le_set_scan_enable(blescan.hci_device_handle,
      0x01, BLESCAN_FILTER_DUP, BLESCAN_HCI_TIMEOUT) < 0) {

    g_dbus_method_invocation_return_dbus_error(
      invocation,
      BLESCAN_BUS_NAME_DOTTED ".Error.Failed",
      "Scan enable failed");

    goto done;
  } /* if */

  blescan.scanning = true;

  blescan_complete_start(BLEScanObj, invocation, TRUE);

done:
  return TRUE;
} /* on_blescan_start */

/*
 * on_blescan_stop
 *
 * called by the dbus skeleton stuff to stop scanning
 */
static gboolean
on_blescan_stop(
  BLEScan *BLEScanObj,
  GDBusMethodInvocation  *invocation,
  gpointer                user_data) {

  if (false == blescan.scanning) {
    g_dbus_method_invocation_return_dbus_error(
      invocation,
      BLESCAN_BUS_NAME_DOTTED ".Error.Failed",
      "Not scanning");

    goto done;
  } /* if */

  if (hci_le_set_scan_enable(blescan.hci_device_handle,
      0x00, BLESCAN_FILTER_DUP, BLESCAN_HCI_TIMEOUT) < 0) {

    g_dbus_method_invocation_return_dbus_error(
      invocation,
      BLESCAN_BUS_NAME_DOTTED ".Error.Failed",
      "Stop scan failed");

    goto done;
  } /* if */

  blescan.scanning = false;

  blescan_complete_stop(BLEScanObj, invocation, TRUE);

done:
  return TRUE;
} /* on_blescan_stop */

/*
 * on_bus_acquired
 *
 * instantiate the dbus connection things and connect them
 * to the scanning things
 */
static void
on_bus_acquired(
  GDBusConnection *connection,
  const gchar     *name,
  gpointer         user_data) {

  g_debug("Bus acquired %s\n", name);

  blescan.obj = blescan_skeleton_new();
  blescan_set_defaults(blescan.obj);
  blescan.scanning = false;
  blescan.hci_device_handle = -1;
  blescan.gio_hci = NULL;

  g_signal_connect(blescan.obj,
                    "handle-start",
                    G_CALLBACK (on_blescan_start),
                    NULL); /* user_data */

  g_signal_connect(blescan.obj,
                    "handle-stop",
                    G_CALLBACK (on_blescan_stop),
                    NULL); /* user_data */

  g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(blescan.obj),
    connection, BLESCAN_BUS_PATH, NULL);
} /* on_bus_acquired */

/*
 * on_name_acquired
 *
 * pointlessly called when dbus name has been acquired
 */
static void
on_name_acquired(
  GDBusConnection *connection,
  const gchar     *name,
  gpointer         user_data) {

  g_debug("Acquired the name %s\n", name);
} /* on_name_acquired */

/*
 * on_name_lost
 *
 * called when dbus setup fails or the dbus name is lost
 * (probably to another instance of blescan).
 */
static void
on_name_lost(
  GDBusConnection *connection,
  const gchar     *name,
  gpointer         user_data) {

  g_debug("Lost the name %s\n", name);

  // exit(1);
} /* on_name_lost */


static void *blescan_thread(void *arg)
{
  guint id;

  blescan.loop = g_main_loop_new(NULL, FALSE);

  id = g_bus_own_name(G_BUS_TYPE_SYSTEM,
                      BLESCAN_BUS_NAME_DOTTED,
                      G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                      G_BUS_NAME_OWNER_FLAGS_REPLACE,
                      on_bus_acquired,
                      on_name_acquired,
                      on_name_lost,
                      NULL,
                      NULL);

  g_main_loop_run(blescan.loop);

  g_debug("%s main loop done %s\n", __FUNCTION__);

  g_bus_unown_name(id);
  g_main_loop_unref(blescan.loop);

  return 0;
}


int blescan_init()
{
  int rc;

  g_debug("%s", __FUNCTION__);

  memset(&blescan, 0, sizeof(blescan));

  system("hciconfig hci0 up");

  rc = pthread_create(&blescan.thread, NULL, blescan_thread, NULL);
  if(rc != 0) {
    g_error("%s Unable to create blescan_thread", __FUNCTION__);
    return rc;
  }
  
  return 0;
  // TODO should this block?
} 


void blescan_exit()
{
  g_main_loop_quit(blescan.loop);

  if(blescan.thread) {
    pthread_cancel(blescan.thread);
  }
  
  memset(&blescan, 0, sizeof(blescan));
} 
