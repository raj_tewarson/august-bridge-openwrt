#!/usr/bin/env bash
PWD=$(dirname ${0})

SECURE_KEYS_PATH=${SECURE_KEYS_PATH:-${PWD}/../bogus_keys}
SERIAL_NUMBER=${1:-"D1GJS0001H"}
BRIDGE_SERVER=${2:-"https://rbs.august.com"}
shift 2

if [ -n "${GDB}" ]; then
  GDB="$(which gdb) --args"
fi

if [ -n "${VALGRIND}" ]; then
  VALGRIND="$(which valgrind) --tool=memcheck --leak-check=full --num-callers=40"
  VALGRIND="${VALGRIND} --suppressions=/home/bbarr/src/august/GNOME.supp/build/base.supp"
  VALGRIND="${VALGRIND} --suppressions=/home/bbarr/src/august/GNOME.supp/build/gail.supp"
  VALGRIND="${VALGRIND} --suppressions=/home/bbarr/src/august/GNOME.supp/build/gdk.supp"
  VALGRIND="${VALGRIND} --suppressions=/home/bbarr/src/august/GNOME.supp/build/gio.supp"
  VALGRIND="${VALGRIND} --suppressions=/home/bbarr/src/august/GNOME.supp/build/glib.supp"
fi

export MARS_CERT="${SECURE_KEYS_PATH}/mars_tls_client.crt"
export MARS_KEY="${SECURE_KEYS_PATH}/mars_tls_private_key.pem"
export MARS_LOCK_MAC="/tmp/LOCK_MAC"
export MARS_LOCK_ID="/tmp/LOCK_ID"
export MARS_CA="${PWD}/../august-mars-rootfs/rootfs/usr/local/share/ca.cert"
export KEISTER_BLOB_PATH="${PWD}/../host/usr/local/share/ks"
export G_MESSAGES_DEBUG="all"
#export G_DBUS_DEBUG="all"
export G_DEBUG="gc-friendly,fatal-criticals"
#export G_DEBUG="gc-friendly"
export G_SLICE="always-malloc"

${GDB} ${VALGRIND} ${PWD}/x86_64/bin/bridge -s ${SERIAL_NUMBER} -b ${BRIDGE_SERVER} -v 6 ${@}
