#ifndef __UTILS_CURL__
#define __UTILS_CURL__

#include <curl/curl.h>
#include <stdbool.h>
#include <stdlib.h>

/**
 * \brief Initializes the curl library.
 *
 * \note Must be called from the main thread.
 */
void init_curl(void);

/**
 * \brief Cleans up the curl library.
 *
 * \note Must be called from main thread.
 */
void cleanup_curl(void);

/**
 * \brief Formats the bridge setup url.
 *
 * Builds the bridge setup url using the given server and serial number.
 *
 * \note
 * This function dynamically allocates space for the url. It must be freed
 * after use.
 *
 * \param[in] server  Bridge server to use in the url.
 * \param[in] serial  Serial number to be used in the url.
 *
 * \returns The url for the iconfig server.
 */
const char *get_bridge_setup_url(const char *server, const char *serial);

/**
 * \brief Formats the lockop url.
 *
 * Builds the lockop url using the given server and bridge id.
 *
 * \note
 * This function dynamically allocates space for the url. It must be freed
 * after use.
 *
 * \param[in] server      Bridge server to use in the url.
 * \param[in] bridge_id   Bridge id to be used in the url.
 *
 * \returns The url for the lockop request.
 */
const char *get_lockop_url(const char *server, const char *bridge_id);

/**
 * \brief Formats the messages url.
 *
 * Builds the messages url using the given server and bridge id.
 *
 * \note
 * This function dynamically allocates space for the url. It must be freed
 * after use.
 *
 * \param[in] server    Bridge server to use in the url.
 * \param[in] lock_id   Bridge id to be used in the url.
 *
 * \returns The url for the messages request.
 */
const char *get_messages_url(const char *server, const char *lock_id);

/**
 * \brief Returns the transaction ID in a set of headers
 *
 * \note
 * This function dynamically acllocates space for the transaction id. It must
 * be freed after use.
 *
 * \param[in] headers   The headers to find the transaction id in.
 *
 * \returns The transaction id or NULL.
 */
const char *get_transaction_id(const char *headers);

/**
 * \brief Performs a get on the given url.
 *
 * Wraps all of the libcurl implementation details for get. Can optionally send
 * auth headers.
 *
 * \note
 * This function dynamically allocates space for the response. It must be freed
 * after use.
 *
 * \note Not thread safe.
 *
 * \param[in]  url              The url to get.
 * \param[out] response         Pointer to hold the response.
 * \param[out] response_headers Pointer to hold the headers.
 * \param[in]  auth_token       Wether or not auth headers should be included.
 * \param[in]  keep_going       Wether or not to continue waiting on get.
 *
 * \returns HTTP status code of the get request.
 */
long
get(const char *url,
    const char **response,
    const char **response_headers,
    const char *auth_token,
    volatile bool *running);

/**
 * \brief Performs a post to the given url.
 *
 * Wraps all of the lincurl implementation details for post. Must be sent with
 * an auth token.
 *
 * \note Not thread safe.
 *
 * \param[in] url         The url to post.
 * \param[in] post_body   Data to post to the url.
 * \param[in] auth_token  The authentication token to use.
 *
 * \returns HTTP status code of the post request.
 */
long
post(const char *url, const char *post_body, const char *auth_token);

#endif /* UTILS_CURL */
