#ifndef __UTILS_DBUS__
#define __UTILS_DBUS__

#include <gio/gio.h>
#include <stdbool.h>
#include <stdint.h>

bool is_jupiter(const char *bt_addr);

bool dbus_proxy_is_read_secure(GDBusProxy *proxy);
bool dbus_proxy_is_read_data(GDBusProxy *proxy);
bool dbus_proxy_is_service(GDBusProxy *proxy);
bool dbus_proxy_is_device(GDBusProxy *proxy, GDBusProxy *device_proxy);

const char *dbus_adapter_path(const char *device);
const char *dbus_device_path(const char *bt_addr, const char *device);
const char *dbus_service_path(uint8_t lock_gen, const char *bt_addr, const char *device);
const char *dbus_read_secure_path(uint8_t lock_gen, const char *bt_addr, const char *device);
const char *dbus_write_secure_path(uint8_t lock_gen, const char *bt_addr, const char *device);
const char *dbus_read_data_path(uint8_t lock_gen, const char *bt_addr, const char *device);
const char *dbus_write_data_path(uint8_t lock_gen, const char *bt_addr, const char *device);
const char *dbus_char_path(
    uint8_t lock_gen,
    const char *bt_addr,
    const char *device,
    const char *characteristic);

GDBusProxy *dbus_generic_proxy(const char *object, const char *interface);
GDBusProxy *dbus_adapter_proxy(const char *device);
GDBusProxy *dbus_device_proxy(const char *bt_addr, const char *device);
GDBusProxy *dbus_service_proxy(uint8_t lock_gen, const char *bt_addr, const char *device);
GDBusProxy *dbus_read_secure_proxy(uint8_t lock_gen, const char *bt_addr, const char *device);
GDBusProxy *dbus_write_secure_proxy(uint8_t lock_gen, const char *bt_addr, const char *device);
GDBusProxy *dbus_read_data_proxy(uint8_t lock_gen, const char *bt_addr, const char *device);
GDBusProxy *dbus_write_data_proxy(uint8_t lock_gen, const char *bt_addr, const char *device);

GDBusObjectManager *dbus_object_manager(void);

GError *dbus_adapter_start_service_discovery(GDBusProxy *proxy);
GError *dbus_adapter_start_discovery(GDBusProxy *proxy);
GError *dbus_adapter_stop_discovery(GDBusProxy *proxy);
GError *dbus_adapter_remove_device(GDBusProxy *proxy, const char *path);

GError *dbus_device_connect(GDBusProxy *proxy);
GError *dbus_device_disconnect(GDBusProxy *proxy);

GError *dbus_char_start_notify(GDBusProxy *proxy);
GError *dbus_char_stop_notify(GDBusProxy *proxy);
GError *dbus_char_write_value(GDBusProxy *proxy, const char *data);

bool dbus_object_exists(GDBusProxy *proxy);

bool dbus_service_find_august_characteristics(GDBusProxy *proxy);

bool dbus_device_is_connected(GDBusProxy *proxy);
bool dbus_device_service_discovery_complete(GDBusProxy *proxy);

void dbus_error_check_bluetooth_daemon(GError *error);
void dbus_error_check_notifying_fatal(GError *error);

GDBusProxy *dbus_ble_scan_proxy(void);
GError *dbus_ble_scan_start(GDBusProxy *proxy);
GError *dbus_ble_scan_stop(GDBusProxy *proxy);
void dbus_ble_scan_set_properties(
  GDBusProxy *proxy,
  gint32 hci_id,
  gint32 scan_type,
  gint32 scan_window,
  gint32 scan_interval,
  const gchar *mac_address);

void dbus_cleanup(void);

#endif /* UTILS_DBUS */
