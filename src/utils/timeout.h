#ifndef __TIMEOUT_H__
#define __TIMEOUT_H__

#include <glib.h>

unsigned long clear_timeout(unsigned long timeout_id);

unsigned long create_timeout(
    unsigned int timeout_sec,
    GSourceFunc callback,
    void *data);

#endif /* TIMEOUT_H */
