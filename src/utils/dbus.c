#include <ctype.h>
#include <errno.h>
#include <gio/gio.h>
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "august/aug_lock_ble_properties.h"

#include "utils/dbus.h"
#include "utils/logging.h"
#include "utils/string.h"

#define ADAPTER_INTERFACE             "org.bluez.Adapter1"
#define DEVICE_INTERFACE              "org.bluez.Device1"
#define SERVICE_INTERFACE             "org.bluez.GattService1"
#define CHARACTERISTIC_INTERFACE      "org.bluez.GattCharacteristic1"

enum {
  ASL_DATA_READ_HDL_IDX,
  ASL_SECURITY_READ_HDL_IDX,
  ASL_DATA_WRITE_HDL_IDX,
  ASL_SECURITY_WRITE_HDL_IDX,
  CHARACTERISTICS_IN_SERVICE
};

/* this code has hardcoded BLE paths for the first 5 generations of
 * august smart lock
 */
#define ASL_NUM_GENERATIONS 5

/* these are the ASL service paths... if we're connecting to a known ASL we
 * can skip path enumeration and just use these paths
 */
const char* asl_characteristics
   [ASL_NUM_GENERATIONS][CHARACTERISTICS_IN_SERVICE] =
   {{ "char0021", "char0028", "char001e", "char0025"},  /* Helios */
    { "char0104", "char010b", "char0101", "char0108"},  /* Jupiter */
    { "char0013", "char0018", "char0011", "char0016"},  /* Europa */
    { "char0013", "char0018", "char0011", "char0016"},  /* Callisto */
    { "char0013", "char0018", "char0011", "char0016"}}; /* Unity */

const char* asl_service[ASL_NUM_GENERATIONS] = {
      "service001d",  /* Helios */
      "service0100",  /* Jupiter */
      "service0010",  /* Europa */
      "service0010",  /* Callisto */
      "service0010"}; /* Unity */

#define BLUEZ_PATH                    "/org/bluez"

#define JUPITER_AUGUST_SERVICE        "service0100"
#define HELIOS_AUGUST_SERVICE         "service001d"

#define JUPITER_READ_SECURE_CHAR      "char010b"
#define HELIOS_READ_SECURE_CHAR       "char0028"

#define JUPITER_WRITE_SECURE_CHAR     "char0108"
#define HELIOS_WRITE_SECURE_CHAR      "char0025"

#define JUPITER_READ_DATA_CHAR        "char0104"
#define HELIOS_READ_DATA_CHAR         "char0021"

#define JUPITER_WRITE_DATA_CHAR       "char0101"
#define HELIOS_WRITE_DATA_CHAR        "char001e"

#define DBUS_SYSTEMCTL_LOAD_FAILED    "org.freedesktop.systemd1.LoadFailed"
#define DBUS_BLUEZ_ERROR              "org.bluez.Error.Failed"
#define DBUS_BLUEZ_IN_PROGRESS        "org.bluez.Error.InProgress"

#define DBUS_BLUEZ_ALREADY_NOTIFYING  "GDBus.Error:" DBUS_BLUEZ_ERROR ": Already notifying"
#define DBUS_BLUEZ_NO_DISCOVERY       "GDBus.Error:" DBUS_BLUEZ_ERROR ": No discovery started"
#define DBUS_BLUEZ_NOT_READY          "GDBus.Error:" DBUS_BLUEZ_ERROR ": Resource Not Ready"

#define DBUS_UTIL_QUARK               "util/dbus Error:"
#define DBUS_UTIL_ERROR_NO_OBJ        0

G_DEFINE_QUARK(DBUS_UTIL_QUARK, dbus_util);

const gchar* dbus_write_path = NULL;
const gchar* dbus_read_path = NULL;
const gchar* dbus_security_write_path = NULL;
const gchar* dbus_security_read_path = NULL;

// Private variables
static GDBusObjectManager *object_manager;

// Private functions
static GError *object_exists(GDBusProxy *proxy);
static const char *dbus_transform_bt_addr(const char *bt_addr);

static GVariant *dbus_call(
    GDBusProxy *proxy,
    const char *method,
    GVariant *args,
    gint timeout_ms,
    GError **error);

bool is_jupiter(const char *bt_addr) {
  return (bool)g_regex_match_simple("78[\\.:]9[cC][\\.:]85.*", bt_addr, 0, 0);
}

GError *object_exists(GDBusProxy *proxy) {
  const char *proxy_path = NULL;
  GDBusObject *object = NULL;

  if (NULL == object_manager) object_manager = dbus_object_manager();
  proxy_path = g_dbus_proxy_get_object_path(proxy);
  object = g_dbus_object_manager_get_object(object_manager, proxy_path);

  if (object != NULL) {
    g_object_unref(object);
    return NULL;
  }

  return g_error_new(
    dbus_util_quark(),
    DBUS_UTIL_ERROR_NO_OBJ,
    "no object found for %s",
    proxy_path);
}

const char *dbus_transform_bt_addr(const char *bt_addr) {
  GRegex *regex = NULL;
  gchar *new_bt_addr = NULL;
  char c = '\0';
  int i = 0;

  if (NULL == bt_addr)
    die(EAGAIN, "Need non-null ble address to build proxies.");

  regex = g_regex_new("\\.|:", 0, 0, NULL);
  new_bt_addr = g_regex_replace(
      regex,
      bt_addr,
      strlen(bt_addr),
      0,
      "_",
      0,
      NULL);

  while ((c = (char)new_bt_addr[i]) != '\0') {
    new_bt_addr[i++] = toupper(c);
  }

  if (regex != NULL) g_regex_unref(regex);

  return (const char *)new_bt_addr;
}


bool dbus_proxy_is_read_secure(GDBusProxy *proxy) {
  const char *object_path = g_dbus_proxy_get_object_path(proxy);
  return (bool)g_regex_match_simple(
      JUPITER_READ_SECURE_CHAR "|" HELIOS_READ_SECURE_CHAR,
      object_path,
      0,
      G_REGEX_MATCH_PARTIAL);
}

bool dbus_proxy_is_read_data(GDBusProxy *proxy) {
  const char *object_path = g_dbus_proxy_get_object_path(proxy);
  return (bool)g_regex_match_simple(
      JUPITER_READ_DATA_CHAR "|" HELIOS_READ_DATA_CHAR,
      object_path,
      0,
      G_REGEX_MATCH_PARTIAL);
}

bool dbus_proxy_is_service(GDBusProxy *proxy) {
  const char *object_path = g_dbus_proxy_get_object_path(proxy);
  return (bool)g_regex_match_simple(
      JUPITER_AUGUST_SERVICE "|" HELIOS_AUGUST_SERVICE,
      object_path,
      0,
      G_REGEX_MATCH_PARTIAL);
}

bool dbus_proxy_is_device(GDBusProxy *proxy, GDBusProxy *device_proxy) {
  const char *object_path = g_dbus_proxy_get_object_path(proxy);
  const char *device_path = g_dbus_proxy_get_object_path(device_proxy);
  return (0 == strcmp(object_path, device_path));
}

const char *dbus_adapter_path(const char *device) {
  if (NULL == device)
    return string_new_concat(2, BLUEZ_PATH, "/hci0");
  else
    return string_new_concat(3, BLUEZ_PATH, "/", device);
}

const char *dbus_device_path(const char *bt_addr, const char *device) {
  const char *munged_address = NULL;
  const char *adapter_path = NULL;
  const char *device_path = NULL;

  munged_address = dbus_transform_bt_addr(bt_addr);
  adapter_path = dbus_adapter_path(device);
  device_path = string_new_concat(3, adapter_path, "/dev_", munged_address);

  free((void *)munged_address);
  free((void *)adapter_path);
  return device_path;
}

const char *dbus_service_path(uint8_t lock_gen, const char *bt_addr, const char *device) {
  const char *device_path = NULL;
  const char *service_path = NULL;

  device_path = dbus_device_path(bt_addr, device);

  service_path = string_new_concat(3, device_path, "/",
    asl_service[lock_gen - 1]);

  free((void *)device_path);
  return service_path;
}

const char *dbus_char_path(
    uint8_t lock_gen,
    const char *bt_addr,
    const char *device,
    const char *characteristic) {

  const char *service_path = NULL;
  const char *char_path = NULL;

  service_path = dbus_service_path(lock_gen, bt_addr, device);
  char_path = string_new_concat(3, service_path, "/", characteristic);

  free((void *)service_path);
  return char_path;
}
const char *dbus_read_secure_path(uint8_t lock_gen, const char *bt_addr, const char *device) {
  if (dbus_security_read_path)
    return strdup(dbus_security_read_path);

  return dbus_char_path(lock_gen, bt_addr, device,
    asl_characteristics[lock_gen][ASL_SECURITY_READ_HDL_IDX]);
}

const char *dbus_write_secure_path(uint8_t lock_gen, const char *bt_addr, const char *device) {
  if (dbus_security_write_path)
    return strdup(dbus_security_write_path);

  return dbus_char_path(lock_gen, bt_addr, device,
    asl_characteristics[lock_gen][ASL_SECURITY_WRITE_HDL_IDX]);
}

const char *dbus_read_data_path(uint8_t lock_gen, const char *bt_addr, const char *device) {
  if (dbus_read_path)
    return strdup(dbus_read_path);

  return dbus_char_path(lock_gen, bt_addr, device,
    asl_characteristics[lock_gen][ASL_DATA_READ_HDL_IDX]);
}

const char *dbus_write_data_path(uint8_t lock_gen, const char *bt_addr, const char *device) {
  if (dbus_write_path)
    return strdup(dbus_write_path);

  return dbus_char_path(lock_gen, bt_addr, device,
    asl_characteristics[lock_gen][ASL_DATA_WRITE_HDL_IDX]);
}

GDBusProxy *dbus_generic_proxy(const char *object, const char *interface) {
  GError *error = NULL;
  GDBusProxy *proxy = NULL;

  proxy = g_dbus_proxy_new_for_bus_sync(
      G_BUS_TYPE_SYSTEM,
      G_DBUS_PROXY_FLAGS_NONE,
      NULL,
      "org.bluez",
      object,
      interface,
      NULL,
      &error);
  if (NULL == proxy)
    die(EAGAIN, "Could not get a proxy to the adapter");

  g_clear_error(&error);
  return proxy;
}

GDBusProxy *dbus_adapter_proxy(const char *device) {
  GDBusProxy *adapter_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_adapter_path(device);
  adapter_proxy = dbus_generic_proxy(object_path, ADAPTER_INTERFACE);

  free((void *)object_path);
  return adapter_proxy;
}

GDBusProxy *dbus_device_proxy(const char *bt_addr, const char *device) {
  GDBusProxy *device_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_device_path(bt_addr, device);
  device_proxy = dbus_generic_proxy(object_path, DEVICE_INTERFACE);

  free((void *)object_path);
  return device_proxy;
}

GDBusProxy *dbus_service_proxy(uint8_t lock_gen, const char *bt_addr, const char *device) {
  GDBusProxy *service_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_service_path(lock_gen, bt_addr, device);
  service_proxy = dbus_generic_proxy(object_path, SERVICE_INTERFACE);

  free((void *)object_path);
  return service_proxy;
}

GDBusProxy *dbus_read_secure_proxy(uint8_t lock_gen, const char *bt_addr, const char *device) {
  GDBusProxy *char_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_read_secure_path(lock_gen, bt_addr, device);
  char_proxy = dbus_generic_proxy(object_path, CHARACTERISTIC_INTERFACE);

  free((void *)object_path);
  return char_proxy;
}

GDBusProxy *dbus_write_secure_proxy(uint8_t lock_gen, const char *bt_addr, const char *device) {
  GDBusProxy *char_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_write_secure_path(lock_gen, bt_addr, device);
  char_proxy = dbus_generic_proxy(object_path, CHARACTERISTIC_INTERFACE);

  free((void *)object_path);
  return char_proxy;
}

GDBusProxy *dbus_read_data_proxy(uint8_t lock_gen, const char *bt_addr, const char *device) {
  GDBusProxy *char_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_read_data_path(lock_gen, bt_addr, device);
  char_proxy = dbus_generic_proxy(object_path, CHARACTERISTIC_INTERFACE);

  free((void *)object_path);
  return char_proxy;
}

GDBusProxy *dbus_write_data_proxy(uint8_t lock_gen, const char *bt_addr, const char *device) {
  GDBusProxy *char_proxy = NULL;
  const char *object_path = NULL;

  object_path = dbus_write_data_path(lock_gen, bt_addr, device);
  char_proxy = dbus_generic_proxy(object_path, CHARACTERISTIC_INTERFACE);

  free((void *)object_path);
  return char_proxy;
}

GDBusObjectManager *dbus_object_manager(void) 
{
  GDBusObjectManager *manager = NULL;
  GError *error = NULL;
  
  manager = g_dbus_object_manager_client_new_for_bus_sync(
      G_BUS_TYPE_SYSTEM,
      G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_DO_NOT_AUTO_START,
      "org.bluez",
      "/",
      NULL,
      NULL,
      NULL,
      NULL,
      &error);

  if (NULL == manager) {
    die(EAGAIN, "couldn't create manager: %s", error->message);
  }

  g_clear_error(&error);

  return manager;
}

// MARS-528
void dbus_error_check_bluetooth_daemon(GError *error) {
  const char *dbus_error_name = g_dbus_error_get_remote_error(error);
  if (NULL == dbus_error_name)
    return;
  if (1 == strcmp(dbus_error_name, DBUS_SYSTEMCTL_LOAD_FAILED))
    die(EAGAIN, "Bluetooth daemon has crashed.");
}

// MARS-517
// We let "In Progress" and "Already Notifying", "No discovery" pass through,
// the rest we consider fatal.
void dbus_error_check_notifying_fatal(GError *error) {
  const char *dbus_error_name = NULL;

  dbus_error_name = g_dbus_error_get_remote_error(error);

  if ((0 == strcmp(dbus_error_name, DBUS_BLUEZ_ERROR)) &&
      ((0 == strcmp(error->message, DBUS_BLUEZ_ALREADY_NOTIFYING)) ||
      (0 == strcmp(error->message, DBUS_BLUEZ_NO_DISCOVERY)))) {
    return;
  } else if (0 == strcmp(dbus_error_name, DBUS_BLUEZ_IN_PROGRESS)) {
    return;
  } else if (0 == strcmp(dbus_error_name, DBUS_BLUEZ_NOT_READY)) {
    return;
  } else {
    goto die;
  }
  return;

die:
  die(EAGAIN, "notify error: (%d) %s", error->code, error->message);
}

GVariant *dbus_call(
    GDBusProxy *proxy,
    const char *method,
    GVariant *args,
    gint timeout_ms,
    GError **error) {

  GVariant *value = NULL;

  // *error = object_exists(proxy);
  // if (*error != NULL) return value;

  value = g_dbus_proxy_call_sync(
     proxy,
     method,
     args,
     G_DBUS_CALL_FLAGS_NONE,
     timeout_ms,
     NULL,
     error);

  if (!value) {
    // Check for especially nasty/tricky errors here.
    //dbus_error_check_bluetooth_daemon(*error);
    //dbus_error_check_notifying_fatal(*error);
  }

  return value;
}

GError *dbus_adapter_start_service_discovery(GDBusProxy *proxy) {
  GVariant *dict = NULL;
  GVariant *uuids = NULL;
  GVariant *value = NULL;
  GVariantBuilder *bob = NULL;
  GVariantBuilder *jim = NULL;
  GError *error = NULL;

  /* setting a discovery filter tricks bluez into starting service discovery.
   * why am i even writing a note for such intuitive behavior?
   */
  jim = g_variant_builder_new(G_VARIANT_TYPE("as"));
  g_variant_builder_add(jim, "s", kAugLockServiceUUID);
  g_variant_builder_add(jim, "s", kAugLockServiceUUID16);
  uuids = g_variant_new("as", jim);
  g_variant_builder_unref(jim);

  bob = g_variant_builder_new(G_VARIANT_TYPE("a{sv}"));
  g_variant_builder_add(bob, "{sv}", "UUIDs", uuids);
  g_variant_builder_add(bob, "{sv}", "Transport", g_variant_new_string("le"));
  dict = g_variant_new("(a{sv})", bob);
  g_variant_builder_unref(bob);

  value = dbus_call(proxy, "org.bluez.Adapter1.SetDiscoveryFilter",
     dict, 500, &error);
     
  if (value != NULL)
    g_variant_unref(value);

  if (error)
    g_warning("set discovery filter says %s", error->message);

  g_clear_error(&error);

  value = dbus_call(proxy, "org.bluez.Adapter1.StartDiscovery", NULL, 500, &error);
  if (value != NULL) g_variant_unref(value);

  return error;
} /* dbus_adapter_start_service_discovery */

GError *dbus_adapter_start_discovery(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(proxy, "org.bluez.Adapter1.StartDiscovery", NULL, 500, &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_adapter_stop_discovery(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(proxy, "org.bluez.Adapter1.StopDiscovery", NULL, 500, &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_adapter_remove_device(GDBusProxy *proxy, const char *path) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(
    proxy,
    "org.bluez.Adapter1.RemoveDevice",
    g_variant_new("(o)", strdup(path)),
    500, &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_device_connect(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(proxy, "org.bluez.Device1.Connect", NULL, 500, &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_device_disconnect(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(proxy, "org.bluez.Device1.Disconnect", NULL, -1, &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_char_start_notify(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(
    proxy,
    "org.bluez.GattCharacteristic1.StartNotify",
    NULL, 500,
    &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_char_stop_notify(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  value = dbus_call(
    proxy,
    "org.bluez.GattCharacteristic1.StopNotify",
    NULL, 500,
    &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

GError *dbus_char_write_value(GDBusProxy *proxy, const char *data) {
  GVariant *args = NULL;
  GVariant *value = NULL;
  GError *error = NULL;

  args = string_hex_to_bytes(data);
  value = dbus_call(
    proxy,
    "org.bluez.GattCharacteristic1.WriteValue",
    args, 500,
    &error);
  if (value != NULL) g_variant_unref(value);

  return error;
}

/*
 * dbus_service_find_august_characteristics
 *
 * iterate through the characteristics on the service proxy to find
 * the August lock characteristics
 */
bool dbus_service_find_august_characteristics(
  GDBusProxy *service_proxy) {

  GError *error = NULL;
  GVariant *characteristics = NULL;
  int discovered = 0;

  characteristics = g_dbus_proxy_call_sync(
    service_proxy,
    "org.freedesktop.DBus.Properties.Get",
    g_variant_new("(ss)", "org.bluez.GattService1",
      "Characteristics"),
    G_DBUS_CALL_FLAGS_NONE, 500, NULL, &error);

  if (characteristics && g_variant_is_container(characteristics)) {
    GVariantIter char_iter;
    GVariant *char_var;

    g_variant_iter_init(&char_iter, characteristics);

    while ((char_var = g_variant_iter_next_value(&char_iter))) {
      GVariant *char_array = g_variant_get_variant(char_var);
      GVariant *char_new = NULL;
      int index = 0;

      while (index < g_variant_n_children(char_array)) {
        GDBusProxy *char_proxy = NULL;
        GVariant *char_uuid_tuple = NULL;
        GVariant *char_uuid = NULL;

        char_new = g_variant_get_child_value(char_array, index++);

        char_proxy = dbus_generic_proxy(
          g_variant_get_string(char_new, NULL),
          "org.bluez.GattCharacteristic1");

        g_debug("char path %s",
          g_variant_get_string(char_new, NULL));

        char_uuid_tuple = g_dbus_proxy_call_sync(
          char_proxy,
          "org.freedesktop.DBus.Properties.Get",
          g_variant_new("(ss)",
            "org.bluez.GattCharacteristic1", "UUID"),
          G_DBUS_CALL_FLAGS_NONE, 500, NULL, &error);

        if (char_uuid_tuple && g_variant_is_container(char_uuid_tuple)) {
          g_variant_get(char_uuid_tuple, "(v)", &char_uuid);

          g_debug("char uuid %s", g_variant_print(char_uuid, true));

          if (0 == strcasecmp(kAugLockWriteUUID,
            g_variant_get_string(char_uuid, NULL))) {
            dbus_write_path =
              g_variant_dup_string(char_new, NULL);
            discovered++;
          } else if (0 == strcasecmp(kAugLockReadUUID,
            g_variant_get_string(char_uuid, NULL))) {
            dbus_read_path =
              g_variant_dup_string(char_new, NULL);
            discovered++;
          } else if (0 == strcasecmp(kAugLockSecurityWriteUUID,
            g_variant_get_string(char_uuid, NULL))) {
            dbus_security_write_path =
              g_variant_dup_string(char_new, NULL);
            discovered++;
          } else if (0 == strcasecmp(kAugLockSecurityReadUUID,
            g_variant_get_string(char_uuid, NULL))) {
            dbus_security_read_path =
              g_variant_dup_string(char_new, NULL);
            discovered++;
          }

          g_variant_unref(char_uuid_tuple);
          g_variant_unref(char_uuid);
        } /* if */
        g_variant_unref(char_new);
        g_object_unref(char_proxy);
      } /* while */

      g_variant_unref(char_array);
      g_variant_unref(char_var);
    } /* while */
  } /* if */

  if (error) {
    g_warning("%s %s", __FUNCTION__, error->message);
    g_error_free(error);
  } /* if */

  if (characteristics)
    g_variant_unref(characteristics);

  return (discovered == 4);
} /* dbus_service_find_august_characteristics */

bool dbus_device_is_connected(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;
  bool ret = false;

  if (!dbus_object_exists(proxy))
    goto done;

  value = dbus_call(
    proxy,
    "org.freedesktop.DBus.Properties.Get",
    g_variant_new("(ss)", "org.bluez.Device1", "Connected"), -1,
    &error);

  g_clear_error(&error);

  if (value != NULL) {
    GVariant *gv_connected = NULL;

    g_variant_get(value, "(v)", &gv_connected);
    ret = g_variant_get_boolean(gv_connected);

    g_variant_unref(gv_connected);
    g_variant_unref(value);
  }

done:
  g_debug("device %sconnected",
    (ret ? "" : "not "));

  return ret;
}

bool dbus_device_service_discovery_complete(GDBusProxy *proxy) {
  GVariant *tuple = NULL;
  GVariant *variant = NULL;
  GVariant *array = NULL;
  GError *error = NULL;
  int num_characteristics = -1;

  if (!dbus_object_exists(proxy))
    return false;

  tuple = dbus_call(
    proxy,
    "org.freedesktop.DBus.Properties.Get",
    g_variant_new("(ss)", "org.bluez.GattService1", "Characteristics"),
    -1, &error);

  if (NULL == tuple) {
    g_warning("service discovery error: %s", error->message);
    g_clear_error(&error);
    return false;
  }

  variant = g_variant_get_child_value(tuple, 0);
  array = g_variant_get_child_value(variant, 0);
  if (array) {
    num_characteristics = g_variant_n_children(array);
  }

  g_variant_unref(tuple);
  g_variant_unref(variant);
  g_variant_unref(array);

  return (num_characteristics >= CHARACTERISTICS_IN_SERVICE);
}

bool dbus_object_exists(GDBusProxy *proxy) {
  GError *error = NULL;
  bool exists = false;

  error = object_exists(proxy);
  exists = (NULL == error) ? true : false;

  g_clear_error(&error);

  return exists;
}

GDBusProxy *dbus_ble_scan_proxy(void) {
  GError *error = NULL;
  GDBusProxy *proxy = NULL;

  proxy = g_dbus_proxy_new_for_bus_sync(
      G_BUS_TYPE_SYSTEM,
      G_DBUS_PROXY_FLAGS_NONE,
      NULL,
      "com.august.BLEScan",
      "/com/august/BLEScan",
      "com.august.BLEScan",
      NULL,
      &error);
  if (NULL == proxy)
    die(EAGAIN, "Could not get a proxy to the scan interface");

  g_clear_error(&error);
  return proxy;
} /* dbus_ble_scan_proxy */

GError *dbus_ble_scan_start(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  g_debug("scan start");

  /* clear any cached scan results */
  g_dbus_proxy_set_cached_property(proxy, "ScanResponse", NULL);

  value = g_dbus_proxy_call_sync(
     proxy,
     "Start",
     NULL,
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);

  return error;
} /* dbus_ble_scan_start */

GError *dbus_ble_scan_stop(GDBusProxy *proxy) {
  GVariant *value = NULL;
  GError *error = NULL;

  g_debug("scan stop");

  value = g_dbus_proxy_call_sync(
     proxy,
     "Stop",
     NULL,
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);

  return error;
} /* dbus_ble_scan_stop */

void dbus_ble_scan_set_properties(
  GDBusProxy *proxy,
  gint32 hci_id,
  gint32 scan_type,
  gint32 scan_window,
  gint32 scan_interval,
  const gchar *mac_address) {

  GVariant *value = NULL;
  GError *error = NULL;

  value = g_dbus_proxy_call_sync(
     proxy,
     "org.freedesktop.DBus.Properties.Set",
     g_variant_new("(ssv)", "com.august.BLEScan", "Device",
       g_variant_new_int32(hci_id)),
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);
  g_clear_error(&error);

  value = g_dbus_proxy_call_sync(
     proxy,
     "org.freedesktop.DBus.Properties.Set",
     g_variant_new("(ssv)", "com.august.BLEScan", "Type",
       g_variant_new_int32(scan_type)),
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);
  g_clear_error(&error);

  value = g_dbus_proxy_call_sync(
     proxy,
     "org.freedesktop.DBus.Properties.Set",
     g_variant_new("(ssv)", "com.august.BLEScan", "Window",
       g_variant_new_int32(scan_window)),
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);
  g_clear_error(&error);

  value = g_dbus_proxy_call_sync(
     proxy,
     "org.freedesktop.DBus.Properties.Set",
     g_variant_new("(ssv)", "com.august.BLEScan", "Interval",
       g_variant_new_int32(scan_interval)),
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);
  g_clear_error(&error);

  value = g_dbus_proxy_call_sync(
     proxy,
     "org.freedesktop.DBus.Properties.Set",
     g_variant_new("(ssv)", "com.august.BLEScan", "AddrFilter",
       (mac_address ? 
        g_variant_new_string(mac_address) : g_variant_new_string(""))),
     G_DBUS_CALL_FLAGS_NONE,
     -1,
     NULL,
     &error);
  if (value != NULL) g_variant_unref(value);
  g_clear_error(&error);
} /* dbus_ble_scan_set_properties */

void dbus_cleanup(void) {
  if (dbus_write_path) {
    free((void*)dbus_write_path);
    dbus_write_path = NULL;
  } /* if */

  if (dbus_read_path) {
    free((void*)dbus_read_path);
    dbus_read_path = NULL;
  } /* if */

  if (dbus_security_write_path) {
    free((void*)dbus_security_write_path);
    dbus_security_write_path = NULL;
  } /* if */

  if (dbus_security_read_path) {
    free((void*)dbus_security_read_path);
    dbus_security_read_path = NULL;
  } /* if */

  if (object_manager != NULL) g_object_unref(object_manager);
}
