#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils/environ.h"

// Private functions
static const char *env_or_default(const char *key, const char *default_val) {
  const char *value = getenv(key);
  return (NULL == value) ? default_val : value;
}

const char *get_mars_cert_path(void) {
  return env_or_default(ENV_MARS_CERT, MARS_CERT);
}

#if KEISTER_ENABLED == 0
const char *get_mars_key_path(void) {
  return env_or_default(ENV_MARS_KEY, MARS_KEY);
}
#endif

const char *get_lock_mac_path(void) {
  return env_or_default(ENV_MARS_LOCK_MAC, MARS_LOCK_MAC);
}

const char *get_lock_gen_path(void) {
  return env_or_default(ENV_MARS_LOCK_GEN, MARS_LOCK_GEN);
}

const char *get_lock_id_path(void) {
  return env_or_default(ENV_MARS_LOCK_ID, MARS_LOCK_ID);
}

const char *get_firmare_version_path(void) {
  return env_or_default(ENV_MARS_FIRMWARE_VERSION_PATH, MARS_FIRMWARE_VERSION_PATH);
}

const char *get_http_cookie_path(void) {
  return env_or_default(ENV_MARS_HTTP_COOKIE_FILE, MARS_HTTP_COOKIE_FILE);
}

const char *get_ca_cert_path(void) {
  return env_or_default(ENV_MARS_CA, MARS_CA);
}

unsigned int
get_timeout_value(const char *timeout_var) {
  unsigned int timeout_val = 0;
  const char *value = env_or_default(timeout_var, NULL);
  if (value) {
    sscanf(value, "%u", &timeout_val);
    return timeout_val;
  }

  if (0 == strcmp(ENV_CONNECT_TIMEOUT, timeout_var)) {
    return CONNECT_TIMEOUT;
  } else if (0 == strcmp(ENV_DISCOVER_TIMEOUT, timeout_var)) {
    return DISCOVER_TIMEOUT;
  } else if (0 == strcmp(ENV_REGISTER_TIMEOUT, timeout_var)) {
    return REGISTER_TIMEOUT;
  } else if (0 == strcmp(ENV_REMOVE_TIMEOUT, timeout_var)) {
    return REMOVE_TIMEOUT;
  } else if (0 == strcmp(ENV_RESEND_DIRTY_BIT_TIMEOUT, timeout_var)) {
    return RESEND_DIRTY_BIT_TIMEOUT;
  } else if (0 == strcmp(ENV_SCAN_TIMEOUT, timeout_var)) {
    return SCAN_TIMEOUT;
  } else if (0 == strcmp(ENV_ADVERTISING_TIMEOUT, timeout_var)) {
    return ADVERTISING_TIMEOUT;
  } else if (0 == strcmp(ENV_WAIT_FOR_CONNECTION, timeout_var)) {
    return WAIT_FOR_CONNECTION;
  } else if (0 == strcmp(ENV_GRATUITOUS_DISCONNECT_GUARD_TIME, timeout_var)) {
    return GRATUITOUS_DISCONNECT_GUARD_TIME;
  } else if (0 == strcmp(ENV_CURL_TIMEOUT, timeout_var)) {
    return CURL_TIMEOUT;
  } else {
    return 0;
  }
}

bool verify_host_cert(void) {
  return ((NULL != getenv(ENV_VERIFY_HOST_CERT)) &&
    (0 == strncmp("true", getenv(ENV_VERIFY_HOST_CERT), 5)));
} /* verify_host_cert */

int get_connect_retries(void) {
  const char *value = getenv(ENV_CONNECT_RETRIES);
  return (NULL == value) ? CONNECT_RETRIES: atoi(value);
}
