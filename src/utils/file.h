#ifndef __UTILS_FILE__
#define __UTILS_FILE__

#include <stdbool.h>

/**
 * \brief Saves string to file.
 *
 * Takes a string and a file path and saves the string to the file path.
 *
 * \param[in] value   The value to save to file.
 * \param[in] path    The path of the file to write.
 *
 * \returns Non-negative number if success, EOF if failure.
 */
int file_write(const char *value, const char *path);

/**
 * \brief Reads string from file.
 *
 * \param[in] path    The path of the file to read.
 *
 * \returns The string in the file.
 */
const char *file_read(const char *path);

/**
 * \brief Checks to see if file exists.
 *
 * \param[in] path    The path of the file to check.
 *
 * \returns True if the file exists, false if the file does not exist.
 */
bool file_exists(const char *path);

#endif /* UTILS_FILE */
