#include <errno.h>
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "utils/file.h"
#include "utils/logging.h"

int file_write(const char *value, const char *path) {
  int result = -1;
  FILE *outfile = NULL;

  outfile = fopen(path, "w");
  if (NULL == outfile)
    die(EAGAIN, "Could not open '%s' for write: %s", path, strerror(errno));

  result = fputs(value, outfile);
  if (EOF == result)
    die(EAGAIN, "Could not write '%s' to '%s'", value, path);

  fclose(outfile);

  return result;
}

const char *file_read(const char *path) {
  char *file_contents = NULL;
  FILE *file = NULL;
  struct stat file_info = {0};

  file = fopen(path, "r");
  if (NULL == file) {
    die(EAGAIN, "Could not open '%s'", path);
  }

  if (fstat(fileno(file), &file_info) != 0) {
    die(EAGAIN, "Could not stat '%s'", path);
  }

  if (file_info.st_size < 1) {
    return NULL;
  }

  file_contents = (char *)malloc(1 + file_info.st_size);
  if (NULL == file_contents)
    die(EAGAIN, "Could not allocate memory for contents of %s", path);
  memset((void *)file_contents, 0, 1 + file_info.st_size);

  fscanf(file, "%s", file_contents);
  fclose(file);

  return (const char *)file_contents;
}

bool file_exists(const char *path) {
  return (0 == access(path, F_OK));
}
