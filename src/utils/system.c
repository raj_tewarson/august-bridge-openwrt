#include <errno.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>

long
uptime(void) {
  struct sysinfo info;

  if (sysinfo(&info) != 0) {
    int errsv = errno;
    g_warning("Could not get sysinfo: %s", strerror(errsv));
    return -1L;
  }

  return info.uptime;
}

unsigned long
freeram(void) {
  struct sysinfo info;

  if (sysinfo(&info) != 0) {
    int errsv = errno;
    g_warning("Could not get sysinfo: %s", strerror(errsv));
    return 0L;
  }

  return info.freeram;
}

int
wlan_rssi(void) {
  FILE *f;
  char line[256];
  int link = -1;
  int level = -1;

  if (NULL == (f = fopen("/proc/net/wireless", "r"))) {
    int errsv = errno;
    g_warning("Could not open /proc/net/wireless: %s", strerror(errsv));
    return 0;
  }

  fgets(line, sizeof(line), f); /* header */
  fgets(line, sizeof(line), f); /* header */
  fgets(line, sizeof(line), f);
  fclose(f);

  sscanf(line + 13, "%d. %d.", &link, &level);
  return level;
}

int
wlan_snr(void) {
  FILE *f;
  char line[256];
  int link = -1;
  int level = -1;

  if (NULL == (f = fopen("/proc/net/wireless", "r"))) {
    int errsv = errno;
    g_warning("Could not open /proc/net/wireless: %s", strerror(errsv));
    return 0;
  }

  fgets(line, sizeof(line), f); /* header */
  fgets(line, sizeof(line), f); /* header */
  fgets(line, sizeof(line), f);
  fclose(f);

  sscanf(line + 13, "%d. %d.", &link, &level);
  return link;
}
