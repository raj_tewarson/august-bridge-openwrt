#include "utils/timeout.h"

unsigned long clear_timeout(unsigned long timeout_id) {
  GMainContext *thread_context = NULL;
  GSource *timeout = NULL;

  if (timeout_id > 0) {
    thread_context = g_main_context_get_thread_default();
    timeout = g_main_context_find_source_by_id(thread_context, timeout_id);
    if (timeout)
      g_source_destroy(timeout);
  }

  return 0;
}

unsigned long create_timeout(
    unsigned int timeout_sec,
    GSourceFunc callback,
    void *data) {

  unsigned long timeout_id = 0;
  GMainContext *thread_context = NULL;
  GSource *timeout = NULL;

  thread_context = g_main_context_get_thread_default();
  timeout = g_timeout_source_new_seconds(timeout_sec);
  g_source_set_callback(timeout, callback, data, NULL);
  timeout_id = g_source_attach(timeout, thread_context);

  if (timeout != NULL) g_source_unref(timeout);

  return timeout_id;
}
