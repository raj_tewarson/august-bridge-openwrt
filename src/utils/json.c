#include <errno.h>
#include <glib.h>
#include <string.h>

#include "utils/json.h"
#include "utils/logging.h"

#define REGISTER        "register"
#define CONNECT         "connect"
#define WRITE_SECURE    "writeSecure"
#define WRITE_DATA      "write"
#define DISCONNECT      "disconnect"
#define REBOOT          "reboot"
#define CONNECT_WRITE   "connectWrite"

#define COMMAND(X)           (json_peek_string((X), "command"))
#define STR_EQ(X, Y)         (0 == strcmp((X), (Y)))
#define IS_REGISTER(X)       STR_EQ(COMMAND(X), REGISTER)
#define IS_CONNECT(X)        STR_EQ(COMMAND(X), CONNECT)
#define IS_WRITE_SECURE(X)   STR_EQ(COMMAND(X), WRITE_SECURE)
#define IS_WRITE_DATA(X)     STR_EQ(COMMAND(X), WRITE_DATA)
#define IS_DISCONNECT(X)     STR_EQ(COMMAND(X), DISCONNECT)
#define IS_REBOOT(X)         STR_EQ(COMMAND(X), REBOOT)
#define IS_CONNECT_WRITE(X)  STR_EQ(COMMAND(X), CONNECT_WRITE)

static int32_t sequence_num = 0;

// Private functions
static json_object *json_base_message(
  const char *lock_id,
  const char *event,
  bool is_command);

json_object *json_base_message(
  const char *lock_id,
  const char *event,
  bool is_command) {

  json_object *base_message = NULL;
  json_object *lock_id_string = NULL;
  json_object *event_string = NULL;

  base_message = json_object_new_object();
  lock_id_string = json_object_new_string(lock_id);
  event_string = json_object_new_string(event);

  json_object_object_add(
    base_message,
    (is_command) ? "command": "event",
    event_string);
  json_object_object_add(base_message, "lockID", lock_id_string);
  if (!is_command)
    json_object_object_add(base_message, "sequenceNumber", json_object_new_int(sequence_num));
  return base_message;
}

void json_set_sequence_num(int32_t seq_number){
  sequence_num = seq_number; 
}

const char *json_clone_string_value(json_object *data, const char *key) {
  int len = 0;
  char *value = NULL;
  json_object *j_value = NULL;

  if (json_object_object_get_ex(data, key, &j_value)) {
    len = 1 + json_object_get_string_len(j_value);
    value = (char *)malloc(len);
    if (NULL == value)
      die(EAGAIN, "Could not allocate value: %s",
        json_object_get_string(j_value));
    memset(value, 0, len);
    strncpy(value, json_object_get_string(j_value), len);
  }

  return (const char *)value;
}

json_object *
json_connection_state(const char *lock_id, const char *state, const int *rssi) {
  json_object *connection_state = NULL;

  connection_state = json_base_message(lock_id, "connectionState", false);
  json_object_object_add(connection_state, "state",
    json_object_new_string(state));
  
  if (rssi) {
    json_object *rssi_obj = NULL;
    rssi_obj = json_object_new_int(*rssi);
    json_object_object_add(connection_state, "rssi", rssi_obj);
  }

  return connection_state;
}

json_object *json_data_secure(const char *lock_id, const char *data) {
  json_object *data_secure = NULL;
  json_object *data_field = NULL;

  data_secure = json_base_message(lock_id, "dataSecure", false);
  data_field = json_object_new_string(data);
  json_object_object_add(data_secure, "data", data_field);

  return data_secure;
}

json_object *json_data(const char *lock_id, const char *data) {
  json_object *data_secure = NULL;
  json_object *data_field = NULL;

  data_secure = json_base_message(lock_id, "data", false);
  data_field = json_object_new_string(data);
  json_object_object_add(data_secure, "data", data_field);

  return data_secure;
}

int32_t json_peek_int(json_object *data, const char *key) {
  json_object *value = NULL;
  if (json_object_object_get_ex(data, key, &value)) {
    return json_object_get_int(value);
  }

  return 0;  
}

const char *json_peek_string(json_object *data, const char *key) {
  json_object *value = NULL;

  if (json_object_object_get_ex(data, key, &value)) {
    return json_object_get_string(value);
  }

  return NULL;
}

json_command_t json_command_type(json_object *command) {
  if (IS_REGISTER(command))
    return JSON_REGISTER_COMMAND;
  if (IS_CONNECT(command))
    return JSON_CONNECT_COMMAND;
  if (IS_WRITE_SECURE(command))
    return JSON_WRITE_SECURE_COMMAND;
  if (IS_WRITE_DATA(command))
    return JSON_WRITE_DATA_COMMAND;
  if (IS_DISCONNECT(command))
    return JSON_DISCONNECT_COMMAND;
  if (IS_REBOOT(command))
    return JSON_REBOOT_COMMAND;
  if (IS_CONNECT_WRITE(command))
    return JSON_CONNECT_WRITE_COMMAND;

  return JSON_UNKNOWN_COMMAND;
}

json_object *json_connect(const char *lock_id) {
  json_object *connect = NULL;

  connect = json_base_message(lock_id, "connect", true);

  return connect;
}

json_object *json_register(const char *lock_id, const char *lock_mac) {
  json_object *register_cmd = NULL;
  json_object *mac_field = NULL;

  register_cmd = json_base_message(lock_id, "register", true);
  mac_field = json_object_new_string(lock_mac);
  json_object_object_add(register_cmd, "macAddr", mac_field);

  return register_cmd;
}

json_object *json_idle(const char *lock_id) {
  json_object *dirty_event;
  json_object *dirty_field;

  dirty_event = json_base_message(lock_id, "advState", false);
  dirty_field = json_object_new_string("idle");
  json_object_object_add(dirty_event, "state", dirty_field);

  return dirty_event;
}

json_object *json_dirty(const char *lock_id) {
  json_object *dirty_event;
  json_object *dirty_field;

  dirty_event = json_base_message(lock_id, "advState", false);
  dirty_field = json_object_new_string("dirty");
  json_object_object_add(dirty_event, "state", dirty_field);

  return dirty_event;
}

json_object *json_busy(const char *lock_id) {
  json_object *dirty_event;
  json_object *dirty_field;

  dirty_event = json_base_message(lock_id, "advState", false);
  dirty_field = json_object_new_string("busy");
  json_object_object_add(dirty_event, "state", dirty_field);

  return dirty_event;
}

json_object *json_timeout(const char *lock_id) {
  json_object *dirty_event;
  json_object *dirty_field;

  dirty_event = json_base_message(lock_id, "advState", false);
  dirty_field = json_object_new_string("timeout");
  json_object_object_add(dirty_event, "state", dirty_field);

  return dirty_event;
}
