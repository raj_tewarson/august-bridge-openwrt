#ifndef __UTILS_JSON__
#define __UTILS_JSON__

#include <json.h>
#include <stdbool.h>

#define JSON_CONN_STATE_CONNECTED         "connected"
#define JSON_CONN_STATE_DISCONNECTED      "disconnected"
#define JSON_CONN_STATE_CONNECTING        "connecting"
#define JSON_CONN_STATE_ALREADY_CONNECTED "already-connected"

/**
 * \brief Peek a string at the given key.
 *
 * \note This functions string is memory managed by json-c.
 *
 * \param[in] data    The json object.
 * \param[in] key     The key to check for the string.
 *
 * \returns The string at the given key.
 */
const char *json_peek_string(json_object *data, const char *key);

/**
 * \brief Peek an int at the given key.
 *
 * \note This functions string is memory managed by json-c.
 *
 * \param[in] data    The json object.
 * \param[in] key     The key to check for the string.
 *
 * \returns The int32_t at the given key.
 */
int32_t json_peek_int(json_object *data, const char *key);

/**
 * \brief Deep copies the string from the JSON object.
 *
 * Takes a JSON object, finds the given key, and returns the value or NULL.
 * This allows us to free the associated `json_object` without freeing the
 * strings we got via `json_object_get_string`.
 *
 * \note
 * This function dynamically allocates space for the value string. It must be
 * freed after use.
 *
 * \param[in] data    The JSON object to search.
 * \param[in] key     The key to find in the JSON object.
 *
 * \returns The value associated with key or NULL.
 */
const char *json_clone_string_value(json_object *data, const char *key);

/**
 * \brief Generates a connection state packet.
 *
 * Uses the server response to generate a connection state packet. This packet
 * includes the connection state of the lock, and the lock id.
 *
 * \note The returned json object must be `put` after its usefulness elapses.
 *
 * \param[in] response    The json object contaning the server response.
 * \param[in] connected   Wether or not the device connected properly.
 *
 * \returns A connected state json packet.
 */
json_object *
json_connection_state(const char *lock_id, const char *state, const int *rssi);

/**
 * \brief Generates a registration state packet.
 *
 * Uses the server response to generate a registration state packet. This packet
 * includes the registration state of the lock, and the lock id.
 *
 * \note The returned json object must be `put` after its usefulness elapses.
 *
 * \param[in] response    The json object contaning the server response.
 *
 * \returns A connected state json packet.
 */
json_object *json_data_secure(const char *lock_id, const char *data);

json_object *json_data(const char *lock_id, const char *data);

void json_set_sequence_num(int32_t seq_number);

typedef enum {
  JSON_REGISTER_COMMAND = 0x00,
  JSON_CONNECT_COMMAND,
  JSON_WRITE_SECURE_COMMAND,
  JSON_WRITE_DATA_COMMAND,
  JSON_DISCONNECT_COMMAND,
  JSON_REBOOT_COMMAND,
  JSON_UNKNOWN_COMMAND,
  JSON_CONNECT_WRITE_COMMAND,
} json_command_t;

json_command_t json_command_type(json_object *command);

json_object *json_connect(const char *lock_id);
json_object *json_register(const char *lock_id, const char *lock_mac);
json_object *json_idle(const char *lock_id);
json_object *json_dirty(const char *lock_id);
json_object *json_busy(const char *lock_id);
json_object *json_timeout(const char *lock_id);

#endif /* UTILS_JSON */
