#ifndef __LOGGING_H__
#define __LOGGING_H__

#include <glib.h>

#include "august/lock.h"

extern int verbosity;
extern const char *transaction_id;

void custom_log(
    const gchar *log_domain,
    GLogLevelFlags log_level,
    const gchar *message,
    gpointer unused_data);

void die(int code, const char *format, ...)
  __attribute__ ((format (printf, 2, 3)));

#endif /* LOGGING_H */
