#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdint.h>

long
uptime(void);

unsigned long
freeram(void);

int
wlan_rssi(void);

int
wlan_snr(void);

#endif /* SYSTEM_H */
