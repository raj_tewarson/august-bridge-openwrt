#include <glib.h>
#include <inttypes.h>
#include <json.h>
#include <math.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>

#include "logging.h"

#define LOGGER_FIFO "/tmp/logger.fifo"

int verbosity = G_LOG_LEVEL_CRITICAL;
const char *transaction_id = NULL;

static int64_t epoch_in_ms(void) {
  double ms;
  struct timespec spec;
  clock_gettime(CLOCK_REALTIME, &spec);
  ms = spec.tv_nsec / 1.0e6f;
  ms += spec.tv_sec * 1000.0;
  return (int64_t)round(ms);
}

static void post_log(const char *message, int64_t timestamp, int log_level) 
{
  int log_fifo_fd = -1;
  char log_level_string[32] = {0};

  const char *json_string = NULL;
  json_object *log_object = NULL;
  json_object *log_level_j = NULL;
  json_object *domain_string = NULL;
  json_object *timestamp_string = NULL;
  json_object *message_string = NULL;
  json_object *source_string = NULL;
  json_object *transaction_id_string = NULL;

  switch (log_level) {
    case G_LOG_LEVEL_ERROR:
      strncpy(log_level_string, "ERROR", 32);
      break;
    case G_LOG_LEVEL_CRITICAL:
      strncpy(log_level_string, "CRITICAL", 32);
      break;
    case G_LOG_LEVEL_WARNING:
      strncpy(log_level_string, "WARNING", 32);
      break;
    case G_LOG_LEVEL_MESSAGE:
      strncpy(log_level_string, "MESSAGE", 32);
      break;
    case G_LOG_LEVEL_INFO:
      strncpy(log_level_string, "INFO", 32);
      break;
    case G_LOG_LEVEL_DEBUG:
      strncpy(log_level_string, "DEBUG", 32);
      break;
    default:
      break;
  }
  
  //fprintf(stderr, "%s:%s\n",  log_level_string, message);

  return;

  // TODO: -----------------------------------------------------------------

  // Build the json object
  log_object = json_object_new_object();
  log_level_j = json_object_new_string(log_level_string);
  domain_string = json_object_new_string("bridge");
  timestamp_string = json_object_new_int64(timestamp);
  message_string = json_object_new_string(message);
  source_string = json_object_new_string("doorbell");
  if (transaction_id != NULL)
    transaction_id_string = json_object_new_string(transaction_id);

  json_object_object_add(log_object, "loglevel", log_level_j);
  json_object_object_add(log_object, "domain", domain_string);
  json_object_object_add(log_object, "my_timestamp", timestamp_string);
  json_object_object_add(log_object, "message", message_string);
  json_object_object_add(log_object, "source", source_string);
  if (transaction_id != NULL)
    json_object_object_add(log_object, "transaction_id", transaction_id_string);

  json_string = json_object_to_json_string_ext(log_object,
      JSON_C_TO_STRING_PLAIN);

  // Open, write, and close the FIFO
  log_fifo_fd = open(LOGGER_FIFO, O_WRONLY | O_NONBLOCK);
  if (-1 == log_fifo_fd) {
    // use fprintf here to prevent recursing.
    fprintf(stderr, "Unable to open the logger fifo for writing\n");
    goto bail;
  }

  write(log_fifo_fd, json_string, strlen(json_string) + 1);
  write(log_fifo_fd, "\n", 1);
  close(log_fifo_fd);

bail:
  // Free the things
  if (log_object != NULL) json_object_put(log_object);
}

void custom_log(
    const gchar *log_domain,
    GLogLevelFlags log_level,
    const gchar *message,
    gpointer data) {

  if (log_level > verbosity) return;

  int64_t timestamp = epoch_in_ms();
  switch (log_level) {
    case G_LOG_LEVEL_ERROR:
      fprintf(stderr, "** (%" PRId64 ") [txid:%s] ERROR **: %s\n",
          timestamp, transaction_id, message);
      break;
    case G_LOG_LEVEL_CRITICAL:
      fprintf(stderr, "** (%" PRId64 ") [txid:%s] CRITICAL **: %s\n",
          timestamp, transaction_id, message);
      break;
    case G_LOG_LEVEL_WARNING:
      fprintf(stderr, "** (%" PRId64 ") [txid:%s] WARNING **: %s\n",
          timestamp, transaction_id, message);
      break;
    case G_LOG_LEVEL_MESSAGE:
      fprintf(stdout, "** (%" PRId64 ") [txid:%s] MESSAGE **: %s\n",
          timestamp, transaction_id, message);
      break;
    case G_LOG_LEVEL_INFO:
      fprintf(stdout, "** (%" PRId64 ") [txid:%s] INFO **: %s\n",
          timestamp, transaction_id, message);
      break;
    case G_LOG_LEVEL_DEBUG:
      fprintf(stdout, "** (%" PRId64 ") [txid:%s] DEBUG **: %s\n",
          timestamp, transaction_id, message);
      break;
    default:
      break;
  }

  post_log(message, timestamp, (int)log_level);
}

void die(int code, const char *format, ...) {
  int size = 0;
  char *message = NULL;
  va_list args;

  va_start(args, format);
  size = vsnprintf(NULL, 0, format, args);
  message = malloc(size + 1);
  vsprintf(message, format, args);
  g_logv(G_LOG_DOMAIN, G_LOG_LEVEL_CRITICAL, format, args);
  va_end(args);

  exit(code);
}
