#include <assert.h>
#include <errno.h>
#include <glib.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "utils/curl.h"
#include "utils/file.h"
#include "utils/environ.h"
#include "utils/logging.h"
#include "utils/string.h"
#include "utils/system.h"

#if KEISTER_ENABLED == 1
#include "mars_keister.h"
#endif

#define MARS_ICONFIG_ENDPOINT   "/devices/"
#define MARS_SETUP_ENDPOINT     "/bridges/"
#define MARS_LOCKOP_ENDPOINT    "/lockop/"
#define MARS_MESSAGES_ENDPOINT  "/messages/"
#define AUTH_HEADER_KEY         "x-august-bridge-auth-token: "
#define TRANSACTION_ID_HEADER   "x-august-txid: "
#define TRANSACTION_ID_LENGTH   128

#define BRIDGE_FIRMWARE_DEFAULT_VERSION \
  "2.2.1-doorbell"

/**
 * TODO:
 * - Factor our common curl config options into a seperate function.
 */

// Private variables
#if KEISTER_ENABLED == 1
static uint16_t tls_private_key_length;
static uint8_t *tls_private_key;
#endif

static CURL *curl_get;
static CURL *curl_post;
static const char *bridge_firmware_version = NULL;

// Private functions
static int xferinfo(
  void *p,
  curl_off_t dl_total,
  curl_off_t dl_now,
  curl_off_t ul_total,
  curl_off_t ul_now);
static size_t alloc_headers(void *ptr, size_t size, size_t nmemb, void *stream);
static size_t alloc_resp(void *ptr, size_t size, size_t nmemb, void *stream);
static size_t drop_resp(void *ptr, size_t size, size_t nmemb, void *stream);
static const char *build_url(
  const char *server,
  const char *endpoint,
  const char *args);
static const char *build_auth_header(const char *auth_token);
static struct curl_slist *
append_system_stat_headers(struct curl_slist *headers);
static CURLcode ssl_ctx_callback(CURL *curl, void *ssl_ctx, void *user_data);

int xferinfo(void *p,
  curl_off_t dl_total,
  curl_off_t dl_now,
  curl_off_t ul_total,
  curl_off_t ul_now) {

  volatile bool *running = (volatile bool *)p;
  if (false == *running) {
    return 1;
  }
  return 0;
}

size_t alloc_headers(void *ptr, size_t size, size_t nmemb, void *stream) {
  char *temp = NULL;
  char **headers = stream;
  size_t total_size = 1 + size * nmemb;

  /*
   * If we weren't given a place to store our headers, bail.
   */
  if (NULL == headers) {
    return (size *nmemb);
  }

  /*
   * Because alloc_headers will be called once per header, we need to track
   * the total length of the headers, that includes the previous headers.
   */
  if (*headers != NULL) {
    total_size += strlen(*headers);
  }

  temp = (char *)malloc(total_size);
  if (NULL == temp)
    die(EAGAIN, "Could not allocate curl headers");
  memset(temp, 0, total_size);

  /*
   * If we have previously set headers, we need to concat the headers
   * already formatted with this iteration's new headers. Otherwise, we
   * need to set headers to the contents of ptr.
   */
  if (*headers != NULL) {
    snprintf(temp, total_size, "%s\n%s", *headers, (char *)ptr);
    free(*headers);
  } else {
    snprintf(temp, total_size, "%s", (char *)ptr);
  }
  *headers = temp;

  return (size * nmemb);
}

size_t alloc_resp(void *ptr, size_t size, size_t nmemb, void *stream) {
  size_t total_size = 1 + size * nmemb;
  
  //g_debug("%s(%d, %d, %d)", __FUNCTION__, size, nmemb, total_size);
  
  char **response = stream;
  *response = (char *)malloc(total_size);
  if (NULL == *response)
    die(EAGAIN, "Could not allocate curl response");
  memset(*response, 0, total_size);
  memcpy(*response, ptr, total_size);
  return total_size - 1;
}

size_t drop_resp(void *ptr, size_t size, size_t nmemb, void *stream) {
  return (size * nmemb);
}

const char *build_url(
  const char *server,
  const char *endpoint,
  const char *args) {

  return string_new_concat(3, server, endpoint, args);
}

const char *build_auth_header(const char *auth_token) {
  return string_new_concat(2, AUTH_HEADER_KEY, auth_token);
}

struct curl_slist *append_system_stat_headers(struct curl_slist *headers) {
  char line[256] = {0};

  snprintf(line, 256, "bridge-firmware-version: %s",
    (bridge_firmware_version ? bridge_firmware_version :
      BRIDGE_FIRMWARE_DEFAULT_VERSION));
  headers = curl_slist_append(headers, line);

  snprintf(line, 256, "bridge-wlan-rssi: %d", wlan_rssi());
  headers = curl_slist_append(headers, line);

  memset(line, 0, 256);
  snprintf(line, 256, "bridge-wlan-snr: %d", wlan_snr());
  headers = curl_slist_append(headers, line);

  memset(line, 0, 256);
  snprintf(line, 256, "bridge-uptime: %ld", uptime());
  headers = curl_slist_append(headers, line);

  memset(line, 0, 256);
  snprintf(line, 256, "bridge-mem-free: %lu", freeram());
  headers = curl_slist_append(headers, line);

  return headers;
}

CURLcode ssl_ctx_callback(CURL *curl, void *ssl_ctx, void *user_data) {
  g_debug("%s(%d) %X", __FUNCTION__, tls_private_key_length, tls_private_key);

#if KEISTER_ENABLED == 1
  EVP_PKEY *pkey = NULL; // TODO: Make sure this gets freed.
  BIO *key_bio = NULL;

  key_bio = BIO_new_mem_buf(tls_private_key, tls_private_key_length);
  if(key_bio == NULL) {
    g_warning("%s !key_bio", __FUNCTION__);
    return CURLE_SSL_ENGINE_SETFAILED;
  }
  
  PEM_read_bio_PrivateKey(key_bio, &pkey, NULL, NULL);

  BIO_free(key_bio);

  if (!SSL_CTX_use_PrivateKey(ssl_ctx, pkey)) {
    g_warning("private key error");
    return CURLE_SSL_ENGINE_SETFAILED;
  } /* if */

  EVP_PKEY_free(pkey);
#else
  if (!SSL_CTX_use_PrivateKey_file(
        ssl_ctx,
        get_mars_key_path(),
        SSL_FILETYPE_PEM)) {
    g_warning("privte key error");
    return CURLE_SSL_ENGINE_SETFAILED;
  }
#endif
  
  if (!SSL_CTX_use_certificate_file(
        ssl_ctx,
        get_mars_cert_path(),
        SSL_FILETYPE_PEM)) {
    g_warning("client cert problem");
    return CURLE_SSL_CERTPROBLEM;
  }

  if (!SSL_CTX_check_private_key(ssl_ctx)) {
    g_warning("check privatekey problem");
    return CURLE_SSL_CERTPROBLEM;
  }
  
  return CURLE_OK;
}

void init_curl(void) 
{
  g_debug("%s", __FUNCTION__);

  if(curl_global_init(CURL_GLOBAL_DEFAULT) != CURLE_OK)
    die(EAGAIN, "Could not initialize curl");

  curl_get = curl_easy_init();
  curl_post = curl_easy_init();

  const char *firmware_version_path = get_firmare_version_path();

  bridge_firmware_version = NULL;
  if (firmware_version_path && file_exists(firmware_version_path)) {
    bridge_firmware_version = file_read(firmware_version_path);
  } /* if */

#if KEISTER_ENABLED == 1
  int ret = 0;

  g_debug("%s mars_keister_init", __FUNCTION__);
  if (0 > (ret = mars_keister_init())) {
    g_warning("%s -- mars_keister_init() %d", __FUNCTION__, ret);
    goto bail;
  }

  g_debug("%s mars_keister_get_key", __FUNCTION__);
  ret = mars_keister_get_key(MARS_TLS_PRIVATE_KEY, &tls_private_key_length, NULL);
  if(ret != 0) {
    g_warning("%s -- get_key(%d) %d", __FUNCTION__, MARS_TLS_PRIVATE_KEY, ret);
    goto bail;
  }

  if (tls_private_key_length == 0) {
    g_warning("no keys");
    ret = -2;
    goto bail;
  }

  tls_private_key = malloc(tls_private_key_length);
  if (NULL == tls_private_key)
    die(EAGAIN, "Could not allocate space for tls private key");

  ret = mars_keister_get_key(
      MARS_TLS_PRIVATE_KEY,
      &tls_private_key_length,
      tls_private_key);

bail:
  if (ret < 0)
    die(EAGAIN, "init curl failed with error: %d", ret);
#endif
}

void cleanup_curl(void) {
  curl_easy_cleanup(curl_get);
  curl_easy_cleanup(curl_post);
  curl_global_cleanup();

  if (bridge_firmware_version)
    free((void*)bridge_firmware_version);

#if KEISTER_ENABLED == 1
  if (tls_private_key != NULL) free((void *)tls_private_key);
#endif
}

const char *get_bridge_setup_url(const char *server, const char *serial) {
  return build_url(server, MARS_SETUP_ENDPOINT, serial);
}

const char *get_lockop_url(const char *server, const char *bridge_id) {
  return build_url(server, MARS_LOCKOP_ENDPOINT, bridge_id);
}

const char *get_messages_url(const char *server, const char *lock_id) {
  return build_url(server, MARS_MESSAGES_ENDPOINT, lock_id);
}

const char *get_transaction_id(const char *headers)
{
  static int transaction_int = 0;

  char *key = strstr(headers, TRANSACTION_ID_HEADER);
  char *value = NULL;

  char *temp = NULL;
  ssize_t value_len = 0;

  /*
   * If the header doesn't contain a transaction id, fall back to a
   * dumb generated transaction id.
   */
  if (NULL == key) {
    temp = malloc(TRANSACTION_ID_LENGTH);
    if (NULL == temp)
      die(EAGAIN, "Could not allocate transaction id");
    memset(temp, 0, TRANSACTION_ID_LENGTH);
    snprintf(temp, TRANSACTION_ID_LENGTH, "%d", ++transaction_int);
    return temp;
  }

  value = key + strlen(TRANSACTION_ID_HEADER);
  value_len = (strchr(value, '\n') - value);
  assert(value_len > 0);

  temp = malloc(value_len);
  if (NULL == temp)
    die(EAGAIN, "Could not allocate transaction id");
  memset(temp, 0, value_len);
  memcpy(temp, value, value_len - 1);

  return temp;
}

#if 0
static void
print_cookies(CURL *curl)
{
  CURLcode res;
  struct curl_slist *cookies;
  struct curl_slist *nc;
  int i;
 
  g_message("Cookies, curl knows:\n");
  res = curl_easy_getinfo(curl, CURLINFO_COOKIELIST, &cookies);
  if(res != CURLE_OK) {
    g_message("Curl curl_easy_getinfo failed: %s\n",
            curl_easy_strerror(res));
    return;
  }
  nc = cookies;
  i = 1;
  while(nc) {
    g_message("[%d]: %s\n", i, nc->data);
    nc = nc->next;
    i++;
  }
  if(i == 1) {
    g_message("(none)\n");
  }
  curl_slist_free_all(cookies);
}
#endif

long
get(const char *url,
    const char **response,
    const char **response_headers,
    const char *auth_token,
    volatile bool *running) {

  CURLcode res = CURLE_OK;
  struct curl_slist *headers = NULL;
  const char *auth_header = NULL;
  long http_response_code = -1;
  
  g_debug("%s(%s)", __FUNCTION__, url);

  curl_easy_reset(curl_get);
  headers = curl_slist_append(headers, "x-device: doorbell");
  if (NULL != auth_token) {
    auth_header = build_auth_header(auth_token);
    headers = curl_slist_append(headers, auth_header);
  }

  headers = append_system_stat_headers(headers);
  
  curl_easy_setopt(curl_get, CURLOPT_TIMEOUT, get_timeout_value(ENV_CURL_TIMEOUT));
  curl_easy_setopt(curl_get, CURLOPT_XFERINFOFUNCTION, xferinfo);
  curl_easy_setopt(curl_get, CURLOPT_XFERINFODATA, running);
  curl_easy_setopt(curl_get, CURLOPT_NOPROGRESS, 0L);
  curl_easy_setopt(curl_get, CURLOPT_HEADERFUNCTION, alloc_headers);
  curl_easy_setopt(curl_get, CURLOPT_HEADERDATA, response_headers);
  curl_easy_setopt(curl_get, CURLOPT_COOKIEFILE, get_http_cookie_path());
  curl_easy_setopt(curl_get, CURLOPT_COOKIEJAR, get_http_cookie_path());
  curl_easy_setopt(curl_get, CURLOPT_WRITEFUNCTION, alloc_resp);
  curl_easy_setopt(curl_get, CURLOPT_WRITEDATA, response);
  curl_easy_setopt(curl_get, CURLOPT_URL, url);
  curl_easy_setopt(curl_get, CURLOPT_HTTPGET, 1L);
  curl_easy_setopt(curl_get, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl_get, CURLOPT_TCP_KEEPALIVE, 1L);
  curl_easy_setopt(curl_get, CURLOPT_TCP_KEEPIDLE, 30L);
  curl_easy_setopt(curl_get, CURLOPT_TCP_KEEPINTVL, 30L);
  curl_easy_setopt(curl_get, CURLOPT_CAINFO, get_ca_cert_path());
  curl_easy_setopt(curl_get, CURLOPT_SSL_CTX_FUNCTION, ssl_ctx_callback);
  curl_easy_setopt(curl_get, CURLOPT_SSL_VERIFYPEER, 
    (VERIFY_PEER || verify_host_cert()));
  curl_easy_setopt(curl_get, CURLOPT_SSL_VERIFYHOST,
    (VERIFY_HOST || verify_host_cert()));
  
  if ((res = curl_easy_perform(curl_get)) != CURLE_OK)
    g_warning("GET %s error: %d - %s", url, res, curl_easy_strerror(res));
  curl_easy_getinfo(curl_get, CURLINFO_RESPONSE_CODE, &http_response_code);
  
  /*writes all known cookies to the file specified by CURLOPT_COOKIEJAR*/
  curl_easy_setopt(curl_get, CURLOPT_COOKIELIST, "FLUSH");
  
  curl_slist_free_all(headers);
  if (NULL != auth_header) free((void *)auth_header);
  return http_response_code;
}

long post(const char *url, const char *post_body, const char *auth_token) {
  CURLcode res = CURLE_OK;
  struct curl_slist *headers = NULL;
  const char *auth_header = NULL;
  long http_response_code = -1;

  curl_easy_reset(curl_post);
  headers = curl_slist_append(headers, "x-device: doorbell");
  headers = curl_slist_append(headers, "Content-Type: application/json");
  if (NULL != auth_token) {
    auth_header = build_auth_header(auth_token);
    headers = curl_slist_append(headers, auth_header);
  } else {
    die(EAGAIN, "need auth token to post.");
  }
  headers = append_system_stat_headers(headers);

  curl_easy_setopt(curl_post, CURLOPT_TIMEOUT, get_timeout_value(ENV_CURL_TIMEOUT));
  curl_easy_setopt(curl_post, CURLOPT_WRITEFUNCTION, drop_resp);
  curl_easy_setopt(curl_post, CURLOPT_URL, url);
  curl_easy_setopt(curl_post, CURLOPT_POST, 1L);
  curl_easy_setopt(curl_post, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl_post, CURLOPT_COOKIEFILE, get_http_cookie_path());
  curl_easy_setopt(curl_post, CURLOPT_COOKIEJAR, get_http_cookie_path());
  curl_easy_setopt(curl_post, CURLOPT_POSTFIELDS, post_body);
  curl_easy_setopt(curl_post, CURLOPT_POSTFIELDSIZE, strlen(post_body));
  curl_easy_setopt(curl_post, CURLOPT_CAINFO, get_ca_cert_path());
  curl_easy_setopt(curl_post, CURLOPT_SSL_CTX_FUNCTION, ssl_ctx_callback);
  curl_easy_setopt(curl_post, CURLOPT_SSL_VERIFYPEER,
    (VERIFY_PEER || verify_host_cert()));
  curl_easy_setopt(curl_post, CURLOPT_SSL_VERIFYHOST,
    (VERIFY_HOST || verify_host_cert()));

  if ((res = curl_easy_perform(curl_post)) != CURLE_OK)
    g_warning("POST %s error: %s", url, curl_easy_strerror(res));
  curl_easy_getinfo(curl_post, CURLINFO_RESPONSE_CODE, &http_response_code);
  
  /*writes all known cookies to the file specified by CURLOPT_COOKIEJAR*/
  curl_easy_setopt(curl_post, CURLOPT_COOKIELIST, "FLUSH");
  
  curl_slist_free_all(headers);
  if (NULL != auth_header) free((void *)auth_header);
  return http_response_code;
}
