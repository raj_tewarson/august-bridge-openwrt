#ifndef __UTILS_ENVIRON__
#define __UTILS_ENVIRON__

#include <stdbool.h>

#define ENV_MARS_CA       "MARS_CA"
#define ENV_MARS_CERT     "MARS_CERT"
#define ENV_MARS_KEY      "MARS_KEY"
#define ENV_MARS_LOCK_ID  "MARS_LOCK_ID"
#define ENV_MARS_LOCK_MAC "MARS_LOCK_MAC"
#define ENV_MARS_LOCK_GEN "MARS_LOCK_GEN"
#define ENV_MARS_FIRMWARE_VERSION_PATH "MARS_FIRMWARE_VERSION"
#define ENV_MARS_HTTP_COOKIE_FILE "MARS_HTTP_COOKIE_FILE"
#define ENV_VERIFY_HOST_CERT "verifyHost"

#define ENV_CONNECT_TIMEOUT          "CONNECT_TIMEOUT"
#define ENV_DISCOVER_TIMEOUT         "DISCOVER_TIMEOUT"
#define ENV_REGISTER_TIMEOUT         "REGISTER_TIMEOUT"
#define ENV_REMOVE_TIMEOUT           "REMOVE_TIMEOUT"
#define ENV_RESEND_DIRTY_BIT_TIMEOUT "RESEND_DIRTY_BIT_TIMEOUT"
#define ENV_SCAN_TIMEOUT             "SCAN_TIMEOUT"
#define ENV_ADVERTISING_TIMEOUT      "ADVERTISING_TIMEOUT"
#define ENV_WAIT_FOR_CONNECTION      "WAIT_FOR_CONNECTION"
#define ENV_GRATUITOUS_DISCONNECT_GUARD_TIME "GRATUITOUS_DISCONNECT_GUARD_TIME"
#define ENV_CONNECT_RETRIES          "CONNECT_RETRIES"
#define ENV_CURL_TIMEOUT             "CURL_TIMEOUT"

/**
 * \brief Gets the mars certificate path.
 *
 * \returns File path to the mars certificate.
 */
const char *get_mars_cert_path(void);

/**
 * \brief Gets the mars key path.
 *
 * \returns File path to the mars key.
 */
#if KEISTER_ENABLED == 0
const char *get_mars_key_path(void);
#endif

/**
 * \brief Gets the certificate authority  cert path.
 *
 * \returns File path to the ca cert.
 */
const char *get_ca_cert_path(void);

/**
 * \brief Gets the lock mac address path.
 *
 * \returns File path to the lock mac address.
 */
const char *get_lock_mac_path(void);

/**
 * \brief Gets the lock generation path.
 *
 * \returns File path to the lock generation
 */
const char *get_lock_gen_path(void);

/**
 * \brief Gets the lock id path.
 *
 * \returns File path to the lock id.
 */
const char *get_lock_id_path(void);

/**
 * \brief Gets the firmware version path
 *
 * \returns File path to the firmware version
 */
const char *get_firmare_version_path(void);

/**
 * \brief Gets the bridge http cookie path.
 *
 * \returns File path to the cookie file
 */
const char *get_http_cookie_path(void);

/**
 * \brief Gets an arbitrary environment variable or its default value.
 *
 * \returns The value of the environment variable or its default value.
 *          Zero if the timeout_var is undefined.
 */
unsigned int
get_timeout_value(const char *timeout_var);

/**
 * \brief Get environment configuration for verifying SSL certs
 *
 * \returns true if host SSL certs should be verified
 */
bool verify_host_cert(void);

/**
 * \brief Get environment config for connection retries
 *
 * \returns number of connect retries
 */
int get_connect_retries(void);

#endif /* UTILS_ENVIRON */
