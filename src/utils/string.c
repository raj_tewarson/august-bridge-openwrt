#include <ctype.h>
#include <errno.h>
#include <glib.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "utils/logging.h"
#include "utils/string.h"

#define HEX_CHARS_PER_BYTE 2

const char *string_new_concat(int num, ...) {
  va_list list;
  int i = 0;
  char *new_string = NULL;
  size_t new_string_len = 1;

  va_start(list, num);
  for (i = 0; i < num; i++) {
    char *p = va_arg(list, char *);
    if(p) {
      new_string_len += strlen(p);
    }
  }
  va_end(list);

  new_string = malloc(new_string_len);
  if (NULL == new_string)
    die(EAGAIN, "Could not allocate new string");
  memset(new_string, 0, new_string_len);

  va_start(list, num);
  for (i = 0; i < num; i++) {
    char *p = va_arg(list, char *);
    if(p) {
      strncat(new_string, p, new_string_len);
    }
  }
  va_end(list);

  return (const char *)new_string;
}

const char *string_clone(const char *str) {
  if (!str) return NULL;

  char *new_str = malloc(1+strlen(str));
  if (NULL == new_str)
    die(EAGAIN, "could not clone string: %s", str);
  memset(new_str, 0, 1 + strlen(str));

  return (const char *)strncpy(new_str, str, 1 + strlen(str));
}

GVariant *string_hex_to_bytes(const char *data) {
  GVariantBuilder *builder = NULL;
  GVariant *value = NULL;
  const char *pos = data;
  int i = 0;
  uint8_t v = 0;

  if(!data)
    return NULL;

  builder = g_variant_builder_new(G_VARIANT_TYPE("ay"));
  for (i = 0; i < strlen(data); i+=2) {
    sscanf(pos, "%2hhx", &v);
    g_variant_builder_add(builder, "y", v);
    pos += 2;
  }
  value = g_variant_new("(ay)", builder);

  if (builder != NULL) g_variant_builder_unref(builder);
  return value;
}

int string_hex_to_byte_array(const char *hex, uint8_t *byte_array) {
  const char *char_pos = hex;
  uint8_t *bin_pos = byte_array;
  int char_len = 0;
  int i = 0;

  if(!hex)
     return 0;

  char_len = strlen(hex);
  for (i = 0; i < char_len; i += 2) {
    sscanf(char_pos, "%2hhx", bin_pos);
    bin_pos++;
    char_pos += 2;
  } /* for */

  return char_len >> 1;
} /* string_hex_to_byte_array */

const char *string_bytes_to_hex(GVariant *byte_array, size_t num_bytes) {
  GVariantIter *iter = NULL;
  char *output = NULL;
  char *output_pos = NULL;
  uint8_t x;

  output = malloc(1 + (num_bytes * HEX_CHARS_PER_BYTE));
  if (NULL == output)
    die(EAGAIN, "Could not allocate space for output string");
  memset(output, 0, 1 + num_bytes);
  output_pos = output;

  g_variant_get(byte_array, "ay", &iter);
  while (g_variant_iter_loop(iter, "y", &x)) {
    sprintf(output_pos, "%02x", x);
    output_pos += 2;
  }
  g_variant_iter_free(iter);

  return (const char *)output;
}

bool string_compare_mac(const char *incoming_mac, const char *system_mac) {
  GRegex *regex = NULL;
  gchar *new_bt_addr = NULL;
  char c = '\0';
  int i = 0;
  bool same_mac = false;

  if(!system_mac)
    return false;

  regex = g_regex_new("\\.", 0, 0, NULL);
  new_bt_addr = g_regex_replace(
      regex,
      system_mac,
      strlen(system_mac),
      0,
      ":",
      0,
      NULL);

  while ((c = (char)new_bt_addr[i]) != '\0') {
    new_bt_addr[i++] = toupper(c);
  }

  if (regex != NULL) g_regex_unref(regex);

  same_mac = (0 == strcmp(incoming_mac, new_bt_addr));

  if (new_bt_addr != NULL) g_free(new_bt_addr);

  return same_mac;
}
