#ifndef __UTILS_STRING__
#define __UTILS_STRING__

#include <glib.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * \brief Creates a new string that is the concatination of all the arguments.
 *
 * \note The provided string must be freed.
 *
 * \param[in] num     The number of strings to concatinate.
 * \param[in] ...     The strings to concat.
 *
 * \returns A new string.
 */
const char *string_new_concat(int num, ...);
const char *string_clone(const char *str);

GVariant *string_hex_to_bytes(const char *data);
const char *string_bytes_to_hex(GVariant *byte_array, size_t num_bytes);
int string_hex_to_byte_array(const char *hex, uint8_t *byte_array);

bool string_compare_mac(const char *incoming_mac, const char *system_mac);

#endif /* UTILS_STRING */
