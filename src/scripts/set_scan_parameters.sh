#!/bin/sh

HCI_DEVICE=$1
SCAN_TYPE=$2
SCAN_INTERVAL=$3
SCAN_WINDOW=$4
MAC_ADDRESS=$5

dbus-send --print-reply --type=method_call --system \
  --dest=com.august.BLEScan /com/august/BLEScan \
  org.freedesktop.DBus.Properties.Set \
  string:com.august.BLEScan string:Device variant:int32:${HCI_DEVICE}

dbus-send --print-reply --type=method_call --system \
  --dest=com.august.BLEScan /com/august/BLEScan \
  org.freedesktop.DBus.Properties.Set \
  string:com.august.BLEScan string:Type variant:int32:${SCAN_TYPE}

dbus-send --print-reply --type=method_call --system \
  --dest=com.august.BLEScan /com/august/BLEScan \
  org.freedesktop.DBus.Properties.Set \
  string:com.august.BLEScan string:Interval variant:int32:${SCAN_INTERVAL}

dbus-send --print-reply --type=method_call --system \
  --dest=com.august.BLEScan /com/august/BLEScan \
  org.freedesktop.DBus.Properties.Set \
  string:com.august.BLEScan string:Window variant:int32:${SCAN_WINDOW}

dbus-send --print-reply --type=method_call --system \
  --dest=com.august.BLEScan /com/august/BLEScan \
  org.freedesktop.DBus.Properties.Set \
  string:com.august.BLEScan string:AddrFilter variant:string:${MAC_ADDRESS}
