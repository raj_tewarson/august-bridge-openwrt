#!/bin/sh

dbus-send --print-reply --type=method_call --system \
  --dest=com.august.BLEScan /com/august/BLEScan \
  org.freedesktop.DBus.Properties.GetAll \
  string:com.august.BLEScan
