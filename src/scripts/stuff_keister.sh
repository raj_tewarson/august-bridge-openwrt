#!/bin/bash
#
# Copyright 2015 August Home
#

OPENSSL=`which openssl`
if [ $? -ne 0 ]; then
  echo "$0: please install openssl"
  exit 1
fi

HEXDUMP=`which hexdump`
if [ $? -ne 0 ]; then
  echo "$0: please install hexdump"
  exit 1
fi

WC=`which wc`
if [ $? -ne 0 ]; then
  echo "$0: please install wc"
  exit 1
fi

if (( $# < 3 )); then
  echo "Usage: $0 [master key file] [new keystore file] [file to add]..."
  exit 1
fi

MASTER_KEY_FILE=$1
shift
KEYSTORE=$1
shift

CLEARSTORE=`mktemp -q /tmp/keister.XXXXXX`

if [ $? -ne 0 ]; then
  echo "$0: can't create temp file."
  exit 1
fi

PARTIAL_KEYSTORE=`mktemp -q /tmp/keister.XXXXXX`

if [ $? -ne 0 ]; then
  echo "$0: can't create temp file."
  exit 1
fi



bin_to_hex() {
  ${HEXDUMP} -v -e "/1 \"%02x\"" $1
}

hex_to_bin() {
  HEXOUT_REMAINING=$1

  while [ ${#HEXOUT_REMAINING} != 0 ]; do {
    printf "\x${HEXOUT_REMAINING:0:2}"
    HEXOUT_REMAINING=${HEXOUT_REMAINING:2}
  } done
}

hex_to_bin_16_le() {
  HEXOUT_REMAINING=$1

  while [ ${#HEXOUT_REMAINING} != 0 ]; do {
    NEW_LENGTH=$((${#HEXOUT_REMAINING} - 2))
    printf "\x${HEXOUT_REMAINING:${NEW_LENGTH}:2}"
    HEXOUT_REMAINING=${HEXOUT_REMAINING:0:${NEW_LENGTH}}
  } done
}

get_file_size() {
  THE_SIZE=`cat $1 | ${WC} -c`
  echo ${THE_SIZE#"${THE_SIZE%%[![:space:]]*}"}
}

add_file_to_clearstore() {
  ADDFILE=$1
  CLEARFILES=$2

  if [ "${ADDFILE##*.}" == "hex" ]; then
    ADDFILE_SIZE=$(( $(get_file_size ${ADDFILE}) / 2 ))
    ADDFILE_SIZE_HEX=$(printf "%04x" ${ADDFILE_SIZE})
    hex_to_bin_16_le ${ADDFILE_SIZE_HEX} >> ${CLEARFILES}
    hex_to_bin `cat ${ADDFILE}` >> ${CLEARFILES}
  else
    ADDFILE_SIZE=`get_file_size ${ADDFILE}`
    ADDFILE_SIZE_HEX=`printf "%04x" ${ADDFILE_SIZE}`
    hex_to_bin_16_le ${ADDFILE_SIZE_HEX} >> ${CLEARFILES}
    cat ${ADDFILE} >> ${CLEARFILES}
  fi
}

HEX_IV=`${OPENSSL} rand -hex 16`
HEX_MASTER_KEY=`cat ${MASTER_KEY_FILE}`

# add each file to a clear-text key store
while (( "$#" )); do
  add_file_to_clearstore $1 ${CLEARSTORE}
  shift
done

#encrypt the keys, padding to the correct block size
${OPENSSL} enc -aes-128-cbc -K ${HEX_MASTER_KEY} -iv ${HEX_IV} -in ${CLEARSTORE} -out ${PARTIAL_KEYSTORE}

# write magic number to keystore
hex_to_bin 4B73 > ${KEYSTORE}

# write total keystore size to keystore
BLOBFILE_SIZE=`get_file_size ${PARTIAL_KEYSTORE}`
BLOBFILE_SIZE_HEX=`printf "%04x" ${BLOBFILE_SIZE}`
hex_to_bin_16_le ${BLOBFILE_SIZE_HEX} >> ${KEYSTORE}

# write IV to the keystore
hex_to_bin ${HEX_IV} >> ${KEYSTORE}

# write encrypted keys to the keystore
cat ${PARTIAL_KEYSTORE} >> ${KEYSTORE}

rm -f ${CLEARSTORE} ${PARTIAL_KEYSTORE}
