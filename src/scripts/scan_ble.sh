#!/bin/bash

dbus-monitor --system sender=com.august.BLEScan &
MONITOR_PID=$!

dbus-send --system --dest=com.august.BLEScan \
  --type=method_call --print-reply /com/august/BLEScan \
  com.august.BLEScan.Start

sleep 2

dbus-send --system --dest=com.august.BLEScan \
  --type=method_call --print-reply /com/august/BLEScan \
  com.august.BLEScan.Stop

kill ${MONITOR_PID}
