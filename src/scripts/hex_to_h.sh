#!/bin/bash
#
# Copyright 2015 August Home
#

INFILE=$1

BASE_FILE_NAME=$(basename ${INFILE%.*})

if [ "$2" == "" ]; then
  OUTFILE=${BASE_FILE_NAME}.h
else
  OUTFILE=$2
fi

HEADER_DEFINE=`echo ${BASE_FILE_NAME}_H | tr \"\[:lower:\]\" \"\[:upper:\]\"`
VARIABLE_NAME=`echo ${BASE_FILE_NAME} | tr \"\[:upper:\]\" \"\[:lower:\]\"`

printf "/*\n" > ${OUTFILE}
printf " * Copyright 2015 August Home\n" >> ${OUTFILE}
printf " *\n" >> ${OUTFILE}
printf " * THIS FILE WAS AUTOMATICALLY GENERATED FROM "${INFILE}"\n" >> ${OUTFILE}
printf " */\n\n" >> ${OUTFILE}

printf "#ifndef "${HEADER_DEFINE}"\n" >> ${OUTFILE}
printf "#define "${HEADER_DEFINE}" 1 \n\n" >> ${OUTFILE}


printf "static const unsigned char "${VARIABLE_NAME}"[] = {\n  " >> ${OUTFILE}

HEX=`cat ${INFILE}`
LENGTH=0

while [ ${#HEX} != 0 ]; do {
  printf "0x${HEX:0:2}, " >> ${OUTFILE}
  HEX=${HEX:2}
  LENGTH=$(( ${LENGTH} + 1 ))
  if [ "$(( ${LENGTH} % 8))" == "0" ]; then
    printf "\n  " >> ${OUTFILE}
  fi
} done

printf "};\n" >> ${OUTFILE}
printf "#endif\n" >> ${OUTFILE}
