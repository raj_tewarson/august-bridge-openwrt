/*
 * Copyright 2015, August Home Inc
 *
 * Mars immutable key store interface
 */
#ifndef MARS_KEISTER_H
#define MARS_KEISTER_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>

typedef enum {
  MARS_WIFI_SETUP_ENCRYPTION_KEY = 0,
  MARS_UPDATE_ENCRYPTION_KEY,
  MARS_UPDATE_PUBLIC_KEY,
  MARS_TLS_PRIVATE_KEY,
} mars_key_index_t;

/*
 * mars_keister_get_key
 *
 * retrieve the given keyNum from the keister.
 * the maximum size key to return is passed in *keySize
 * the size of the key is returned in *keySize
 * if 'key' is non-NULL, key data is copied to the address pointed to
 * by 'key'.
 *
 * returns <0 on error
 */
int 
mars_keister_get_key(
  mars_key_index_t keyNum,
  uint16_t *keySize,
  uint8_t *key);

/*
 * mars_keister_init
 *
 * initialize the keystore interface.
 *
 * returns <0 on error
 */
int
mars_keister_init(void);

/*
 * mars_keister_done
 *
 * free the keystore interface.
 *
 * returns <0 on error
 */
int
mars_keister_done(void);

#ifdef __cplusplus
}
#endif


#endif /* MARS_KEISTER_H */
