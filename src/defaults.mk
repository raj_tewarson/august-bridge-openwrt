###############################################################################
# DEFAULTS FOR BRIDGE APPLICATION                                             #
#   Edit this file to make persisting changes for your local build easier.    #
###############################################################################

#=============================================================================#
# Wether or not to verify the host name of the rbs server.                    #
#                                                                             #
# If VERIFY_HOST is 1, the rbs server host name will be verified.             #
# If VERIFY_HOST is 0, the rbs server host name will not be verified.         #
#=============================================================================#
#VERIFY_HOST?=1
VERIFY_HOST?=0

#=============================================================================#
# Wether or not to validate the certificate of the rbsserver.                 #
#                                                                             #
# If VERIFY_PEER is 1, the rbs server certificate will be validated.          #
# If VERIFY_PEER is 0, the rbs server certificate will not be validated.      #
#=============================================================================#
#VERIFY_PEER?=1
VERIFY_PEER?=0

#=============================================================================#
# Wether or not to use the august proprietary key store.                      #
#                                                                             #
# If KEISTER_ENABLED is 1, the proprietary key store will be used.            #
# If KEISTER_ENABLED is 0, certificate keys must be on the filesystem.        #
#=============================================================================#
KEISTER_ENABLED?=1
#KEISTER_ENABLED?=0

#=============================================================================#
# Location to look for the august CA bundle.                                  #
#=============================================================================#
MARS_CA?="/usr/share/august_bridge/ca.cert"

#=============================================================================#
# Location to look for the cert file.                                         #
#=============================================================================#
MARS_CERT?="/usr/share/august_bridge/mars.cert"

#=============================================================================#
# Location to look for the key file.                                          #
#                                                                             #
# Only used if KEISTER_ENABLED is false.                                      #
#=============================================================================#
MARS_KEY?="/usr/local/share/mars.key"

#=============================================================================#
# Location to look for the cached lock generation.                            #
#=============================================================================#
MARS_LOCK_GEN?="/mars/LOCK_GEN"

#=============================================================================#
# Location to look for the cached lock mac address.                           #
#=============================================================================#
MARS_LOCK_MAC?="/mars/LOCK_MAC"

#=============================================================================#
# Location to look for the cached lock id.                                    #
#=============================================================================#
MARS_LOCK_ID?="/mars/LOCK_ID"

#=============================================================================#
# Location to look for the firmware version                                   #
#=============================================================================#
MARS_FIRMWARE_VERSION_PATH?="/usr/local/etc/VERSION"

#=============================================================================#
# Location to store bridge http cookies                                       #
#=============================================================================#
MARS_HTTP_COOKIE_FILE?="/tmp/bridge_cookies"

#=============================================================================#
# Time to wait for a successful ble connection.                               #
#=============================================================================#
CONNECT_TIMEOUT?=7

#=============================================================================#
# Time to wait for registering for notifications and indications on both      #
# data and secure characteristics.                                            #
#=============================================================================#
REGISTER_TIMEOUT?=10

#=============================================================================#
# Duration to search for a lock. This is only used on initial connection.     #
# Once the mac address is found we always use that.                           #
#=============================================================================#
SCAN_TIMEOUT?=60

#=============================================================================#
# How long to wait to resend the dirty bit after an unanswered dirty bit      #
# beacon.                                                                     #
#=============================================================================#
RESEND_DIRTY_BIT_TIMEOUT?=60

#=============================================================================#
# Time to wait for BlueZ to recognize the device. This should only happen     #
# once per boot if BlueZ cache is enabled. Otherwise it'll happen once per    #
# connection.                                                                 #
#=============================================================================#
DISCOVER_TIMEOUT?=30

#=============================================================================#
# How long we wait for a response to a device removal. This only happens if   #
# BlueZ cache is disabled.                                                    #
#=============================================================================#
REMOVE_TIMEOUT?=10

#=============================================================================#
# How long to wait before we consider ourselves connected in seconds. Should  #
# be greater than: ((1+CONN_LATENCY)*((1.25*MAX_CONN_INTERVAL)*2)/1000)       #
#=============================================================================#
WAIT_FOR_CONNECTION?=0

#=============================================================================#
# How many times to retry connecting before considering the attempt a failure.#
#=============================================================================#
CONNECT_RETRIES?=2

#=============================================================================#
# Don't send a disconnected event in response to a disconnect command unless  #
# it has been at least this many seconds since the last disconneced event.    #
# (works around RBS sending a connect command before waiting for a            #
# disconnected event in reply to certain invocations of the disconnect        #
# command)                                                                    #
#=============================================================================#
GRATUITOUS_DISCONNECT_GUARD_TIME?=2

#=============================================================================#
# Tell RBS that the lock has stopped advertising if this duration passes      #
# without receiving an advertising packet.                                    #
#=============================================================================#
ADVERTISING_TIMEOUT?=60

#=============================================================================#
# Command to run on advertisement timeout. Set at compile time, cannot be set #
# at runtime.                                                                 #
# TODO:                                                                       #
#=============================================================================#
ADVERTISING_TIMEOUT_COMMAND?=""

#=============================================================================#
# Time to wait before timing out a CURL request. In seconds.                  #
#=============================================================================#
CURL_TIMEOUT?=300
