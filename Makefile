##############################################
# OpenWrt Makefile for helloworld program
#
#
# Most of the variables used here are defined in
# the include directives below. We just need to
# specify a basic description of the package,
# where to build our program, where to find
# the source files, and where to install the
# compiled program on the router.
#
# Be very careful of spacing in this file.
# Indents should be tabs, not spaces, and
# there should be no trailing whitespace in
# lines that are not commented.
#
##############################################

include $(TOPDIR)/rules.mk

# Name and release number of this package
PKG_NAME:=august_bridge
PKG_VERSION:=1.0.0
PKG_RELEASE:=1

# This specifies the directory where we're going to build the program.
# The root build directory, $(BUILD_DIR), is by default the build_mipsel
# directory in your OpenWrt SDK directory
PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)

include $(INCLUDE_DIR)/package.mk

# Specify package information for this program.
# The variables defined here should be self explanatory.
define Package/august_bridge
 SECTION:=utils
 CATEGORY:=Utilities
 TITLE:=August connect for Openwrt
 DEPENDS:=+libpthread +librt +libjson-c +glib2 +dbus +bluez-libs +libcurl +libopenssl +dbus-utils +bluez-daemon +bluez-utils
endef

define Package/august_bridge/description
 An august wifi to ble bridge
 NOTE: needs special keys defined by enviorment var SECURE_KEYS_PATH defined
endef

# Specify what needs to be done to prepare for building the package.
# In our case, we need to copy the source files to the build directory.
# This is NOT the default.  The default uses the PKG_SOURCE_URL and the
# PKG_SOURCE which is not defined here to download the source from the web.
# In order to just build a simple program that we have just written, it is
# much easier to do it this way.
define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) -rf ./src/* $(PKG_BUILD_DIR)/
endef

define Build/Configure
	$(call Build/Configure/Default,--with-linux-headers=$(LINUX_DIR))
endef

# We do not need to define Build/Configure or Build/Compile directives
# The defaults are appropriate for compiling a simple program such as this one

# Specify where and how to install the program. Since we only have one file,
# the helloworld executable, install it by copying it to the /bin directory on
# the router. The $(1) variable represents the root directory on the router running
# OpenWrt. The $(INSTALL_DIR) variable contains a command to prepare the install
# directory if it does not already exist.  Likewise $(INSTALL_BIN) contains the
# command to copy the binary file from its current location (in our case the build
# directory) to the install directory.

#TODO: install files...
define Package/august_bridge/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/august_bridge $(1)/usr/bin/
	$(INSTALL_DIR) $(1)/usr/share/august_bridge
	$(INSTALL_CONF) $(PKG_BUILD_DIR)/certs/* $(1)/usr/share/august_bridge
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/scan_ble.sh $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/set_scan_parameters.sh $(1)/usr/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/scripts/get_scan_parameters.sh $(1)/usr/bin/
	$(INSTALL_DIR) $(1)/etc/dbus-1
	$(INSTALL_DIR) $(1)/etc/dbus-1/system.d
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/blescand.conf $(1)/etc/dbus-1/system.d/
endef

# This line executes the necessary commands to compile our program.
# The above define directives specify all the information needed, but this
# line calls BuildPackage which in turn actually uses this information to
# build a package.
$(eval $(call BuildPackage,august_bridge))

